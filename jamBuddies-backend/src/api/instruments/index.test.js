import request from 'supertest-as-promised'
import { masterKey } from '../../config'
import express from '../../services/express'
import routes, { Instruments } from '.'

const app = () => express(routes)

let instruments

beforeEach(async () => {
  instruments = await Instruments.create({})
})

test('GET /instruments 200 (master)', async () => {
  const { status, body } = await request(app())
    .get('/')
    .query({ access_token: masterKey })
  expect(status).toBe(200)
  expect(Array.isArray(body)).toBe(true)
})

test('GET /instruments 401', async () => {
  const { status } = await request(app())
    .get('/')
  expect(status).toBe(401)
})

test('GET /instruments/:id 200 (master)', async () => {
  const { status, body } = await request(app())
    .get(`/${instruments.id}`)
    .query({ access_token: masterKey })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(instruments.id)
})

test('GET /instruments/:id 401', async () => {
  const { status } = await request(app())
    .get(`/${instruments.id}`)
  expect(status).toBe(401)
})

test('GET /instruments/:id 404 (master)', async () => {
  const { status } = await request(app())
    .get('/123456789098765432123456')
    .query({ access_token: masterKey })
  expect(status).toBe(404)
})

test('PUT /instruments/:id 200 (master)', async () => {
  const { status, body } = await request(app())
    .put(`/${instruments.id}`)
    .send({ access_token: masterKey, name: 'test', description: 'test' })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(instruments.id)
  expect(body.name).toEqual('test')
  expect(body.description).toEqual('test')
})

test('PUT /instruments/:id 401', async () => {
  const { status } = await request(app())
    .put(`/${instruments.id}`)
  expect(status).toBe(401)
})

test('PUT /instruments/:id 404 (master)', async () => {
  const { status } = await request(app())
    .put('/123456789098765432123456')
    .send({ access_token: masterKey, name: 'test', description: 'test' })
  expect(status).toBe(404)
})

test('DELETE /instruments/:id 204 (master)', async () => {
  const { status } = await request(app())
    .delete(`/${instruments.id}`)
    .query({ access_token: masterKey })
  expect(status).toBe(204)
})

test('DELETE /instruments/:id 401', async () => {
  const { status } = await request(app())
    .delete(`/${instruments.id}`)
  expect(status).toBe(401)
})

test('DELETE /instruments/:id 404 (master)', async () => {
  const { status } = await request(app())
    .delete('/123456789098765432123456')
    .query({ access_token: masterKey })
  expect(status).toBe(404)
})
