import { Instruments } from '.'

let instruments

beforeEach(async () => {
  instruments = await Instruments.create({ name: 'test', description: 'test' })
})

describe('view', () => {
  it('returns simple view', () => {
    const view = instruments.view()
    expect(typeof view).toBe('object')
    expect(view.id).toBe(instruments.id)
    expect(view.name).toBe(instruments.name)
    expect(view.description).toBe(instruments.description)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })

  it('returns full view', () => {
    const view = instruments.view(true)
    expect(typeof view).toBe('object')
    expect(view.id).toBe(instruments.id)
    expect(view.name).toBe(instruments.name)
    expect(view.description).toBe(instruments.description)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })
})
