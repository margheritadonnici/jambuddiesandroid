import { Router } from 'express'
import { middleware as query } from 'querymen'
import { middleware as body } from 'bodymen'
import { master } from '../../services/passport'
import { index, show, update, destroy } from './controller'
import { schema } from './model'
export Instruments, { schema } from './model';

const router = new Router()
const { name, description } = schema.tree

/**
 * @api {get} /instruments Retrieve instruments
 * @apiName RetrieveInstruments
 * @apiGroup Instruments
 * @apiPermission master
 * @apiParam {String} access_token master access token.
 * @apiUse listParams
 * @apiSuccess {Object[]} instruments List of instruments.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 401 master access only.
 */
router.get('/',
  master(),
  query(),
  index)

/**
 * @api {get} /instruments/:id Retrieve instruments
 * @apiName RetrieveInstruments
 * @apiGroup Instruments
 * @apiPermission master
 * @apiParam {String} access_token master access token.
 * @apiSuccess {Object} instruments Instruments's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Instruments not found.
 * @apiError 401 master access only.
 */
router.get('/:id',
  master(),
  show)

/**
 * @api {put} /instruments/:id Update instruments
 * @apiName UpdateInstruments
 * @apiGroup Instruments
 * @apiPermission master
 * @apiParam {String} access_token master access token.
 * @apiParam name Instruments's name.
 * @apiParam description Instruments's description.
 * @apiSuccess {Object} instruments Instruments's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Instruments not found.
 * @apiError 401 master access only.
 */
router.put('/:id',
  master(),
  body({ name, description }),
  update)

/**
 * @api {delete} /instruments/:id Delete instruments
 * @apiName DeleteInstruments
 * @apiGroup Instruments
 * @apiPermission master
 * @apiParam {String} access_token master access token.
 * @apiSuccess (Success 204) 204 No Content.
 * @apiError 404 Instruments not found.
 * @apiError 401 master access only.
 */
router.delete('/:id',
  master(),
  destroy)

export default router
