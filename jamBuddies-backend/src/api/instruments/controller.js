import _ from 'lodash'
import { success, notFound } from '../../services/response/'
import { Instruments } from '.'

export const index = ({ querymen: { query, select, cursor } }, res, next) =>
  Instruments.find(query, select, cursor)
    .then((instruments) => instruments.map((instruments) => instruments.view()))
    .then(success(res))
    .catch(next)

export const show = ({ params }, res, next) =>
  Instruments.findById(params.id)
    .then(notFound(res))
    .then((instruments) => instruments ? instruments.view() : null)
    .then(success(res))
    .catch(next)

export const update = ({ bodymen: { body }, params }, res, next) =>
  Instruments.findById(params.id)
    .then(notFound(res))
    .then((instruments) => instruments ? _.merge(instruments, body).save() : null)
    .then((instruments) => instruments ? instruments.view(true) : null)
    .then(success(res))
    .catch(next)

export const destroy = ({ params }, res, next) =>
  Instruments.findById(params.id)
    .then(notFound(res))
    .then((instruments) => instruments ? instruments.remove() : null)
    .then(success(res, 204))
    .catch(next)
