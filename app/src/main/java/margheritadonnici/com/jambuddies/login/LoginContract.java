package margheritadonnici.com.jambuddies.login;

/**
 * Created by Margherita Donnici on 10/28/17.
 */


import android.content.Intent;

import margheritadonnici.com.jambuddies.BasePresenter;
import margheritadonnici.com.jambuddies.BaseView;

/**
 * This specifies the contract between the Login view and the Login presenter.
 */

public interface LoginContract {

    interface View extends BaseView<Presenter> {

        void showError(String error);

        void removeError(String error);

        void showLoadingDialog();

        void showLoginSuccess();

        void showLoginError(String error);

        void showSuccessfulSignUp();
    }

    interface Presenter extends BasePresenter {

        void login(String email, String password);

        void onSignUpResult(int requestCode, int resultCode, Intent data);
    }
}
