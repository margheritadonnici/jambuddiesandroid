package margheritadonnici.com.jambuddies.login;


import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import margheritadonnici.com.jambuddies.R;
import margheritadonnici.com.jambuddies.signup.SignUpActivity;

/**
 * Display the login form.
 */
public class LoginFragment extends Fragment implements LoginContract.View {

    private LoginContract.Presenter mPresenter;

    private static final String TAG = "LoginFragment";

    private EditText mEmailEditText;
    private EditText mPasswordEditText;
    private Button mLoginButton;
    private TextView mSignUpLink;
    private TextInputLayout mEmailTIL;
    private TextInputLayout mPasswordTIL;
    private ProgressDialog mProgressDialog;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment LoginFragment.
     */
    public static LoginFragment newInstance() {
        LoginFragment fragment = new LoginFragment();
        return fragment;
    }

    @Override
    public void setPresenter(@NonNull LoginContract.Presenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_login, container, false);

        // Bind views
        mEmailEditText = (EditText) rootView.findViewById(R.id.signup_email);
        mPasswordEditText = (EditText) rootView.findViewById(R.id.signup_password);
        mLoginButton = (Button) rootView.findViewById(R.id.btn_login);
        mSignUpLink = (TextView) rootView.findViewById(R.id.link_signup);
        mEmailTIL = (TextInputLayout) rootView.findViewById(R.id.email_til);
        mPasswordTIL = (TextInputLayout) rootView.findViewById(R.id.password_til);

        // Sign Up link styling
        SpannableString styledString = new SpannableString("No account yet? Create one");
        styledString.setSpan(new StyleSpan(Typeface.BOLD), 16, 26, 0);
        mSignUpLink.setText(styledString);

        // Login Button
        mLoginButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                String email = mEmailEditText.getText().toString();
                String password = mPasswordEditText.getText().toString();
                mLoginButton.setEnabled(false);
                mPresenter.login(email, password);
            }
        });

        // Sign Up Link
        mSignUpLink.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // Show the sign up activity
                Intent intent = new Intent(getContext(), SignUpActivity.class);
                startActivityForResult(intent, LoginActivity.REQUEST_SIGNUP);
            }
        });

        return rootView;
    }

    // Method to show errors
    @Override
    public void showError(String error) {
        switch (error) {
            case "email": // Email inserted in form field was not valid
                mEmailTIL.setError("Invalid email address");
                break;
            case "password": // Password field is empty
                mPasswordTIL.setError("Invalid password");
                break;
            case "login_failed": // Login failed
                if (mProgressDialog != null) {
                    // If loading dialog was showing, dismiss it
                    mProgressDialog.dismiss();
                }
                showMessage("Login failed");
                mLoginButton.setEnabled(true);
                break;
            default:
                showMessage("Login failed");
        }

    }

    // Removes errors from form fields
    @Override
    public void removeError(String error) {
        switch (error) {
            case "email":
                mEmailTIL.setError("");
                break;
            case "password":
                mPasswordTIL.setError("");
                break;
            default:
                mEmailTIL.setError("");
                mPasswordTIL.setError("");
        }

    }

    @Override
    public void showLoadingDialog() {
        mProgressDialog = new ProgressDialog(getContext(), R.style.Dark_Dialog);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Authenticating...");
        mProgressDialog.show();
    }

    // Called when sign up was successful
    @Override
    public void showSuccessfulSignUp() {
        showMessage("Sign up successful");
        // By default we just finish the Activity and log them in automatically
        getActivity().finish();
    }

    // Called when login was successful
    @Override
    public void showLoginSuccess() {
        if (mProgressDialog != null) {
            // If loading dialog was showing, dismiss it
            mProgressDialog.dismiss();
        }
        showMessage("Login successful");
        mLoginButton.setEnabled(true);
        //Finish parent activity and return to home page
        getActivity().finish();
    }

    @Override
    public void showLoginError(String error) {
        if (mProgressDialog != null) {
            // If loading dialog was showing, dismiss it
            mProgressDialog.dismiss();
        }
        showMessage(error);
        mLoginButton.setEnabled(true);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        mPresenter.onSignUpResult(requestCode, resultCode, data);
    }

    private void showMessage(String message) {
        Snackbar.make(getView(), message, Snackbar.LENGTH_LONG).show();
    }

}
