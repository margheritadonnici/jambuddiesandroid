package margheritadonnici.com.jambuddies.home;

import margheritadonnici.com.jambuddies.BasePresenter;
import margheritadonnici.com.jambuddies.BaseView;
import margheritadonnici.com.jambuddies.data.model.JoinRequestWithDetails;
import margheritadonnici.com.jambuddies.data.model.UserRequests;

/**
 * Created by Margherita Donnici on 1/12/18.
 */

public interface HomeContract {

    interface View extends BaseView<Presenter> {

        void showLoadingDialog(String msg);

        void hideLoadingDialog();

        void showMessage(String msg);

        void displayData(UserRequests reqList);
    }

    interface Presenter extends BasePresenter {
        String getCurrentUserFirstName();

        void getCurrentUserRequests();

        void acceptRequest(JoinRequestWithDetails req);

        void rejectRequest(JoinRequestWithDetails req);
    }
}
