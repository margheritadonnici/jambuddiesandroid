package margheritadonnici.com.jambuddies.home;

import android.content.Context;
import android.util.Log;

import margheritadonnici.com.jambuddies.data.AppDataManager;
import margheritadonnici.com.jambuddies.data.DataManager;
import margheritadonnici.com.jambuddies.data.model.ExistingMember;
import margheritadonnici.com.jambuddies.data.model.Group;
import margheritadonnici.com.jambuddies.data.model.JoinRequest;
import margheritadonnici.com.jambuddies.data.model.JoinRequestWithDetails;
import margheritadonnici.com.jambuddies.data.model.UserRequests;

/**
 * Created by Margherita Donnici on 1/11/18.
 */

public class HomePresenter implements HomeContract.Presenter {

    private static final String TAG = "HomePresenter";
    private final HomeContract.View mHomeView;
    private final AppDataManager mDataManager;
    private final Context mContext;

    public HomePresenter(HomeContract.View view, AppDataManager appDataManager, Context context) {
        this.mHomeView = view;
        view.setPresenter(this);
        this.mDataManager = appDataManager;
        this.mContext = context;
    }

    @Override
    public void start() {
        getCurrentUserRequests();
    }

    @Override
    public void getCurrentUserRequests() {
        mHomeView.showLoadingDialog("Getting requests...");
        if (mDataManager.getCurrentUserId() != null) {
            mDataManager.getUserRequestsApiCall(mDataManager.getCurrentUserId(), new DataManager.GetUserRequestsCallback() {
                @Override
                public void onGetUserRequestsSuccess(UserRequests reqList) {
                    mHomeView.hideLoadingDialog();
                    mHomeView.displayData(reqList);
                }

                @Override
                public void onGetUserRequestsFailure(String error) {
                    mHomeView.hideLoadingDialog();
                    mHomeView.showMessage("Unable to fetch requests: " + error);
                }
            });
        } else {
            mHomeView.hideLoadingDialog();
        }
    }

    @Override
    public String getCurrentUserFirstName() {
        return mDataManager.getCurrentUserFirstName();
    }

    @Override
    public void acceptRequest(JoinRequestWithDetails req) {
        mHomeView.showLoadingDialog("Updating...");
        mDataManager.updateRequestStatusApiCall(req.getId(), "Accepted", new DataManager.UpdateRequestStatusCallback() {
            @Override
            public void onUpdateRequestStatusSuccess(JoinRequest request) {
                // add user to group members
                ExistingMember newMember = new ExistingMember(request.getInstrument(), request.getFrom());
                mDataManager.addMemberToGroupApiCall(request.getGroupId(), newMember, new DataManager.UpdateGroupMembersCallback() {
                    @Override
                    public void onUpdateGroupMembersSuccess(Group group) {
                        mHomeView.hideLoadingDialog();
                        mHomeView.showMessage("Request accepted");
                        getCurrentUserRequests();
                    }

                    @Override
                    public void onUpdateGroupMembersFailure(String error) {
                        mHomeView.hideLoadingDialog();
                        mHomeView.showMessage("Unable to add member right now.");
                    }
                });
            }

            @Override
            public void onUpdateRequestStatusFailure(String error) {
                mHomeView.hideLoadingDialog();
                mHomeView.showMessage("Unable to accept request at the moment.");
            }
        });
    }

    @Override
    public void rejectRequest(JoinRequestWithDetails req) {
        mHomeView.showLoadingDialog("Updating...");
        mDataManager.updateRequestStatusApiCall(req.getId(), "Rejected", new DataManager.UpdateRequestStatusCallback() {
            @Override
            public void onUpdateRequestStatusSuccess(JoinRequest request) {
                mHomeView.hideLoadingDialog();
                mHomeView.showMessage("Request rejected.");
                getCurrentUserRequests();
            }

            @Override
            public void onUpdateRequestStatusFailure(String error) {
                mHomeView.hideLoadingDialog();
                mHomeView.showMessage("Unable to reject request at the moment.");
            }
        });
    }
}
