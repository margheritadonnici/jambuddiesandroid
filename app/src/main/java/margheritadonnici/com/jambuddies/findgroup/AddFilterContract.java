package margheritadonnici.com.jambuddies.findgroup;

import android.content.Intent;
import android.graphics.Bitmap;
import android.widget.ImageView;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.model.LatLng;

import java.util.List;

import margheritadonnici.com.jambuddies.BasePresenter;
import margheritadonnici.com.jambuddies.BaseView;
import margheritadonnici.com.jambuddies.utils.PlaceArrayAdapter;

/**
 * Created by Margherita Donnici on 12/6/17.
 */

public interface AddFilterContract {

    interface View extends BaseView<Presenter> {
        void showMessage(String msg);

        void showEnableGPSDialog();

        PlaceArrayAdapter getPlaceArrayAdapter();

        void onCreateFilterSuccess();
    }

    interface Presenter extends BasePresenter {
        LatLng getUserLocation();

        LatLng getUserStoredLocation();

        List<String> getUserInstruments();

        String getLocationAddressString(LatLng location);

        GoogleApiClient getGoogleApiClient();

        void checkUserPermissions();

        void createSearchFilter(String name, List<String> genres, List<String> instruments, LatLng location, int range);
    }

}
