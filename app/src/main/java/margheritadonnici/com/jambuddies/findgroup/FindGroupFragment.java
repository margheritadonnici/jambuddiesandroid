package margheritadonnici.com.jambuddies.findgroup;


import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import margheritadonnici.com.jambuddies.MainActivity;
import margheritadonnici.com.jambuddies.R;
import margheritadonnici.com.jambuddies.data.model.Group;
import margheritadonnici.com.jambuddies.data.model.GroupWithMembers;
import margheritadonnici.com.jambuddies.mygroups.MyGroupsContract;
import margheritadonnici.com.jambuddies.utils.ActivityUtils;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FindGroupFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FindGroupFragment extends Fragment implements FindGroupContract.View {

    private static final String TAG = "FindGroupFragment";
    FindGroupContract.Presenter mPresenter;
    List<GroupWithMembers> mSearchResults = new ArrayList<>();
    LatLng mCurrentUserLocation;
    FindGroupListAdapter findGroupListAdapter;

    ProgressDialog mProgressDialog;
    TextView emptyListView;
    RecyclerView groupsRv;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment FindGroupFragment.
     */
    public static FindGroupFragment newInstance() {
        FindGroupFragment fragment = new FindGroupFragment();
        return fragment;
    }

    @Override
    public void setPresenter(FindGroupContract.Presenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPresenter.start();
        mCurrentUserLocation = mPresenter.getCurrentUserLocation();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Set ActionBar Title
        getActivity().setTitle("Find Group");
        // ActionBar buttons
        setHasOptionsMenu(true);

        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_find_group, container, false);

        // Recycler View
        emptyListView = (TextView) rootView.findViewById(R.id.empty_group_list_view);
        groupsRv = (RecyclerView) rootView.findViewById(R.id.groups_list);
        // Get the TextView to display if our dataset is empty
        if (mSearchResults.isEmpty()) {
            emptyListView.setVisibility(View.VISIBLE);
        }
        // Create adapter passing in the members list
        findGroupListAdapter = new FindGroupListAdapter(getContext(), this, ((MainActivity) getActivity()).getSearchFilter().getLocation(), mSearchResults);
        // Set layout manager to position the items
        groupsRv.setLayoutManager(new LinearLayoutManager(getActivity()));
        // Attach the adapter to the recyclerview to populate items
        groupsRv.setAdapter(findGroupListAdapter);
        return rootView;
    }

    /********** ACTION BAR BUTTONS **********/
    // Create the action bar buttons
    @Override
    public void onCreateOptionsMenu(
            Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.action_bar_filter, menu);
    }

    // Handle action bar help button activities
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.help_button) {
            // HELP DIALOG
            AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
            alertDialog.setTitle("Help");
            alertDialog.setMessage("This screen displays all the groups nearest to your location which are looking for one of your instruments. To change/add filters, use the upper right button.");
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            alertDialog.show();
        }

        if (id == R.id.filter_button) {
            ((MainActivity) getActivity()).goToAddFilterFragment();
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void showLoadingDialog(String msg) {
        mProgressDialog = new ProgressDialog(getContext(), R.style.Dark_Dialog);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage(msg);
        mProgressDialog.show();
    }

    @Override
    public void hideLoadingDialog() {
        if (mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    public TextView getEmptyListView() {
        return emptyListView;
    }

    public void showMessage(String message) {
        Snackbar.make(getView(), message, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void displayData(List<GroupWithMembers> groupList) {
        mSearchResults = groupList;
        findGroupListAdapter.updateGroupList(groupList);
        if (!mSearchResults.isEmpty()) {
            emptyListView.setVisibility(View.GONE);
        } else {
            emptyListView.setVisibility(View.VISIBLE);
        }
        findGroupListAdapter.notifyDataSetChanged();
    }

    @Override
    public void onStart() {
        super.onStart();
        // Refresh search results with current filter
        mPresenter.searchForGroups(((MainActivity) getActivity()).getSearchFilter());
    }

}
