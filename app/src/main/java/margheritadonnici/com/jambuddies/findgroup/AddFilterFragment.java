package margheritadonnici.com.jambuddies.findgroup;


import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;

import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import margheritadonnici.com.jambuddies.MainActivity;
import margheritadonnici.com.jambuddies.R;
import margheritadonnici.com.jambuddies.utils.PlaceArrayAdapter;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddFilterFragment extends Fragment implements AddFilterContract.View,
        OnMapReadyCallback {

    private static final String TAG = "AddFilterFragment";
    private static final LatLngBounds BOUNDS = new LatLngBounds(new LatLng(37.398160, -122.180831),
            new LatLng(37.430610, -121.972090));
    AddFilterContract.Presenter mPresenter;

    private Spinner rangeSpinner;
    private Button genreButton;
    private Button instrumentButton;
    private Button addFilterButton;
    private AutoCompleteTextView locationAutoCompleteTextView;
    private ImageButton myLocationButton;
    private EditText nameFilter;
    private ProgressDialog mProgressDialog;

    private PlaceArrayAdapter mPlaceArrayAdapter;

    private LatLng mLocation;
    private Circle rangeCircle;
    private Marker locationMarker;
    private GoogleMap mGoogleMap;
    int mSelectedRange = 0;

    private ArrayList<String> mSelectedGenres = new ArrayList<String>();
    private ArrayList<String> mSelectedInstruments = new ArrayList<String>();

    private String[] musicGenresList;
    private boolean[] checkedGenreItems;
    private boolean[] checkedInstrumentItems;
    private String[] instrumentsArray;

    public static AddFilterFragment newInstance() {
        AddFilterFragment fragment = new AddFilterFragment();
        return fragment;
    }


    @Override
    public void setPresenter(AddFilterContract.Presenter presenter) {
        mPresenter = presenter;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPresenter.start();
        mLocation = mPresenter.getUserStoredLocation();// default location is the one from the user's profile
        mSelectedInstruments = (ArrayList<String>) mPresenter.getUserInstruments();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_add_filter, container, false);

        // Set ActionBar Title and Buttons
        getActivity().setTitle("Search Filters");
        setHasOptionsMenu(true);

        // Bind views
        rangeSpinner = (Spinner) rootView.findViewById(R.id.range_spinner);
        genreButton = (Button) rootView.findViewById(R.id.genre_button);
        instrumentButton = (Button) rootView.findViewById(R.id.instrument_button);
        locationAutoCompleteTextView = (AutoCompleteTextView) rootView.findViewById(R.id.user_location);
        myLocationButton = (ImageButton) rootView.findViewById(R.id.my_location_button);
        addFilterButton = (Button) rootView.findViewById(R.id.apply_filter_btn);
        nameFilter = (EditText) rootView.findViewById(R.id.name_filter);


        /** Range spinner **/
        // Create an ArrayAdapter using the string array and a default spinner layout
        final ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(), R.array.distance_filter, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        rangeSpinner.setAdapter(adapter);
        rangeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long id) {
                switch (adapter.getItem(pos).toString()) {
                    case "5 km":
                        mSelectedRange = 5;
                        rangeCircle.setVisible(true);
                        rangeCircle.setRadius(5000);
                        mGoogleMap.moveCamera(CameraUpdateFactory.zoomTo(10));
                        break;
                    case "10 km":
                        mSelectedRange = 10;
                        rangeCircle.setVisible(true);
                        rangeCircle.setRadius(10000);
                        mGoogleMap.moveCamera(CameraUpdateFactory.zoomTo(10));
                        break;
                    case "20 km":
                        mSelectedRange = 20;
                        rangeCircle.setVisible(true);
                        rangeCircle.setRadius(20000);
                        mGoogleMap.moveCamera(CameraUpdateFactory.zoomTo(10));
                        break;
                    case "50 km":
                        mSelectedRange = 50;
                        rangeCircle.setVisible(true);
                        rangeCircle.setRadius(50000);
                        mGoogleMap.moveCamera(CameraUpdateFactory.zoomTo(7));
                        break;
                    case "100 km":
                        mSelectedRange = 100;
                        rangeCircle.setVisible(true);
                        rangeCircle.setRadius(100000);
                        mGoogleMap.moveCamera(CameraUpdateFactory.zoomTo(7));
                        break;
                    case "200 km":
                        mSelectedRange = 200;
                        mGoogleMap.moveCamera(CameraUpdateFactory.zoomTo(5));
                        rangeCircle.setVisible(true);
                        rangeCircle.setRadius(200000);
                        break;
                    default:
                        rangeCircle.setVisible(false);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        /** Genre Button **/
        musicGenresList = getResources().getStringArray(R.array.music_genres);
        checkedGenreItems = new boolean[musicGenresList.length];
        genreButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                final ArrayList selectedItems = new ArrayList();  // Where we track the selected items
                builder.setMultiChoiceItems(musicGenresList, checkedGenreItems,
                        new DialogInterface.OnMultiChoiceClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int which, boolean isChecked) {
                                if (isChecked) {
                                    // If the user checked the item, add it to the selected items
                                    selectedItems.add(which);
                                    checkedGenreItems[which] = true;
                                } else if (selectedItems.contains(which)) {
                                    // Else, if the item is already in the array, remove it
                                    selectedItems.remove(Integer.valueOf(which));
                                    checkedGenreItems[which] = false;
                                }
                            }
                        })
                        // Set the action buttons
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                for (Object item : selectedItems) {
                                    mSelectedGenres.add(musicGenresList[(int) item]);
                                }
                                // User clicked OK, so save the mSelectedItems results somewhere
                                // or return them to the component that opened the dialog
                            }
                        })
                        .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        });
                AlertDialog genresDialog = builder.create();
                genresDialog.show();
            }
        });

        /** Instruments Button **/
        final List<String> instrumentsList = mPresenter.getUserInstruments();
        instrumentsArray = instrumentsList.toArray(new String[instrumentsList.size()]);
        checkedInstrumentItems = new boolean[instrumentsArray.length];
        Arrays.fill(checkedInstrumentItems, true);
        instrumentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                final ArrayList selectedItems = new ArrayList(); // Where we track the selected items
                for (int i = 0; i < instrumentsList.size(); i++) {
                    // At first all instruments are added
                    selectedItems.add(i);
                }
                builder.setMultiChoiceItems(instrumentsArray, checkedInstrumentItems,
                        new DialogInterface.OnMultiChoiceClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int which, boolean isChecked) {
                                if (isChecked) {
                                    // If the user checked the item, add it to the selected items
                                    selectedItems.add(which);
                                    checkedInstrumentItems[which] = true;
                                } else if (selectedItems.contains(which)) {
                                    // Else, if the item is already in the array, remove it
                                    selectedItems.remove(Integer.valueOf(which));
                                    checkedInstrumentItems[which] = false;
                                }
                            }
                        })
                        // Set the action buttons
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                ArrayList<String> tempInstrumentList = new ArrayList<String>();
                                for (Object item : selectedItems) {
                                    tempInstrumentList.add(instrumentsArray[(int) item]);
                                }
                                mSelectedInstruments = tempInstrumentList;
                            }
                        })
                        .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        });
                AlertDialog instrumentDialog = builder.create();
                instrumentDialog.show();
            }
        });

        /***** LOCATION AUTOCOMPLETE *****/

        locationAutoCompleteTextView.setThreshold(2);
        locationAutoCompleteTextView.setOnItemClickListener(
                new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                        final PlaceArrayAdapter.PlaceAutocomplete item = mPlaceArrayAdapter.getItem(position);
                        final String placeId = String.valueOf(item.placeId);
                        Log.i(TAG, "Selected: " + item.description);
                        PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi
                                .getPlaceById(mPresenter.getGoogleApiClient(), placeId);
                        placeResult.setResultCallback(updatePlaceDetailsCallback);
                        Log.i(TAG, "Fetching details for ID: " + item.placeId);
                    }
                }
        );

        mPlaceArrayAdapter = new PlaceArrayAdapter(getContext(), android.R.layout.simple_list_item_1, BOUNDS, null);
        locationAutoCompleteTextView.setAdapter(mPlaceArrayAdapter);
        // Set default initial location
        Log.e(TAG, String.valueOf(mLocation.latitude) + String.valueOf(mLocation.longitude));
        locationAutoCompleteTextView.setText(mPresenter.getLocationAddressString(mLocation));

        // My Location Button
        myLocationButton = (ImageButton) rootView.findViewById(R.id.my_location_button);
        myLocationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Get the user location
                mPresenter.checkUserPermissions();
                mLocation = mPresenter.getUserLocation();
                if (mLocation != null) {
                    locationAutoCompleteTextView.setText(mPresenter.getLocationAddressString(mLocation));
                    // Update map
                    updateMapLocation();
                }
            }
        });

        /** MAP **/
        // Get the SupportMapFragment and request notification
        // when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        /** ADD FILTER BUTTON **/

        addFilterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPresenter.createSearchFilter(nameFilter.getText().toString(), mSelectedGenres, mSelectedInstruments, mLocation, mSelectedRange);
            }
        });

        return rootView;
    }

    // Create the action bar buttons
    @Override
    public void onCreateOptionsMenu(
            Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.action_bar_button, menu);
    }

    private ResultCallback<PlaceBuffer> updatePlaceDetailsCallback
            = new ResultCallback<PlaceBuffer>() {
        @Override
        public void onResult(PlaceBuffer places) {
            if (!places.getStatus().isSuccess()) {
                Log.e(TAG, "Place query did not complete. Error: " +
                        places.getStatus().toString());
                return;
            }
            // Selecting the first object buffer.
            final Place place = places.get(0);
            // Save selected location
            mLocation = place.getLatLng();
        }
    };

    public PlaceArrayAdapter getPlaceArrayAdapter() {
        return mPlaceArrayAdapter;
    }

    @Override
    public void onCreateFilterSuccess() {
        // Go back to FindGroup fragment
        getFragmentManager().popBackStackImmediate();
    }

    public void showMessage(String message) {
        Snackbar.make(getView(), message, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showEnableGPSDialog() {
        final android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(getContext());
        builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        dialog.cancel();
                    }
                });
        final android.support.v7.app.AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        // Add a marker in Sydney, Australia,
        // and move the map's camera to the same location.
        mGoogleMap = googleMap;
        locationMarker = googleMap.addMarker(new MarkerOptions().position(mLocation)
                .title("Your Location"));
        rangeCircle = googleMap.addCircle(new CircleOptions()
                .center(mLocation)
                .radius(5000)
                .strokeColor(Color.rgb(30, 144, 255))
                .fillColor(Color.argb(75, 30, 144, 255))
                .visible(false));
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mLocation, 10));
    }

    private void updateMapLocation() {
        // update marker
        locationMarker.setPosition(mLocation);
        // update circle center
        rangeCircle.setCenter(mLocation);
        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLng(mLocation));
    }

    // Handle action bar help button activities
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.help_button) {
            // HELP DIALOG
            android.support.v7.app.AlertDialog alertDialog = new android.support.v7.app.AlertDialog.Builder(getActivity()).create();
            alertDialog.setTitle("Help");
            alertDialog.setMessage("Add filters for your search.");
            alertDialog.setButton(android.support.v7.app.AlertDialog.BUTTON_NEUTRAL, "OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            alertDialog.show();
        }
        return super.onOptionsItemSelected(item);
    }
}

