package margheritadonnici.com.jambuddies.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Margherita Donnici on 11/23/17.
 */

public class ExistingMember {

    @SerializedName("userId")
    @Expose
    private String userId;
    @SerializedName("instrument")
    @Expose
    private String instrument;

    private boolean isHeader;

    public ExistingMember(String instrument, boolean isHeader) {
        this.instrument = instrument;
        this.isHeader = isHeader;
    }

    public ExistingMember(String instrument, String userId) {
        this.instrument = instrument;
        this.userId = userId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getInstrument() {
        return instrument;
    }

    public void setInstrument(String instrument) {
        this.instrument = instrument;
    }

    public void setIsHeader(boolean bool) {
        this.isHeader = bool;
    }

    public boolean isHeader() {
        return isHeader;
    }

    @Override
    public String toString() {
        return "{" +
                "userId='" + userId + '\'' +
                ", instrument='" + instrument + '\'' +
                '}';
    }
}
