package margheritadonnici.com.jambuddies.data.model;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Margherita Donnici on 11/15/17.
 */

public class User {

    @SerializedName("firstName")
    @Expose
    private String firstName;
    @SerializedName("lastName")
    @Expose
    private String lastName;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("location")
    @Expose
    private List<Double> location = null;
    @SerializedName("bio")
    @Expose
    private String bio = null;
    @SerializedName("profilePicture")
    @Expose
    private String profilePicture = null;
    @SerializedName("dateOfBirth")
    @Expose
    private String dateOfBirth = null;
    @SerializedName("youtubeChannel")
    @Expose
    private String youtubeChannel = null;
    @SerializedName("fbPage")
    @Expose
    private String fbPage = null;
    @SerializedName("_id")
    @Expose
    private String _id;
    @SerializedName("groups")
    @Expose
    private List<String> groups = null;
    @SerializedName("sentRequests")
    @Expose
    private List<String> sentRequests = null;
    @SerializedName("receivedRequests")
    @Expose
    private List<String> receivedRequests = null;
    @SerializedName("instruments")
    @Expose
    private List<String> instruments = null;
    @SerializedName("token")
    @Expose
    private String token = null;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<Double> getLocation() {
        return location;
    }

    public void setLocation(List<Double> location) {
        this.location = location;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getYoutubeChannel() {
        return youtubeChannel;
    }

    public void setYoutubeChannel(String youtubeChannel) {
        this.youtubeChannel = youtubeChannel;
    }

    public String getFbPage() {
        return fbPage;
    }

    public void setFbPage(String fbPage) {
        this.fbPage = fbPage;
    }

    public String getId() {
        return _id;
    }

    public void setId(String id) {
        this._id = id;
    }

    public List<String> getGroups() {
        return groups;
    }

    public void setGroups(List<String> groups) {
        this.groups = groups;
    }

    public List<String> getInstruments() {
        return instruments;
    }

    public void setInstruments(List<String> instruments) {
        this.instruments = instruments;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public String getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        this.profilePicture = profilePicture;
    }


    @Override
    public String toString() {
        return "User{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", location=" + location +
                ", bio='" + bio + '\'' +
                ", profilePicture='" + profilePicture + '\'' +
                ", dateOfBirth='" + dateOfBirth + '\'' +
                ", youtubeChannel='" + youtubeChannel + '\'' +
                ", fbPage='" + fbPage + '\'' +
                ", id='" + _id + '\'' +
                ", groups=" + groups +
                ", instruments=" + instruments +
                ", token='" + token + '\'' +
                '}';
    }
}
