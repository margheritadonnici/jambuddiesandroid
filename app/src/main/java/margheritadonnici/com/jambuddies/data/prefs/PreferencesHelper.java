package margheritadonnici.com.jambuddies.data.prefs;

import com.google.android.gms.maps.model.LatLng;

import java.util.List;

/**
 * Created by Margherita Donnici on 11/15/17.
 */

public interface PreferencesHelper {

    void createLoginSession(String firstName, String lastName, String id, String token, boolean firstLogin,
                            List<Double> location, List<String> instruments);

    void endLoginSession();

    boolean isLoggedIn();

    List<String> getCurrentUserInstruments();

    LatLng getUserStoredLocation();

    String getCurrentUserId();

    String getToken();

    String getCurrentUserFirstName();

    boolean isFirstLogin();

    void setFirstLogin(boolean isFirstLogin);

    void updateUserSession(List<Double> location, List<String> instruments);
}
