package margheritadonnici.com.jambuddies.createprofile;


import android.content.DialogInterface;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.util.ArrayList;
import java.util.Arrays;

import margheritadonnici.com.jambuddies.R;
import margheritadonnici.com.jambuddies.data.model.User;

import static android.R.attr.data;

/**
 * A simple {@link Fragment} subclass.
 */
public class CreateProfileInstrumentsFragment extends Fragment implements InstrumentsListAdapter.ItemClickListener,
        CreateProfileInstrumentsContract.View {

    private static final String TAG = "ProfileInstruments";
    private CreateProfileInstrumentsContract.Presenter mPresenter;
    private InstrumentsListAdapter instrumentsAdapter;
    private FloatingActionButton addInstrumentFAB;
    private Button submitButton;
    private ArrayList<String> userInstruments = new ArrayList<String>();
    private String[] instrumentsArray;

    public static CreateProfileInstrumentsFragment newInstance() {
        CreateProfileInstrumentsFragment fragment = new CreateProfileInstrumentsFragment();
        return fragment;
    }

    @Override
    public void setPresenter(CreateProfileInstrumentsContract.Presenter presenter) {
        this.mPresenter = presenter;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_create_profile_instruments, container, false);

        // Bind views
        addInstrumentFAB = (FloatingActionButton) rootView.findViewById(R.id.add_instrument_fab);
        submitButton = (Button) rootView.findViewById(R.id.submit_button);

        // Load the array list of instrument strings form resources
        instrumentsArray = getResources().getStringArray(R.array.music_instruments);

        // Set up the RecyclerView
        RecyclerView recyclerView = (RecyclerView) rootView.findViewById(R.id.instruments_rv);
        int numberOfColumns = 3;
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(), numberOfColumns));
        instrumentsAdapter = new InstrumentsListAdapter(getContext(), userInstruments);
        instrumentsAdapter.setClickListener(this);
        recyclerView.setAdapter(instrumentsAdapter);

        // Add instrument button
        addInstrumentFAB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Create a new ArrayList of instruments and remove the ones already chosen from the user
                final ArrayList<String> dialogInstruments = new ArrayList<String>(Arrays.asList(instrumentsArray));
                dialogInstruments.removeAll(userInstruments);
                // Convert to array (setItems requires an array)
                final String[] newInstrumentsArray = dialogInstruments.toArray(new String[dialogInstruments.size()]);
                // Build dialog with the list of instruments
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("Select instrument")
                        .setItems(newInstrumentsArray, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                String chosenInstrument = newInstrumentsArray[which];
                                instrumentsAdapter.add(chosenInstrument);
                                dialog.dismiss();
                            }
                        })
                        .create()
                        .show();
            }
        });

        // Submit button

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPresenter.saveData(userInstruments);
            }
        });

        return rootView;
    }

    @Override
    public void onItemClick(View view, int position) {
        instrumentsAdapter.remove(position);
    }


    @Override
    public void showMessage(String msg) {
        Snackbar.make(getView(), msg, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showUpdateSuccess() {
        showMessage("Profile updated.");
        getActivity().finish();
    }
}
