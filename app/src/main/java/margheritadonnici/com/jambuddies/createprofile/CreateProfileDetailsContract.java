package margheritadonnici.com.jambuddies.createprofile;

import android.content.Intent;
import android.graphics.Bitmap;
import android.widget.ImageView;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.model.LatLng;
import margheritadonnici.com.jambuddies.BasePresenter;
import margheritadonnici.com.jambuddies.BaseView;
import margheritadonnici.com.jambuddies.utils.PlaceArrayAdapter;

/**
 * Created by Margherita Donnici on 11/19/17.
 */

public interface CreateProfileDetailsContract {

    interface View extends BaseView<Presenter> {

        void showMessage(String msg);

        void showEnableGPSDialog();
        PlaceArrayAdapter getPlaceArrayAdapter();
    }

    interface Presenter extends BasePresenter {

        LatLng getUserLocation();

        Bitmap result(int requestCode, int resultCode, Intent data, String path, int width, int height);
        String getLocationAddressString(LatLng location);
        GoogleApiClient getGoogleApiClient();
        void checkUserPermissions();

        void saveData(ImageView profilePic, String bio, String dateOfBirth, LatLng location, String youtubeChannel, String fbPage);
    }
}
