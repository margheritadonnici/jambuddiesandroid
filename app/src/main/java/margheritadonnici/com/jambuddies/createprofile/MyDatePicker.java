package margheritadonnici.com.jambuddies.createprofile;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;

import margheritadonnici.com.jambuddies.R;

public class MyDatePicker extends DialogFragment {

    DatePicker datePicker;


    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final DatePickerDialogCallbackContract hostFragment = ((DatePickerDialogCallbackContract) getTargetFragment());
        View v = getActivity().getLayoutInflater().inflate(R.layout.date_picker, null);
        datePicker = (DatePicker) v.findViewById(R.id.dialog_date_datePicker);
        return new AlertDialog.Builder(getActivity())
                .setView(v)
                .setPositiveButton("Set", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        hostFragment.setChosenDate(datePicker.getDayOfMonth(), datePicker.getMonth() + 1, datePicker.getYear());
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                })
                .create();
    }

    public interface DatePickerDialogCallbackContract {
        void setChosenDate(int day, int month, int year);
    }
}


