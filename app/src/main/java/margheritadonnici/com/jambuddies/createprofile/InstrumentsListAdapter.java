package margheritadonnici.com.jambuddies.createprofile;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import margheritadonnici.com.jambuddies.R;

/**
 * Created by Margherita Donnici on 10/24/17.
 */

// https://stackoverflow.com/questions/40587168/simple-android-grid-example-using-recyclerview-with-gridlayoutmanager-like-the

public class InstrumentsListAdapter extends RecyclerView.Adapter<margheritadonnici.com.jambuddies.createprofile.InstrumentsListAdapter.ViewHolder> {
    private Context mContext;
    private ArrayList<String> instrumentsList;
    private ItemClickListener mClickListener;


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        // Provide a reference to the views for each data item
        public ImageView icon;
        public TextView label;
        public View layout;

        public ViewHolder(View itemView) {
            super(itemView);
            layout = itemView;
            icon = (ImageView) itemView.findViewById(R.id.instrument_icon);
            label = (TextView) itemView.findViewById(R.id.instrument_name);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }

    // Pass in the instruments array into the constructor
    public InstrumentsListAdapter(Context context, ArrayList<String> instruments) {
        mContext = context;
        instrumentsList = instruments;
    }

    public void add(String item) {
        instrumentsList.add(item);
        int position = instrumentsList.size() - 1;
        if (position < 0) {
            notifyItemInserted(0);
        } else {
            notifyItemInserted(instrumentsList.size() - 1);
        }
        // TODO: if empty list?
        //  mFragment.getEmptyListView().setVisibility(View.GONE);
    }

    public void remove(int position) {
        instrumentsList.remove(position);
        notifyItemRemoved(position);
        if (instrumentsList.isEmpty()) {
            // TODO: if empty list?
            //   mFragment.getEmptyListView().setVisibility(View.VISIBLE);
        }
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View instrumentView = inflater.inflate(R.layout.instruments_grid_item, parent, false);
        margheritadonnici.com.jambuddies.createprofile.InstrumentsListAdapter.ViewHolder viewHolder =
                new margheritadonnici.com.jambuddies.createprofile.InstrumentsListAdapter.ViewHolder(instrumentView);
        return viewHolder;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final String instrument = instrumentsList.get(position);
        holder.label.setText(instrument);
        holder.icon.setImageResource(getInstrumentImage(instrument));
        //TODO: remove button listener
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return instrumentsList.size();
    }

    // allows clicks events to be caught
    void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }

    //TODO: duplicate code?
    private int getInstrumentImage(String instrument) {
        int image;
        switch (instrument) {
            case "Accordion":
                image = R.drawable.accordion;
                break;
            case "Acoustic Guitar":
                image = R.drawable.acoustic_guitar;
                break;
            case "Balalaika":
                image = R.drawable.balalaika;
                break;
            case "Banjo":
                image = R.drawable.banjo;
                break;
            case "Bass Guitar":
                image = R.drawable.bass_guitar;
                break;
            case "Bongo":
                image = R.drawable.bongo;
                break;
            case "Clarinet":
                image = R.drawable.clarinet;
                break;
            case "Cymbals":
                image = R.drawable.cymbals;
                break;
            case "Conga":
                image = R.drawable.conga;
                break;
            case "Djembe":
                image = R.drawable.djembe;
                break;
            case "Drums":
                image = R.drawable.drums;
                break;
            case "Electric Guitar":
                image = R.drawable.electric_guitar;
                break;
            case "Mixer":
                image = R.drawable.mixer;
                break;
            case "Flute":
                image = R.drawable.flute;
                break;
            case "French Horn":
                image = R.drawable.french_horn;
                break;
            case "Gong":
                image = R.drawable.gong;
                break;
            case "Harmonica":
                image = R.drawable.harmonica;
                break;
            case "Harp":
                image = R.drawable.harp;
                break;
            case "Maracas":
                image = R.drawable.maracas;
                break;
            case "Singer":
                image = R.drawable.microphone;
                break;
            case "Oboe":
                image = R.drawable.oboe;
                break;
            case "Classical Piano":
                image = R.drawable.piano;
                break;
            case "Keyboard":
                image = R.drawable.keyboard;
                break;
            case "Piccolo":
                image = R.drawable.piccolo;
                break;
            case "Saxophone":
                image = R.drawable.saxophone;
                break;
            case "Tambourine":
                image = R.drawable.tambourine;
                break;
            case "Timpani":
                image = R.drawable.timpani;
                break;
            case "Triangle":
                image = R.drawable.triangle;
                break;
            case "Trombone":
                image = R.drawable.trombone;
                break;
            case "Trumpet":
                image = R.drawable.trumpet;
                break;
            case "Tuba":
                image = R.drawable.tuba;
                break;
            case "Ukulele":
                image = R.drawable.ukelele;
                break;
            case "Violin":
                image = R.drawable.violin;
                break;
            case "Xylophone":
                image = R.drawable.xylophone;
            default:
                image = R.drawable.quaver;
                break;
        }
        return image;
    }

}


