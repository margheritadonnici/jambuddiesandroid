package margheritadonnici.com.jambuddies;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;

import margheritadonnici.com.jambuddies.createprofile.CreateProfileActivity;
import margheritadonnici.com.jambuddies.data.AppDataManager;
import margheritadonnici.com.jambuddies.findgroup.AddFilterFragment;
import margheritadonnici.com.jambuddies.findgroup.AddFilterPresenter;
import margheritadonnici.com.jambuddies.findgroup.FindGroupContract;
import margheritadonnici.com.jambuddies.findgroup.FindGroupFragment;
import margheritadonnici.com.jambuddies.findgroup.FindGroupPresenter;
import margheritadonnici.com.jambuddies.findgroup.SearchFilter;
import margheritadonnici.com.jambuddies.home.HomeContract;
import margheritadonnici.com.jambuddies.home.HomeFragment;
import margheritadonnici.com.jambuddies.home.HomePresenter;
import margheritadonnici.com.jambuddies.mygroups.DiscussionFragment;
import margheritadonnici.com.jambuddies.mygroups.DiscussionPresenter;
import margheritadonnici.com.jambuddies.mygroups.MyGroupsContract;
import margheritadonnici.com.jambuddies.mygroups.MyGroupsFragment;
import margheritadonnici.com.jambuddies.mygroups.MyGroupsPresenter;
import margheritadonnici.com.jambuddies.settings.SettingsContract;
import margheritadonnici.com.jambuddies.settings.SettingsFragment;
import margheritadonnici.com.jambuddies.settings.SettingsPresenter;
import margheritadonnici.com.jambuddies.utils.ActivityUtils;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";
    public static final int REQUEST_CREATE_GROUP = 1;
    private SettingsPresenter mSettingsPresenter;
    private MyGroupsPresenter mMyGroupsPresenter;
    private FindGroupPresenter mFindGroupPresenter;
    private DiscussionPresenter mDiscussionPresenter;
    private HomePresenter mHomePresenter;
    private AppDataManager appDataManager;
    public SearchFilter searchFilter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        appDataManager = AppDataManager.getInstance(getApplicationContext());

        // Check if user is logged in, if not then redirect to login screen
        if (!appDataManager.isLoggedIn()) {
            Log.i(TAG, "User is not logged in, redirecting to login");
            ActivityUtils.startLoginActivity(getApplicationContext());
        }
        // Initialize searchFilter
        searchFilter = new SearchFilter();

        // Bottom Navigation
        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.navigation);

        bottomNavigationView.setOnNavigationItemSelectedListener
                (new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        Fragment selectedFragment = null;
                        switch (item.getItemId()) {
                            case R.id.action_home:
                                selectedFragment = HomeFragment.newInstance();
                                mHomePresenter = new HomePresenter((HomeContract.View) selectedFragment, appDataManager, MainActivity.this);
                                break;
                            case R.id.action_my_groups:
                                selectedFragment = MyGroupsFragment.newInstance();
                                mMyGroupsPresenter = new MyGroupsPresenter((MyGroupsContract.View) selectedFragment, appDataManager);
                                break;
                            case R.id.action_find_group:
                                selectedFragment = FindGroupFragment.newInstance();
                                mFindGroupPresenter = new FindGroupPresenter((FindGroupContract.View) selectedFragment, appDataManager, MainActivity.this);
                                break;
                            case R.id.action_settings:
                                selectedFragment = SettingsFragment.newInstance();
                                mSettingsPresenter = new SettingsPresenter((SettingsContract.View) selectedFragment, appDataManager);
                                break;
                        }
                        ActivityUtils.replaceFragment(getSupportFragmentManager(), selectedFragment, R.id.frame_layout);
                        return true;
                    }
                });

        //Manually displaying the first fragment - one time only
        Fragment firstFragment = HomeFragment.newInstance();
        mHomePresenter = new HomePresenter((HomeContract.View) firstFragment, appDataManager, MainActivity.this);
        ActivityUtils.replaceFragment(getSupportFragmentManager(), firstFragment, R.id.frame_layout);
    }

    @Override
    protected void onResume() {
        super.onResume();
        // Check if it's the user first time logging in, if it is then redirect to create profile
        // Will only be true when resuming after signing up
        if (appDataManager.isFirstLogin()) {
            Log.i(TAG, "First login, redirecting to Create Profile");
            startActivity(new Intent(this, CreateProfileActivity.class));
        }
        appDataManager = AppDataManager.getInstance(this);
    }

    public void goToAddFilterFragment() {
        AddFilterFragment addFilterFragment = AddFilterFragment.newInstance();
        ActivityUtils.replaceFragment(getSupportFragmentManager(), addFilterFragment, R.id.frame_layout);
        // Create the presenter
        AddFilterPresenter addFilterPresenter = new AddFilterPresenter(addFilterFragment, this);
    }

    public void goToDiscussionFragment(String id) {
        DiscussionFragment discussionFragment = DiscussionFragment.newInstance();
        ActivityUtils.replaceFragment(getSupportFragmentManager(), discussionFragment, R.id.frame_layout);
        // Create the presenter
        mDiscussionPresenter = new DiscussionPresenter(discussionFragment, id, appDataManager);
    }

    public SearchFilter getSearchFilter() {
        return searchFilter;
    }

    public void setSearchFilter(SearchFilter searchFilter) {
        this.searchFilter = searchFilter;
    }
}
