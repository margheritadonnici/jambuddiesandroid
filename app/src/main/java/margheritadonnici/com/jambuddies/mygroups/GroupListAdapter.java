package margheritadonnici.com.jambuddies.mygroups;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.plumillonforge.android.chipview.Chip;
import com.plumillonforge.android.chipview.ChipView;
import com.plumillonforge.android.chipview.ChipViewAdapter;
import com.plumillonforge.android.chipview.OnChipClickListener;

import java.util.ArrayList;
import java.util.List;

import margheritadonnici.com.jambuddies.MainActivity;
import margheritadonnici.com.jambuddies.R;
import margheritadonnici.com.jambuddies.data.model.ExistingMemberWithUser;
import margheritadonnici.com.jambuddies.data.model.GroupWithMembers;
import margheritadonnici.com.jambuddies.utils.ImageUtils;
import margheritadonnici.com.jambuddies.utils.MainChipViewAdapter;
import margheritadonnici.com.jambuddies.utils.MemberListAdapter;
import margheritadonnici.com.jambuddies.utils.Tag;

/**
 * Created by Margherita Donnici on 11/25/17.
 */

public class GroupListAdapter extends RecyclerView.Adapter<GroupListAdapter.ViewHolder> {

    private static final String TAG = "GroupListAdapter";
    private List<GroupWithMembers> mGroups;
    private Context mContext;
    private MyGroupsFragment mFragment;
    private String mCurrentUserId;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public CardView card;
        public ImageView icon;
        public TextView txtHeader;
        public TextView txtFooter;
        public ImageButton deleteButton;
        public Button lookingButton;
        public ImageButton discussionButton;
        public ChipView chipView;
        public View layout;

        public ViewHolder(View itemView) {
            super(itemView);
            layout = itemView;
            icon = (ImageView) itemView.findViewById(R.id.icon);
            card = (CardView) itemView.findViewById(R.id.group_list_card_view);
            txtHeader = (TextView) itemView.findViewById(R.id.firstLine);
            txtFooter = (TextView) itemView.findViewById(R.id.secondLine);
            deleteButton = (ImageButton) itemView.findViewById(R.id.delete_btn);
            discussionButton = (ImageButton) itemView.findViewById(R.id.message_btn);
            lookingButton = (Button) itemView.findViewById(R.id.looking_button);
            chipView = (ChipView) itemView.findViewById(R.id.group_chipview);
        }
    }

    // Pass in the group list into the constructor
    public GroupListAdapter(Context context, MyGroupsFragment myFragment, String currentUserId, List<GroupWithMembers> groupList) {
        mGroups = groupList;
        mCurrentUserId = currentUserId;
        mContext = context;
        mFragment = myFragment;

    }

    // Easy access to the context object in the recyclerview
    private Context getContext() {
        return mContext;
    }

    public void add(GroupWithMembers item) {
        mGroups.add(item);
        int position = mGroups.size() - 1;
        if (position < 0) {
            notifyItemInserted(0);
        } else {
            notifyItemInserted(mGroups.size() - 1);
        }
        mFragment.getEmptyListView().setVisibility(View.GONE);
    }

    public void updateGroupList(List<GroupWithMembers> newlist) {
        mGroups.clear();
        mGroups.addAll(newlist);
        this.notifyDataSetChanged();
    }

    public void remove(int position) {
        mGroups.remove(position);
        notifyItemRemoved(position);
        if (mGroups.isEmpty()) {
            mFragment.getEmptyListView().setVisibility(View.VISIBLE);
        }
    }

    // Create new views (invoked by the layout manager)
    @Override
    public GroupListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View groupView;
        groupView = inflater.inflate(R.layout.group_list_element, parent, false);
        // set the view's size, margins, paddings and layout parameters
        GroupListAdapter.ViewHolder viewHolder = new GroupListAdapter.ViewHolder(groupView);
        return viewHolder;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final GroupListAdapter.ViewHolder holder, final int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        final GroupWithMembers group = mGroups.get(position);
        holder.txtHeader.setText(group.getGroupName());
        // Get the current user's instrument
        String currentUserInstrument = "";
        List<ExistingMemberWithUser> groupMembers = group.getExistingMembers();
        for (ExistingMemberWithUser member : groupMembers) {
            if (member.getUserId().getId().equals(mCurrentUserId)) {
                currentUserInstrument = member.getInstrument();
            }
        }
        holder.txtFooter.setText(currentUserInstrument);
        holder.icon.setImageResource(ImageUtils.getInstrumentImage(currentUserInstrument));
        // Delete button listener
        if (!group.getCreator().equals(mCurrentUserId)) {
            holder.deleteButton.setVisibility(View.INVISIBLE);
        } else {
            holder.deleteButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Warning dialog before delete group?
                    android.support.v7.app.AlertDialog.Builder shareDialogBuilder = new android.support.v7.app.AlertDialog.Builder(getContext());
                    shareDialogBuilder.setTitle("Delete Group?");
                    shareDialogBuilder.setMessage("The group will be deleted permanently and all data will be lost. Are you sure you want to proceed?");
                    shareDialogBuilder.setIcon(R.drawable.warning_icon);
                    shareDialogBuilder.setPositiveButton("YES I AM SURE", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            mFragment.mPresenter.deleteGroup(group.getId());
                            remove(holder.getAdapterPosition());
                            dialogInterface.dismiss();
                        }
                    });
                    shareDialogBuilder.setNegativeButton("NO, TAKE ME BACK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    });
                    android.support.v7.app.AlertDialog shareDialog = shareDialogBuilder.create();
                    shareDialog.show();
                }
            });
        }

        // Discussion button listener

        holder.discussionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity) mContext).goToDiscussionFragment(group.getId());
            }
        });

        // looking for button listener
        holder.lookingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                List<String> lookingFor = group.getLookingFor();
                //Create sequence of items
                final CharSequence[] Instruments = lookingFor.toArray(new String[lookingFor.size()]);
                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getContext());
                dialogBuilder.setTitle("Looking for...");
                dialogBuilder.setItems(Instruments, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int item) {
                        // do nothing
                    }
                });
                //Create alert dialog object via builder
                AlertDialog alertDialogObject = dialogBuilder.create();
                //Show the dialog
                alertDialogObject.show();
            }
        });

        // Chip List management
        final List<Chip> chipList = new ArrayList<>();
        // Chip list adapter
        ChipViewAdapter adapterOverride = new MainChipViewAdapter(getContext());
        holder.chipView.setAdapter(adapterOverride);
        // Add chip
        if (group.getCreator().equals(mCurrentUserId)) {
            chipList.add(new Tag("Admin", 2));
        }
        if (group.getLookingFor().isEmpty()) {
            chipList.add(new Tag("Complete", 3));
        } else {
            int totalMemberNumber = group.getExistingMembers().size() + group.getLookingFor().size();
            chipList.add(new Tag("Members: " + String.valueOf(group.getExistingMembers().size()) + "/" + String.valueOf(totalMemberNumber), 4));
        }
        // Remove selected item from list (to avoid duplicates)
        holder.chipView.setChipList(chipList);
        holder.chipView.setOnChipClickListener(new OnChipClickListener() {
            @Override
            public void onChipClick(Chip chip) {
                AlertDialog.Builder builderSingle = new AlertDialog.Builder(mContext);
                builderSingle.setTitle("Group Members");
                final MemberListAdapter memberListAdapter = new MemberListAdapter(mContext, group.getExistingMembers());
                builderSingle.setPositiveButton("CLOSE", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

                builderSingle.setAdapter(memberListAdapter, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        // do nothing
                    }
                });
                builderSingle.show();
            }
        });
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mGroups.size();
    }
}
