package margheritadonnici.com.jambuddies.mygroups;

import android.content.Intent;

import java.util.List;

import margheritadonnici.com.jambuddies.BasePresenter;
import margheritadonnici.com.jambuddies.BaseView;
import margheritadonnici.com.jambuddies.data.model.Group;
import margheritadonnici.com.jambuddies.data.model.GroupWithMembers;

/**
 * Created by Margherita Donnici on 11/24/17.
 */

public interface MyGroupsContract {

    interface View extends BaseView<Presenter> {
        void showLoadingDialog(String msg);

        void hideLoadingDialog();

        void displayData(List<GroupWithMembers> groupList, String currentUserId);

        void showMessage(String msg);
    }

    interface Presenter extends BasePresenter {
        String getCurrentUserId();

        void deleteGroup(String groupId);

        void onCreateGroupResult(int requestCode, int resultCode, Intent data);
    }

}
