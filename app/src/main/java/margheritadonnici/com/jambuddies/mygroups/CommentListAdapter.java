package margheritadonnici.com.jambuddies.mygroups;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import margheritadonnici.com.jambuddies.R;
import margheritadonnici.com.jambuddies.data.model.Comment;
import margheritadonnici.com.jambuddies.utils.ImageUtils;

/**
 * Created by Margherita Donnici on 1/11/18.
 */

public class CommentListAdapter extends RecyclerView.Adapter<CommentListAdapter.ViewHolder> {

    private static final String TAG = "GroupListAdapter";
    private List<Comment> mCommentList;
    private Context mContext;
    private DiscussionFragment mFragment;

    public CommentListAdapter(List<Comment> mCommentList, Context mContext, DiscussionFragment fragment) {
        this.mFragment = fragment;
        this.mCommentList = mCommentList;
        this.mContext = mContext;
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder

    public class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public ImageView icon;
        public TextView name;
        public TextView date;
        public TextView message;
        public View layout;

        public ViewHolder(View itemView) {
            super(itemView);
            layout = itemView;
            icon = (ImageView) itemView.findViewById(R.id.comment_image);
            name = (TextView) itemView.findViewById(R.id.comment_author);
            date = (TextView) itemView.findViewById(R.id.comment_date);
            message = (TextView) itemView.findViewById(R.id.comment_body);
        }

    }

    public void add(Comment item) {
        mCommentList.add(item);
        int position = mCommentList.size() - 1;
        if (position < 0) {
            notifyItemInserted(0);
        } else {
            notifyItemInserted(mCommentList.size() - 1);
        }
        mFragment.getEmptyListView().setVisibility(View.GONE);
    }

    public void updateCommentList(List<Comment> newlist) {
        mCommentList.clear();
        mCommentList.addAll(newlist);
        this.notifyDataSetChanged();
    }

    public void remove(int position) {
        mCommentList.remove(position);
        notifyItemRemoved(position);
        if (mCommentList.isEmpty()) {
            mFragment.getEmptyListView().setVisibility(View.VISIBLE);
        }
    }

    // Create new views (invoked by the layout manager)
    @Override
    public CommentListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View commentView;
        commentView = inflater.inflate(R.layout.comment, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder viewHolder = new CommentListAdapter.ViewHolder(commentView);
        return viewHolder;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final CommentListAdapter.ViewHolder holder, final int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        final Comment comment = mCommentList.get(position);
        holder.message.setText(comment.getMsg());
        // Format date in current timezone
        DateFormat utcFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        DateFormat localFormat = new SimpleDateFormat("EEE dd MMM yyyy hh:mm aaa");
        utcFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        localFormat.setTimeZone(TimeZone.getDefault());
        String formattedDate;
        try {
            Date date = utcFormat.parse(comment.getCreated());
            formattedDate = localFormat.format(date);
        } catch (ParseException e) {
            formattedDate = "";
        }
        holder.date.setText(formattedDate);
        holder.name.setText(comment.getAuthorName());
        holder.icon.setImageResource(ImageUtils.getInstrumentImage(comment.getInstrument()));
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mCommentList.size();
    }

    // Easy access to the context object in the recyclerview
    private Context getContext() {
        return mContext;
    }
}
