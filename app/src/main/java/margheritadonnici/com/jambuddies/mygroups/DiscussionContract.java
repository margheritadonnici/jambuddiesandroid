package margheritadonnici.com.jambuddies.mygroups;

import java.util.List;

import margheritadonnici.com.jambuddies.BasePresenter;
import margheritadonnici.com.jambuddies.BaseView;
import margheritadonnici.com.jambuddies.data.model.Comment;

/**
 * Created by Margherita Donnici on 1/11/18.
 */

public interface DiscussionContract {

    interface View extends BaseView<Presenter> {

        void showLoadingDialog(String msg);

        void hideLoadingDialog();

        void displayData(List<Comment> commentList);

        void showMessage(String msg);
    }

    interface Presenter extends BasePresenter {

        List<Comment> getGroupComments();

        void submitComment(String msg);
    }
}
