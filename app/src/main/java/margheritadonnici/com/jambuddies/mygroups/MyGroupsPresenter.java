package margheritadonnici.com.jambuddies.mygroups;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import margheritadonnici.com.jambuddies.MainActivity;
import margheritadonnici.com.jambuddies.data.AppDataManager;
import margheritadonnici.com.jambuddies.data.DataManager;
import margheritadonnici.com.jambuddies.data.model.ExistingMember;
import margheritadonnici.com.jambuddies.data.model.Group;
import margheritadonnici.com.jambuddies.data.model.UserWithGroups;

/**
 * Created by Margherita Donnici on 11/24/17.
 */

public class MyGroupsPresenter implements MyGroupsContract.Presenter {

    private static final String TAG = "MyGroupsPresenter";
    private final MyGroupsContract.View mMyGroupsView;
    private final AppDataManager mDataManager;

    private List<Group> userGroupList = new ArrayList<Group>();

    public MyGroupsPresenter(MyGroupsContract.View mMyGroupsView, AppDataManager mDataManager) {
        this.mMyGroupsView = mMyGroupsView;
        mMyGroupsView.setPresenter(this);
        this.mDataManager = mDataManager;
    }


    @Override
    public void start() {
        mMyGroupsView.showLoadingDialog("Fetching groups...");
        mDataManager.getUserByIdApiCall(mDataManager.getCurrentUserId(), new DataManager.GetUserByIdCallback() {
            @Override
            public void onGetUserSuccess(UserWithGroups user) {
                mMyGroupsView.hideLoadingDialog();
                mMyGroupsView.displayData(user.getGroups(), mDataManager.getCurrentUserId());
            }

            @Override
            public void onGetUserFailure(String error) {
                mMyGroupsView.hideLoadingDialog();
                mMyGroupsView.showMessage("Unable to fetch groups");
            }
        });
    }

    @Override
    public String getCurrentUserId() {
        return mDataManager.getCurrentUserId();
    }

    @Override
    public void deleteGroup(String groupId) {
        mMyGroupsView.showLoadingDialog("Deleting group...");
        mDataManager.deleteGroupApiCall(groupId, new DataManager.DeleteGroupCallback() {
            @Override
            public void onDeleteGroupSuccess(String msg) {
                mMyGroupsView.hideLoadingDialog();
                mMyGroupsView.showMessage(msg);
                start();
            }

            @Override
            public void onDeleteGroupFailure(String error) {
                mMyGroupsView.hideLoadingDialog();
            }
        });
    }

    @Override
    public void onCreateGroupResult(int requestCode, int resultCode, Intent data) {
        // If CreateGroup was successful
        if (MainActivity.REQUEST_CREATE_GROUP == requestCode && Activity.RESULT_OK == resultCode) {
            // Update Group List
            this.start();
        }

    }
}
