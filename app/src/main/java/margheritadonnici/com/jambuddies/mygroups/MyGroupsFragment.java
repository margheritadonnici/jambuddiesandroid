package margheritadonnici.com.jambuddies.mygroups;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import margheritadonnici.com.jambuddies.MainActivity;
import margheritadonnici.com.jambuddies.R;
import margheritadonnici.com.jambuddies.creategroup.CreateGroupActivity;
import margheritadonnici.com.jambuddies.data.model.Group;
import margheritadonnici.com.jambuddies.data.model.GroupWithMembers;

public class MyGroupsFragment extends Fragment implements MyGroupsContract.View {

    private static final String TAG = "MyGroupsFragment";
    MyGroupsContract.Presenter mPresenter;

    ProgressDialog mProgressDialog;
    RecyclerView groupsRv;
    GroupListAdapter groupListAdapter;
    TextView emptyListView;
    String mCurrentUserId;
    List<GroupWithMembers> mGroupList = new ArrayList<GroupWithMembers>();

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment MyGroupsFragment.
     */
    public static MyGroupsFragment newInstance() {
        MyGroupsFragment fragment = new MyGroupsFragment();
        return fragment;
    }

    @Override
    public void setPresenter(MyGroupsContract.Presenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPresenter.start();
        mCurrentUserId = mPresenter.getCurrentUserId();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_my_groups, container, false);

        // Set ActionBar Title
        getActivity().setTitle("My Groups");

        // Recycler View
        groupsRv = (RecyclerView) view.findViewById(R.id.group_list);
        emptyListView = (TextView) view.findViewById(R.id.empty_group_list_view);
        // Get the TextView to display if our dataset is empty
        if (mGroupList.isEmpty()) {
            emptyListView.setVisibility(View.VISIBLE);
        }
        // Create adapter passing in the members list
        groupListAdapter = new GroupListAdapter(getContext(), this, mCurrentUserId, mGroupList);
        // Set layout manager to position the items
        groupsRv.setLayoutManager(new LinearLayoutManager(getActivity()));
        // Attach the adapter to the recyclerview to populate items
        groupsRv.setAdapter(groupListAdapter);

        // FAB Listener
        FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.newGroupFAB);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Start Create Group Activity
                startActivityForResult(new Intent(getActivity(), CreateGroupActivity.class), MainActivity.REQUEST_CREATE_GROUP);
            }
        });
        return view;

    }

    public TextView getEmptyListView() {
        return emptyListView;
    }

    @Override
    public void showLoadingDialog(String msg) {
        mProgressDialog = new ProgressDialog(getContext(), R.style.Dark_Dialog);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage(msg);
        mProgressDialog.show();
    }

    @Override
    public void hideLoadingDialog() {
        if (mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }


    @Override
    public void displayData(List<GroupWithMembers> groupList, String currentUserId) {
        mCurrentUserId = currentUserId;
        mGroupList = groupList;
        groupListAdapter.updateGroupList(groupList);
        if (!mGroupList.isEmpty()) {
            emptyListView.setVisibility(View.GONE);
        }
    }

    @Override
    public void showMessage(String msg) {
        Snackbar.make(getView(), msg, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        mPresenter.onCreateGroupResult(requestCode, resultCode, data);
    }
}
