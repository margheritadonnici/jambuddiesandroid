package margheritadonnici.com.jambuddies.mygroups;


import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import margheritadonnici.com.jambuddies.R;
import margheritadonnici.com.jambuddies.data.model.Comment;

/**
 * A simple {@link Fragment} subclass.
 */
public class DiscussionFragment extends Fragment implements DiscussionContract.View {

    private static final String TAG = "DiscussionFragment";
    DiscussionContract.Presenter mPresenter;

    ProgressDialog mProgressDialog;
    RecyclerView mCommentsRv;
    CommentListAdapter mCommentListAdapter;
    TextView emptyListView;
    ImageButton sendButton;
    List<Comment> mCommentList = new ArrayList<Comment>();
    EditText mMessageBox;


    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment MyGroupsFragment.
     */
    public static DiscussionFragment newInstance() {
        DiscussionFragment fragment = new DiscussionFragment();
        return fragment;
    }

    @Override
    public void setPresenter(DiscussionContract.Presenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPresenter.start();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View rootView = inflater.inflate(R.layout.fragment_discussion, container, false);

        // Bind views
        emptyListView = rootView.findViewById(R.id.empty_list_view);
        mCommentsRv = rootView.findViewById(R.id.comment_rv);
        sendButton = rootView.findViewById(R.id.button_chatbox_send);
        mMessageBox = rootView.findViewById(R.id.edittext_chatbox);


        // Comments RecycleView
        // If list is empty, display empty list message
        if (mCommentList.isEmpty()) {
            emptyListView.setVisibility(View.VISIBLE);
        }
        // Create adapter
        mCommentListAdapter = new CommentListAdapter(mCommentList, getContext(), this);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        mCommentsRv.setLayoutManager(layoutManager);
        mCommentsRv.setAdapter(mCommentListAdapter);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(mCommentsRv.getContext(), layoutManager.getOrientation());
        mCommentsRv.addItemDecoration(dividerItemDecoration);

        // SEND BUTTON
        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String newComment = mMessageBox.getText().toString();
                // Close keyboard
                InputMethodManager in = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                in.hideSoftInputFromWindow(rootView.getApplicationWindowToken(), 0);
                mPresenter.submitComment(newComment);
            }
        });
        return rootView;
    }

    @Override
    public void displayData(List<Comment> commentList) {
        mMessageBox.setText(""); // clear text from message box
        mCommentList = commentList;
        mCommentListAdapter.updateCommentList(mCommentList);
        if (!mCommentList.isEmpty()) {
            emptyListView.setVisibility(View.GONE);
        } else {
            emptyListView.setVisibility(View.VISIBLE);
        }
        mCommentListAdapter.notifyDataSetChanged();
        if (!mCommentList.isEmpty()) {
            // Scroll to bottom
            mCommentsRv.smoothScrollToPosition(mCommentList.size() - 1);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        // Refresh search results with current filter
        mPresenter.getGroupComments();
    }

    public TextView getEmptyListView() {
        return emptyListView;
    }

    @Override
    public void showLoadingDialog(String msg) {
        mProgressDialog = new ProgressDialog(getContext(), R.style.Dark_Dialog);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage(msg);
        mProgressDialog.show();
    }

    @Override
    public void hideLoadingDialog() {
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
        }
    }

    @Override
    public void showMessage(String msg) {
        Snackbar.make(getView(), msg, Snackbar.LENGTH_LONG).show();
    }


}
