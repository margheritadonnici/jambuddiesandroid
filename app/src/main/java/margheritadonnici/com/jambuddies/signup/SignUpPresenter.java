package margheritadonnici.com.jambuddies.signup;

import android.content.Intent;
import android.util.Log;

import java.util.List;

import margheritadonnici.com.jambuddies.data.AppDataManager;
import margheritadonnici.com.jambuddies.data.DataManager;

/**
 * Created by Margherita Donnici on 10/28/17.
 */

public class SignUpPresenter implements SignUpContract.Presenter {

    private static final String TAG = "SignUpPresenter";

    private final SignUpContract.View mSignUpView;
    private final AppDataManager mDataManager;

    public SignUpPresenter(SignUpContract.View signUpView, AppDataManager dataManager) {
        mSignUpView = signUpView;
        this.mDataManager = dataManager;
        mSignUpView.setPresenter(this);
    }

    @Override
    public void start() {
        //TODO
    }

    @Override
    public void signUp(final String firstName, final String lastName, String email, String password) {

        // Form validation
        boolean valid = true;

        if (firstName.isEmpty()) {
            mSignUpView.showError("first_name");
            valid = false;
        } else {
            mSignUpView.removeError("first_name");
        }

        if (lastName.isEmpty()) {
            mSignUpView.showError("last_name");
            valid = false;
        } else {
            mSignUpView.removeError("last_name");
        }

        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            mSignUpView.showError("email");
            valid = false;
        } else {
            mSignUpView.removeError("email");
        }

        if (password.isEmpty() || password.length() < 4) {
            mSignUpView.showError("password");
            valid = false;
        } else {
            mSignUpView.removeError("email");
        }

        if (!valid) {
            // If form is not valid, do not continue
            return;
        }

        // Form is valid: proceed with sign up
        mSignUpView.showLoadingDialog();

        // Sign Up
        mDataManager.createUserApiCall(firstName, lastName, email, password, new DataManager.SignUpCallback() {
            @Override
            public void onSignUpSuccess(String userId, String token, List<Double> location, List<String> instruments) {
                // Prepare onSignUpResult intent with data to send to login activity
                Log.i(TAG, "userId: " + userId);
                Log.i(TAG, "userId: " + token);
                Intent resultIntent = new Intent();
                resultIntent.putExtra("firstName", firstName);
                resultIntent.putExtra("lastName", lastName);
                resultIntent.putExtra("userId", userId);
                resultIntent.putExtra("token", token);
                mSignUpView.showSignUpSuccess(resultIntent);
            }

            @Override
            public void onSignUpFailure(String error) {
                mSignUpView.showSignUpFailure(error);
            }
        });
    }
}
