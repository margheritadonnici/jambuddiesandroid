package margheritadonnici.com.jambuddies.signup;

import android.content.Intent;

import margheritadonnici.com.jambuddies.BasePresenter;
import margheritadonnici.com.jambuddies.BaseView;

/**
 * Created by Margherita Donnici on 10/28/17.
 */

public interface SignUpContract {

    /**
     * This specifies the contract between the Sign up view and the Sign up presenter.
     */

    interface View extends BaseView<SignUpContract.Presenter> {
        void showError(String error);

        void removeError(String error);

        void showLoadingDialog();

        void showSignUpSuccess(Intent resultIntent);

        void showSignUpFailure(String error);

    }

    interface Presenter extends BasePresenter {
        void signUp(String firstName, String lastName, String email, String password);
    }

}
