package margheritadonnici.com.jambuddies.signup;

import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import margheritadonnici.com.jambuddies.R;
import margheritadonnici.com.jambuddies.data.AppDataManager;
import margheritadonnici.com.jambuddies.utils.ActivityUtils;

public class SignUpActivity extends AppCompatActivity {

    private static final String TAG = "SignupActivity";

    private SignUpPresenter mSignUpPresenter;

    private EditText emailText;
    private EditText passwordText;
    private Button signUpButton;
    private TextView loginLink;
    private TextInputLayout emailTIL;
    private TextInputLayout passwordTIL;
    private EditText usernameText;
    private TextInputLayout usernameTIL;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        //Set up the toolbar
        getSupportActionBar().hide();

        // Load LoginFragment
        SignUpFragment signUpFragment = (SignUpFragment) getSupportFragmentManager().findFragmentById(R.id.signup_frame_layout);
        if (signUpFragment == null) {
            // Create Fragment
            signUpFragment = SignUpFragment.newInstance();
            ActivityUtils.addFragmentToActivity(getSupportFragmentManager(), signUpFragment, R.id.signup_frame_layout);
        }

        // Create the presenter
        mSignUpPresenter = new SignUpPresenter(signUpFragment, AppDataManager.getInstance(getApplicationContext()));

    }
}
