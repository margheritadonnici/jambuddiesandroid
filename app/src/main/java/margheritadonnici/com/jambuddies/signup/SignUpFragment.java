package margheritadonnici.com.jambuddies.signup;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import margheritadonnici.com.jambuddies.R;
import margheritadonnici.com.jambuddies.createprofile.CreateProfileActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class SignUpFragment extends Fragment implements SignUpContract.View {

    SignUpContract.Presenter mPresenter;

    private EditText emailText;
    private EditText passwordText;
    private Button signUpButton;
    private TextView loginLink;
    private TextInputLayout emailTIL;
    private TextInputLayout passwordTIL;
    private EditText firstNameText;
    private EditText lastNameText;
    private TextInputLayout mFirstNameTIL;
    private TextInputLayout mLastNameTIL;
    private ProgressDialog mProgressDialog;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment SignUpFragment.
     */
    public static SignUpFragment newInstance() {
        SignUpFragment fragment = new SignUpFragment();
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_sign_up, container, false);

        // Bind views
        firstNameText = (EditText) rootView.findViewById(R.id.signup_firstname);
        lastNameText = (EditText) rootView.findViewById(R.id.signup_lastname);
        emailText = (EditText) rootView.findViewById(R.id.signup_email);
        passwordText = (EditText) rootView.findViewById(R.id.signup_password);
        signUpButton = (Button) rootView.findViewById(R.id.btn_signup);
        loginLink = (TextView) rootView.findViewById(R.id.link_login);
        emailTIL = (TextInputLayout) rootView.findViewById(R.id.signup_email_til);
        passwordTIL = (TextInputLayout) rootView.findViewById(R.id.signup_password_til);
        mFirstNameTIL = (TextInputLayout) rootView.findViewById(R.id.signup_firstname_til);
        mLastNameTIL = (TextInputLayout) rootView.findViewById(R.id.signup_lastname_til);

        // Login link styling
        SpannableString styledString = new SpannableString("Already a member? Login");
        styledString.setSpan(new StyleSpan(Typeface.BOLD), 18, 23, 0);
        loginLink.setText(styledString);

        // Sign Up Button
        signUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String firstName = firstNameText.getText().toString();
                String lastName = lastNameText.getText().toString();
                String email = emailText.getText().toString();
                String password = passwordText.getText().toString();

                signUpButton.setEnabled(false);
                mPresenter.signUp(firstName, lastName, email, password);
//                Intent createProfileIntent = new Intent(getActivity(), CreateProfileActivity.class);
//                startActivity(createProfileIntent);
            }
        });

        loginLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Finish the registration screen and return to the Login activity
                getActivity().finish();
            }
        });

        return rootView;
    }

    @Override
    public void setPresenter(SignUpContract.Presenter presenter) {
        mPresenter = presenter;
    }

    // Method to show errors
    @Override
    public void showError(String error) {
        switch (error) {
            case "first_name":
                mFirstNameTIL.setError("First name required");
                break;
            case "last_name":
                mLastNameTIL.setError("Last name required");
                break;
            case "email":
                emailTIL.setError("Invalid email address");
                break;
            case "password":
                passwordTIL.setError("Password must be at least 4 characters");
                break;
            case "signup_failed":
                if (mProgressDialog != null) {
                    // If loading dialog was showing, dismiss it
                    mProgressDialog.dismiss();
                }
                showMessage("Sign up failed");
                signUpButton.setEnabled(true);
            default:
                showMessage("Sign up failed");
        }
    }

    @Override
    // Removes errors from form fields
    public void removeError(String error) {
        switch (error) {
            case "first_name":
                mFirstNameTIL.setError("");
                break;
            case "last_name":
                mLastNameTIL.setError("");
                break;
            case "email":
                emailTIL.setError("");
                break;
            case "password":
                passwordTIL.setError("");
                break;
            default:
                mFirstNameTIL.setError("");
                mLastNameTIL.setError("");
                emailTIL.setError("");
                passwordTIL.setError("");
        }
    }

    @Override
    public void showLoadingDialog() {
        mProgressDialog = new ProgressDialog(getActivity(), R.style.Dark_Dialog);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Creating Account...");
        mProgressDialog.show();
    }

    @Override
    public void showSignUpSuccess(Intent resultIntent) {
        if (mProgressDialog != null) {
            // If loading dialog was showing, dismiss it
            mProgressDialog.dismiss();
        }
        signUpButton.setEnabled(true);
        // Send onSignUpResult to LoginActivity
        showMessage("Sign up was successful");
        getActivity().setResult(Activity.RESULT_OK, resultIntent);
        getActivity().finish();
    }

    @Override
    public void showSignUpFailure(String error) {
        if (mProgressDialog != null) {
            // If loading dialog was showing, dismiss it
            mProgressDialog.dismiss();
        }
        signUpButton.setEnabled(true);
        showMessage("There was an error signing up: " + error);
    }

    private void showMessage(String message) {
        Snackbar.make(getView(), message, Snackbar.LENGTH_LONG).show();
    }
}
