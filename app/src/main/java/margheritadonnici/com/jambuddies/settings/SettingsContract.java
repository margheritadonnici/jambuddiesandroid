package margheritadonnici.com.jambuddies.settings;

import margheritadonnici.com.jambuddies.BasePresenter;
import margheritadonnici.com.jambuddies.BaseView;

/**
 * Created by Margherita Donnici on 11/17/17.
 */

public interface SettingsContract {
    interface View extends BaseView<Presenter> {

    }

    interface Presenter extends BasePresenter {
        void onLogoutClick();
    }
}
