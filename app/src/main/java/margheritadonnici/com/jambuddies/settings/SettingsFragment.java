package margheritadonnici.com.jambuddies.settings;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Arrays;
import java.util.List;

import margheritadonnici.com.jambuddies.MainActivity;
import margheritadonnici.com.jambuddies.R;
import margheritadonnici.com.jambuddies.utils.ActivityUtils;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link SettingsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SettingsFragment extends Fragment implements SettingsContract.View {

    private static final String TAG = "SettingsFragment";

    SettingsContract.Presenter mPresenter;
    private SettingsListAdapter mSettingsListAdapter;

    private RecyclerView mSettingsRV;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment SettingsFragment.
     */
    public static SettingsFragment newInstance() {
        SettingsFragment fragment = new SettingsFragment();
        return fragment;
    }

    @Override
    public void setPresenter(@NonNull SettingsContract.Presenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_settings, container, false);
        // Set ActionBar Title
        getActivity().setTitle("Settings");

        mSettingsRV = (RecyclerView) rootView.findViewById(R.id.settings_rv);
        mSettingsRV.setLayoutManager(new LinearLayoutManager(getContext()));
        List<String> settingsList = Arrays.asList(getResources().getStringArray(R.array.settings_list));
        mSettingsListAdapter = new SettingsListAdapter(settingsList);
        mSettingsRV.setAdapter(mSettingsListAdapter);
        mSettingsRV.addItemDecoration(new DividerItemDecoration(getContext(), LinearLayoutManager.VERTICAL));

        return rootView;
    }


    // Adapter Inner Class for Settings list
    private class SettingsListAdapter extends RecyclerView.Adapter<SettingsListAdapter.ViewHolder> {

        private static final String TAG = "SettingsListAdapter";
        private List<String> values;

        // Provide a reference to the views for each data item
        // Complex data items may need more than one view per item, and
        // you provide access to all the views for a data item in a view holder
        public class ViewHolder extends RecyclerView.ViewHolder {
            // each data item is just a string in this case
            public TextView itemText;
            public View layout;

            public ViewHolder(View v) {
                super(v);
                layout = v;
                itemText = (TextView) v.findViewById(android.R.id.text1);
            }
        }

        // Provide a suitable constructor (depends on the kind of dataset)
        public SettingsListAdapter(List<String> settingsList) {
            values = settingsList;
        }

        // Create new views (invoked by the layout manager)
        @Override
        public SettingsListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            // create a new view
            LayoutInflater inflater = LayoutInflater.from(
                    parent.getContext());
            View v =
                    inflater.inflate(android.R.layout.simple_list_item_1, parent, false);
            // set the view's size, margins, paddings and layout parameters
            ViewHolder vh = new ViewHolder(v);
            return vh;
        }

        @Override
        public void onBindViewHolder(SettingsListAdapter.ViewHolder holder, final int position) {
            // - get element from your dataset at this position
            // - replace the contents of the view with that element
            final String name = values.get(position);
            holder.itemText.setText(name);
            holder.itemText.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (values.get(position).equals("Logout")) {
                        mPresenter.onLogoutClick();
                        // Create new MainActivity and destroy the old one
                        startActivity(new Intent(getActivity(), MainActivity.class));
                        getActivity().finish();
                    }
                    if (values.get(position).equals("Info")) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                        builder.setMessage("Creator: Margherita Donnici (margheritadonnici@gmail.com) \n Version: 1.0")
                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.dismiss();
                                    }
                                })
                                .create().show();
                    }
                }
            });
        }

        // Return the size of your dataset (invoked by the layout manager)
        @Override
        public int getItemCount() {
            return values.size();
        }

    }
}
