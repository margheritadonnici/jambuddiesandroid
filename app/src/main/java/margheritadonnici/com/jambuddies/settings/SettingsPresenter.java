package margheritadonnici.com.jambuddies.settings;

import margheritadonnici.com.jambuddies.data.AppDataManager;

/**
 * Created by Margherita Donnici on 11/17/17.
 */

public class SettingsPresenter implements SettingsContract.Presenter {

    private static final String TAG = "SettingsPresenter";

    private final SettingsContract.View mSettingsView;
    private final AppDataManager mDataManager;

    public SettingsPresenter(SettingsContract.View settingsView, AppDataManager mDataManager) {
        this.mSettingsView = settingsView;
        this.mSettingsView.setPresenter(this);
        this.mDataManager = mDataManager;
    }

    @Override
    public void start() {
        //TODO
    }

    @Override
    public void onLogoutClick() {
        mDataManager.endLoginSession();
    }
}
