package margheritadonnici.com.jambuddies.creategroup;

import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;

import margheritadonnici.com.jambuddies.data.model.Group;
import margheritadonnici.com.jambuddies.utils.LocationServicesHelper;

/**
 * Created by Margherita Donnici on 10/29/17.
 */

public class CreateGroupDetailsPresenter implements CreateGroupDetailsContract.Presenter,
        ActivityCompat.OnRequestPermissionsResultCallback,
        GoogleApiClient.OnConnectionFailedListener,
        GoogleApiClient.ConnectionCallbacks {

    private static final String TAG = "GroupDetailsPresenter";

    private final CreateGroupDetailsContract.View mGroupDetailsView;

    private LocationServicesHelper locationHelper;

    private Group mNewGroup;


    public CreateGroupDetailsPresenter(CreateGroupDetailsContract.View groupDetailsView, Context callerContext) {
        mGroupDetailsView = groupDetailsView;
        groupDetailsView.setPresenter(this);
        locationHelper = new LocationServicesHelper(callerContext, this);
        mNewGroup = ((CreateGroupActivity) callerContext).getNewGroup();
    }

    @Override
    public void start() {
        // Check availability of play services
        if (locationHelper.checkPlayServices()) {
            // Building the GoogleApi client
            locationHelper.buildGoogleApiClient();
        }
    }

    @Override
    public LatLng getUserLocation() {
        Location userLocation = locationHelper.getLocation();
        LatLng returnLocation = null;
        checkUserPermissions();
        if (!locationHelper.isGPSEnabled()) {
            mGroupDetailsView.showEnableGPSDialog();
        }
        if (userLocation != null) {
            returnLocation = new LatLng(userLocation.getLatitude(), userLocation.getLongitude());
        }
        return returnLocation;
    }

    @Override
    public String getLocationAddressString(LatLng location) {
        Address locationAddress = locationHelper.getAddress(location.latitude, location.longitude);
        return locationHelper.getAddressString(locationAddress);
    }

    @Override
    public GoogleApiClient getGoogleApiClient() {
        return locationHelper.getGoogleApiClient();
    }

    @Override
    public void checkUserPermissions() {
        locationHelper.checkpermission();
    }

    @Override
    public void result(int requestCode, int resultCode, Intent data) {
        locationHelper.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean validateData(String groupName, String groupDescription, Boolean coverBand, String coverArtists, ArrayList<String> genreList, LatLng userLocation) {
        boolean valid = false;
        // Check for errors
        if (groupName.isEmpty()) {
            mGroupDetailsView.showError("name");
        }
        if (userLocation == null) {
            mGroupDetailsView.showError("location");
        }
        if (!groupName.isEmpty() && userLocation != null) {
            // Fill in data in Group instance
            mNewGroup.setGroupName(groupName);
            mNewGroup.setDescription(groupDescription);
            mNewGroup.setCoverBand(coverBand);
            mNewGroup.setCoverArtists(coverArtists);
            mNewGroup.setGenres(genreList);
            List<Double> locationList = new ArrayList<Double>();
            locationList.add(userLocation.longitude);
            locationList.add(userLocation.latitude);
            mNewGroup.setLocation(locationList);
            valid = true;
        }
        return valid;
    }

    // This method is called when the user responds to the permission request
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        locationHelper.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    /**
     * Google connection callback methods
     */
    @Override
    public void onConnectionFailed(ConnectionResult result) {
        Log.e(TAG, "Google Places API connection failed with error code" + result.getErrorCode());
        mGroupDetailsView.showMessage("Google Places API connection failed");

    }

    @Override
    public void onConnected(Bundle arg0) {
        Log.i(TAG, "Google Places API connected.");
        // Once connected with google api, set the location AutoComplete adapter
        mGroupDetailsView.getPlaceArrayAdapter().setGoogleApiClient(locationHelper.getGoogleApiClient());

    }

    @Override
    public void onConnectionSuspended(int arg0) {
        Log.e(TAG, "Google Places API connection suspended.");
        mGroupDetailsView.getPlaceArrayAdapter().setGoogleApiClient(null);
        // Reconnect
        locationHelper.connectApiClient();
    }
}
