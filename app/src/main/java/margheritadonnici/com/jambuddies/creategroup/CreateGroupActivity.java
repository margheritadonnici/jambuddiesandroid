package margheritadonnici.com.jambuddies.creategroup;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import margheritadonnici.com.jambuddies.R;
import margheritadonnici.com.jambuddies.data.model.Group;
import margheritadonnici.com.jambuddies.utils.ActivityUtils;

public class CreateGroupActivity extends AppCompatActivity {

    private static final String TAG = "CreateGroupActivity";
    private CreateGroupDetailsPresenter mGroupDetailsPresenter;
    public Group newGroup;
    private LookingForPresenter mLookingForPresenter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_group);

        // Set ActionBar Title
        setTitle("Create Group");

        // Initialize new MusicGroup instance
        newGroup = new Group();

        // Load first form fragment
        CreateGroupDetailsFragment groupDetailsFragment = (CreateGroupDetailsFragment) getSupportFragmentManager().findFragmentById(R.id.create_group_frame_layout);
        if (groupDetailsFragment == null) {
            // Create Fragment
            groupDetailsFragment = CreateGroupDetailsFragment.newInstance();
            ActivityUtils.addFragmentToActivity(getSupportFragmentManager(), groupDetailsFragment, R.id.create_group_frame_layout);
        }

        // Create the presenter
        mGroupDetailsPresenter = new CreateGroupDetailsPresenter(groupDetailsFragment, this);

    }

    public void goToPickInstrumentsFragment() {
        LookingForFragment lookingForFragment = LookingForFragment.newInstance();
        ActivityUtils.replaceFragment(getSupportFragmentManager(), lookingForFragment, R.id.create_group_frame_layout);
        // Create the presenter
        mLookingForPresenter = new LookingForPresenter(lookingForFragment, this);
    }

    public Group getNewGroup() {
        return newGroup;
    }

    public void setNewGroup(Group newGroup) {
        this.newGroup = newGroup;
    }
}
