package margheritadonnici.com.jambuddies.creategroup;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

import margheritadonnici.com.jambuddies.data.AppDataManager;
import margheritadonnici.com.jambuddies.data.DataManager;
import margheritadonnici.com.jambuddies.data.model.ExistingMember;
import margheritadonnici.com.jambuddies.data.model.Group;
import margheritadonnici.com.jambuddies.data.model.User;

/**
 * Created by Margherita Donnici on 11/23/17.
 */

public class LookingForPresenter implements LookingForContract.Presenter {

    private static final String TAG = "LookingForPresenter";
    private LookingForContract.View mLookingForView;
    private AppDataManager mDataManager;
    private final Context mCallerContext;
    private String[] userInstruments;
    private Group newGroup;

    @Override
    public String[] getUserInstruments() {
        return userInstruments;
    }

    public LookingForPresenter(LookingForContract.View mLookingForView, Context callerContext) {
        this.mLookingForView = mLookingForView;
        mLookingForView.setPresenter(this);
        this.mDataManager = AppDataManager.getInstance(callerContext);
        this.mCallerContext = callerContext;
    }

    @Override
    public void start() {
        fetchUserInstruments();
    }

    @Override
    public void submitData(List<ExistingMember> membersList) {
        mLookingForView.showLoadingDialog("Creating group...");
        newGroup = ((CreateGroupActivity) mCallerContext).getNewGroup();
        String creatorId = mDataManager.getCurrentUserId();
        ArrayList<ExistingMember> existingMembers = new ArrayList<ExistingMember>();
        List<String> lookingForList = new ArrayList<String>();
        for (ExistingMember member : membersList) {
            if (member.isHeader()) {
                member.setUserId(creatorId);
                existingMembers.add(member);
            } else {
                lookingForList.add(member.getInstrument());
            }
        }
        newGroup.setCreator(creatorId);
        newGroup.setLookingFor(lookingForList);
        newGroup.setExistingMembers(existingMembers);
        mDataManager.createGroupApiCall(newGroup, new DataManager.CreateGroupCallback() {
                    @Override
                    public void onCreateGroupSuccess(String groupId) {
                        // Add newly created group to user's groups
                        addGroupToUser(groupId);
                    }

                    @Override
                    public void onCreateGroupFailure(String error) {
                        mLookingForView.showMessage(error);
                    }
                }
        );
    }

    // Adds group to user groups
    private void addGroupToUser(String groupId) {
        mDataManager.addGroupToUserApiCall(mDataManager.getCurrentUserId(), groupId, new DataManager.AddGroupToUserCallback() {
            @Override
            public void onAddGroupSuccess(User user) {
                mLookingForView.showCreateGroupSuccess(newGroup.getLookingFor(), newGroup.getGroupName());
            }

            @Override
            public void onAddGroupFailure(String error) {
                mLookingForView.showMessage(error);
            }
        });
    }

    @Override
    public boolean validateData(List<ExistingMember> members) {
        if (members.get(0).getInstrument() == "Choose instrument") {
            mLookingForView.showWarningDialog("Please assign an instrument to yourself before continuing.");
            return false;
        } else if (members.size() < 2) {
            mLookingForView.showWarningDialog("Please insert at least one position your are looking for.");
            return false;
        } else {
            return true;
        }
    }


    private void fetchUserInstruments() {
        List<String> userInstrumentsList = mDataManager.getCurrentUserInstruments();
        userInstruments = userInstrumentsList.toArray(new String[userInstrumentsList.size()]);
    }
}
