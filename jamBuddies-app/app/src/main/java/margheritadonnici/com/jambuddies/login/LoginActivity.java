package margheritadonnici.com.jambuddies.login;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import margheritadonnici.com.jambuddies.R;
import margheritadonnici.com.jambuddies.data.AppDataManager;
import margheritadonnici.com.jambuddies.utils.ActivityUtils;

public class LoginActivity extends AppCompatActivity {

    // https://sourcey.com/beautiful-android-login-and-signup-screens-with-material-design/

    private static final String TAG = "LoginActivity";
    public static final int REQUEST_SIGNUP = 1;
    private LoginPresenter mLoginPresenter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);

        //Set up the toolbar
        getSupportActionBar().hide();

        // Load LoginFragment
        LoginFragment loginFragment = (LoginFragment) getSupportFragmentManager().findFragmentById(R.id.login_frame_layout);
        if (loginFragment == null) {
            // Create Fragment
            loginFragment = LoginFragment.newInstance();
            ActivityUtils.addFragmentToActivity(getSupportFragmentManager(), loginFragment, R.id.login_frame_layout);
        }

        // Create the presenter
        mLoginPresenter = new LoginPresenter(loginFragment, AppDataManager.getInstance(getApplicationContext()));

    }

    @Override
    public void onBackPressed() {
        // Disable going back to the MainActivity
        moveTaskToBack(true);
    }


}
