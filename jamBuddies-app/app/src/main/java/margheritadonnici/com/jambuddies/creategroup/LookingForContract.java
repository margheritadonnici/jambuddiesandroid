package margheritadonnici.com.jambuddies.creategroup;

import java.util.List;

import margheritadonnici.com.jambuddies.BasePresenter;
import margheritadonnici.com.jambuddies.BaseView;
import margheritadonnici.com.jambuddies.data.model.ExistingMember;

/**
 * Created by Margherita Donnici on 11/23/17.
 */

public interface LookingForContract {

    interface View extends BaseView<Presenter> {

        void showLoadingDialog(String msg);

        void hideLoadingDialog();

        void showMessage(String msg);

        void showWarningDialog(String msg);

        void showCreateGroupSuccess(List<String> lookingFor, String groupName);
    }

    interface Presenter extends BasePresenter {

        void submitData(List<ExistingMember> membersList);

        boolean validateData(List<ExistingMember> members);

        String[] getUserInstruments();

    }
}
