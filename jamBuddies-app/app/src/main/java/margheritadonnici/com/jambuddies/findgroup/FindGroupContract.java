package margheritadonnici.com.jambuddies.findgroup;

import android.content.Intent;

import com.google.android.gms.maps.model.LatLng;

import java.util.List;

import margheritadonnici.com.jambuddies.BasePresenter;
import margheritadonnici.com.jambuddies.BaseView;
import margheritadonnici.com.jambuddies.data.model.Group;
import margheritadonnici.com.jambuddies.data.model.GroupWithMembers;
import margheritadonnici.com.jambuddies.data.model.JoinRequest;

/**
 * Created by Margherita Donnici on 12/3/17.
 */

public interface FindGroupContract {

    interface View extends BaseView<Presenter> {
        void showLoadingDialog(String msg);

        void hideLoadingDialog();

        void showMessage(String message);

        void displayData(List<GroupWithMembers> groupList);
    }

    interface Presenter extends BasePresenter {
        LatLng getCurrentUserLocation();

        String getCurrentUserId();

        List<String> getCurrentUserInstruments();

        void searchForGroups(SearchFilter filter);

        void createJoinRequest(JoinRequest req);
    }
}
