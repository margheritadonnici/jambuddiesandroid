package margheritadonnici.com.jambuddies.data.remote;

import android.util.Base64;

import java.io.UnsupportedEncodingException;

/**
 * Created by Margherita Donnici on 11/15/17.
 */

public class ApiUtils {

    private ApiUtils() {
    }

    public static final String BASE_URL = "http://b0544b5e.ngrok.io";

    public static ApiService getAPIService(String token) {

        return RetrofitClient.getClient(BASE_URL, token).create(ApiService.class);
    }

    // https://mobikul.com/basic-authentication-retrofit-android/
    public static String getAuthToken(String email, String password) {
        byte[] data = new byte[0];
        try {
            data = (email + ":" + password).getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return "Basic " + Base64.encodeToString(data, Base64.NO_WRAP);
    }
}
