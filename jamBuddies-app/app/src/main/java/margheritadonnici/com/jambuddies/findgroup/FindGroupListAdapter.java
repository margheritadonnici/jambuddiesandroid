package margheritadonnici.com.jambuddies.findgroup;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.location.Location;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import margheritadonnici.com.jambuddies.R;
import margheritadonnici.com.jambuddies.data.model.GroupWithMembers;
import margheritadonnici.com.jambuddies.data.model.JoinRequest;
import margheritadonnici.com.jambuddies.utils.MemberListAdapter;

/**
 * Created by Margherita Donnici on 1/8/18.
 */

public class FindGroupListAdapter extends RecyclerView.Adapter<FindGroupListAdapter.ViewHolder> {

    private static final String TAG = "FindGroupListAdapter";
    private List<GroupWithMembers> mGroups;
    private Context mContext;
    private FindGroupFragment mFragment;
    private LatLng mCurrentUserLocation;
    private String mChosenInstrument;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public CardView card;
        public TextView firstLine;
        public TextView secondLine;
        public TextView thirdLine;
        public TextView fourthLine;
        public TextView fifthLine;
        public TextView sixthLine;
        public Button viewMembersButton;
        public Button joinButton;
        ;
        public View layout;

        public ViewHolder(View itemView) {
            super(itemView);
            layout = itemView;
            card = (CardView) itemView.findViewById(R.id.group_list_card_view);
            firstLine = (TextView) itemView.findViewById(R.id.firstLine);
            secondLine = (TextView) itemView.findViewById(R.id.secondLine);
            thirdLine = (TextView) itemView.findViewById(R.id.thirdLine);
            fourthLine = (TextView) itemView.findViewById(R.id.fourthLine);
            fifthLine = (TextView) itemView.findViewById(R.id.fifthLine);
            sixthLine = (TextView) itemView.findViewById(R.id.sixthLine);
            viewMembersButton = (Button) itemView.findViewById(R.id.looking_button);
            joinButton = (Button) itemView.findViewById(R.id.join_button);
        }
    }

    // Pass in the group array into the constructor
    public FindGroupListAdapter(Context context, FindGroupFragment myFragment, LatLng currentUserLocation, List<GroupWithMembers> groupList) {
        mGroups = groupList;
        mContext = context;
        mCurrentUserLocation = currentUserLocation;
        mFragment = myFragment;
    }

    // Easy access to the context object in the recyclerview
    private Context getContext() {
        return mContext;
    }

    public void add(GroupWithMembers item) {
        mGroups.add(item);
        int position = mGroups.size() - 1;
        if (position < 0) {
            notifyItemInserted(0);
        } else {
            notifyItemInserted(mGroups.size() - 1);
        }
        mFragment.getEmptyListView().setVisibility(View.GONE);
    }

    public void updateGroupList(List<GroupWithMembers> newlist) {
        mGroups.clear();
        mGroups.addAll(newlist);
        this.notifyDataSetChanged();

    }

    public void remove(int position) {
        mGroups.remove(position);
        notifyItemRemoved(position);
        if (mGroups.isEmpty()) {
            mFragment.getEmptyListView().setVisibility(View.VISIBLE);
        }
    }

    // Create new views (invoked by the layout manager)
    @Override
    public FindGroupListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View groupView;
        groupView = inflater.inflate(R.layout.find_group_list_item, parent, false);
        // set the view's size, margins, paddings and layout parameters
        FindGroupListAdapter.ViewHolder viewHolder = new FindGroupListAdapter.ViewHolder(groupView);
        return viewHolder;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final FindGroupListAdapter.ViewHolder holder, final int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        final GroupWithMembers group = mGroups.get(position);
        holder.firstLine.setText(group.getGroupName());
        // Distance
        holder.secondLine.setText(" " + distanceAsString(group.getLocation()) + " km");
        // Looking for
        String lookingForList = " ";
        for (String instrument : group.getLookingFor()) {
            lookingForList += instrument;
            if (group.getLookingFor().indexOf(instrument) != group.getLookingFor().size() - 1) {
                lookingForList += ", ";
            }
        }
        holder.thirdLine.setText(lookingForList);
        // Genres
        String genresList = " ";
        if (!group.getGenres().isEmpty()) {
            for (String genre : group.getGenres()) {
                genresList += genre;
                if (group.getGenres().indexOf(genre) != group.getGenres().size() - 1) {
                    genresList += ", ";
                }
            }
            holder.fourthLine.setText(genresList);
        } else {
            holder.fourthLine.setText(genresList += "not specified");
        }
        if (group.getCoverBand()) {
            String coverBand = "Cover Band";
            if (group.getCoverArtists() != null) {
                coverBand += " (" + group.getCoverArtists() + ")";
            }
            holder.fifthLine.setText(coverBand);
        } else {
            holder.fifthLine.setText("Original songs");
        }
        if (!group.getDescription().isEmpty()) {
            holder.sixthLine.setText(" " + group.getDescription());
        } else {
            holder.sixthLine.setText(" not specified");
        }

        holder.joinButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                List<String> possibleInstruments = new ArrayList<String>(group.getLookingFor());
                possibleInstruments.retainAll(mFragment.mPresenter.getCurrentUserInstruments());
                final String[] possibleInstrumentsArray = possibleInstruments.toArray(new String[possibleInstruments.size() - 1]);
                //Set to default instrument
                mChosenInstrument = possibleInstrumentsArray[0];
                // Dialog to choose instrument
                AlertDialog.Builder builder = new AlertDialog.Builder(mFragment.getActivity());
                builder.setTitle("Choose your instrument")
                        .setSingleChoiceItems(possibleInstrumentsArray, 0, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                mChosenInstrument = possibleInstrumentsArray[which];
                            }
                        })
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                // Create request
                                JoinRequest req = new JoinRequest();
                                req.setFrom(mFragment.mPresenter.getCurrentUserId());
                                Log.e(TAG, "creator: " + group.getCreator() + group.getGroupName());
                                req.setTo(group.getCreator());
                                req.setGroupId(group.getId());
                                req.setStatus("Pending");
                                req.setInstrument(mChosenInstrument);
                                mFragment.mPresenter.createJoinRequest(req);
                                dialogInterface.dismiss();
                            }
                        })
                        .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        })
                        .create()
                        .show();

            }
        });

        holder.viewMembersButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builderSingle = new AlertDialog.Builder(mContext);
                builderSingle.setTitle("Group Members");

                final MemberListAdapter memberListAdapter = new MemberListAdapter(mContext, group.getExistingMembers());
                builderSingle.setPositiveButton("CLOSE", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

                builderSingle.setAdapter(memberListAdapter, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        // do nothing
                    }
                });
                builderSingle.show();
            }
        });
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mGroups.size();
    }

    // Returns distance between current user location and given coordinates, in KM, as a string
    private String distanceAsString(List<Double> groupLocation) {
        float distance[] = new float[1];
        Location.distanceBetween(mCurrentUserLocation.latitude,
                mCurrentUserLocation.longitude,
                groupLocation.get(1), // latitude
                groupLocation.get(0), //longitude
                distance);
        float distanceKm = distance[0] / 1000;
        DecimalFormat df = new DecimalFormat("#.#");
        df.setRoundingMode(RoundingMode.CEILING);
        return df.format(distanceKm);
    }
}



