package margheritadonnici.com.jambuddies.createprofile;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

import java.io.File;
import java.io.IOException;

import margheritadonnici.com.jambuddies.R;
import margheritadonnici.com.jambuddies.data.model.User;
import margheritadonnici.com.jambuddies.utils.ImageUtils;
import margheritadonnici.com.jambuddies.utils.PlaceArrayAdapter;

import static android.app.Activity.RESULT_OK;
import static margheritadonnici.com.jambuddies.R.id.my_location_button;

public class CreateProfileDetailsFragment extends Fragment implements
        CreateProfileDetailsContract.View,
        MyDatePicker.DatePickerDialogCallbackContract {

    private static final String TAG = "CreateProfileView";
    private static final LatLngBounds BOUNDS = new LatLngBounds(new LatLng(37.398160, -122.180831),
            new LatLng(37.430610, -121.972090));

    private CreateProfileDetailsContract.Presenter mPresenter;
    private LatLng mLocation;
    private PlaceArrayAdapter mPlaceArrayAdapter;

    private FloatingActionButton photoFAB;
    private ImageView profilePicture;
    private AutoCompleteTextView locationAutoCompleteTextView;
    private ImageButton myLocationButton;
    private Button nextButton;
    private TextView dateOfBirth;
    private EditText bio;
    private EditText youtubeChannel;
    private EditText fbPage;
    private TextInputLayout locationTIL;

    private String currentPhotoPath;
    private Uri currentPhotoURI;


    public static CreateProfileDetailsFragment newInstance() {
        CreateProfileDetailsFragment fragment = new CreateProfileDetailsFragment();
        return fragment;
    }

    @Override
    public void setPresenter(CreateProfileDetailsContract.Presenter presenter) {
        this.mPresenter = presenter;
    }

    public PlaceArrayAdapter getPlaceArrayAdapter() {
        return mPlaceArrayAdapter;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPresenter.start();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_create_profile_details, container, false);

        //Bind views
        photoFAB = (FloatingActionButton) rootView.findViewById(R.id.select_picture_fab);
        profilePicture = (ImageView) rootView.findViewById(R.id.user_profile_photo);
        locationAutoCompleteTextView = (AutoCompleteTextView) rootView.findViewById(R.id.user_location);
        locationTIL = (TextInputLayout) rootView.findViewById(R.id.user_location_til);
        myLocationButton = (ImageButton) rootView.findViewById(R.id.my_location_button);
        nextButton = (Button) rootView.findViewById(R.id.next_button);
        dateOfBirth = (TextView) rootView.findViewById(R.id.dob);
        youtubeChannel = (EditText) rootView.findViewById(R.id.youtube_channel);
        fbPage = (EditText) rootView.findViewById(R.id.facebook_page);
        bio = (EditText) rootView.findViewById(R.id.user_bio);

        /***** DATE OF BIRTH *****/
        final CreateProfileDetailsFragment parentFragment = this;
        dateOfBirth.setClickable(true);
        dateOfBirth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogFragment newFragment = new MyDatePicker();
                newFragment.setTargetFragment(parentFragment, 0);
                newFragment.show(getFragmentManager(), "datePicker");
            }
        });

        /***** LOCATION AUTOCOMPLETE *****/

        locationAutoCompleteTextView.setThreshold(2);
        locationAutoCompleteTextView.setOnItemClickListener(
                new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                        final PlaceArrayAdapter.PlaceAutocomplete item = mPlaceArrayAdapter.getItem(position);
                        final String placeId = String.valueOf(item.placeId);
                        Log.i(TAG, "Selected: " + item.description);
                        PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi
                                .getPlaceById(mPresenter.getGoogleApiClient(), placeId);
                        placeResult.setResultCallback(updatePlaceDetailsCallback);
                        Log.i(TAG, "Fetching details for ID: " + item.placeId);
                    }
                }
        );

        mPlaceArrayAdapter = new PlaceArrayAdapter(getContext(), android.R.layout.simple_list_item_1, BOUNDS, null);
        locationAutoCompleteTextView.setAdapter(mPlaceArrayAdapter);

        // My Location Button
        myLocationButton = (ImageButton) rootView.findViewById(my_location_button);
        myLocationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Get the user location
                mPresenter.checkUserPermissions();
                mLocation = mPresenter.getUserLocation();
                if (mLocation != null) {
                    mPresenter.getLocationAddressString(mLocation);
                    locationAutoCompleteTextView.setText(mPresenter.getLocationAddressString(mLocation));
                }
            }
        });


        // Buttons
        photoFAB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setItems(R.array.select_picture, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0: // From Gallery
                                // Trigger the intent for image gallery
                                Intent pickPictureIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                if (pickPictureIntent.resolveActivity(getContext().getPackageManager()) != null) { // check if there is an app which can handle the intent
                                    startActivityForResult(pickPictureIntent, CreateProfileDetailsPresenter.REQUEST_IMAGE_FROM_GALLERY);
                                }
                                break;
                            case 1: //From Camera
                                if (getContext().getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
                                    // https://developer.android.com/training/camera/photobasics.html#TaskCaptureIntent
                                    Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                    if (takePictureIntent.resolveActivity(getContext().getPackageManager()) != null) { // check if there is an app which can handle the intent
                                        // Create the File where the photo should go
                                        File photoFile = null;
                                        try {
                                            photoFile = ImageUtils.createImageFile(getContext());
                                            currentPhotoPath = photoFile.getAbsolutePath();
                                        } catch (IOException ex) {
                                            // Error occurred while creating the File
                                        }
                                        // Continue only if the File was successfully created
                                        if (photoFile != null) {
                                            currentPhotoURI = FileProvider.getUriForFile(getContext(),
                                                    "margheritadonnici.com.jambuddies.fileprovider",
                                                    photoFile);
                                            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, currentPhotoURI);
                                            startActivityForResult(takePictureIntent, CreateProfileDetailsPresenter.REQUEST_IMAGE_CAPTURE);
                                        }
                                    } else { // user's device doesn't have a camera
                                        Toast.makeText(getContext(), "No camera detected on device", Toast.LENGTH_LONG).show();
                                    }
                                }
                                break;
                            case 2: // Delete photo
                                profilePicture.setImageResource(R.drawable.default_profile);
                                break;
                            default:
                                Log.e(TAG, "Invalid option");
                                break;
                        }
                    }
                });
                AlertDialog choosePictureDialog = builder.create();
                choosePictureDialog.show();
            }
        });

        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Save form data
                mPresenter.saveData(profilePicture,
                        bio.getText().toString(),
                        dateOfBirth.getText().toString(),
                        mLocation,
                        youtubeChannel.getText().toString(),
                        fbPage.getText().toString());
                // Go to next fragment
                mPresenter.getGoogleApiClient().disconnect();
                ((CreateProfileActivity) getActivity()).goToPickInstrumentsFragment();
            }
        });

        return rootView;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Bitmap image = mPresenter.result(requestCode, resultCode, data, currentPhotoPath, profilePicture.getWidth(), profilePicture.getHeight());
        if (image != null) {
            profilePicture.setImageBitmap(image);
        }
    }

    private ResultCallback<PlaceBuffer> updatePlaceDetailsCallback
            = new ResultCallback<PlaceBuffer>() {
        @Override
        public void onResult(PlaceBuffer places) {
            if (!places.getStatus().isSuccess()) {
                Log.e(TAG, "Place query did not complete. Error: " +
                        places.getStatus().toString());
                return;
            }
            // Selecting the first object buffer.
            final Place place = places.get(0);
            // Save selected location
            mLocation = place.getLatLng();
        }
    };

    @Override
    public void setChosenDate(int day, int month, int year) {
        String dateString = day + "-" + month + "-" + year;
        dateOfBirth.setText(dateString);
    }

    public void showMessage(String message) {
        Snackbar.make(getView(), message, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showEnableGPSDialog() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }


}