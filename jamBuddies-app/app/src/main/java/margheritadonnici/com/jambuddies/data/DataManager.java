package margheritadonnici.com.jambuddies.data;

import java.util.List;

import margheritadonnici.com.jambuddies.data.model.Comment;
import margheritadonnici.com.jambuddies.data.model.Group;
import margheritadonnici.com.jambuddies.data.model.GroupComments;
import margheritadonnici.com.jambuddies.data.model.GroupWithMembers;
import margheritadonnici.com.jambuddies.data.model.JoinRequest;
import margheritadonnici.com.jambuddies.data.model.User;
import margheritadonnici.com.jambuddies.data.model.UserRequests;
import margheritadonnici.com.jambuddies.data.model.UserWithGroups;
import margheritadonnici.com.jambuddies.data.prefs.PreferencesHelper;

/**
 * Created by Margherita Donnici on 11/15/17.
 */

public interface DataManager extends PreferencesHelper {

    interface SignUpCallback {

        void onSignUpSuccess(String userId, String token, List<Double> location, List<String> instruments);
        void onSignUpFailure(String error);
    }

    interface LoginCallback {

        void onLoginSuccess(String firstName, String lastName, String id, String token, List<Double> location, List<String> instruments);
        void onLoginFailure(String error);
    }

    interface UserUpdateCallback {
        void onUserUpdateSuccess(List<String> instrumentsList, List<Double> location);
        void onUserUpdateFailure(String error);
    }

    interface AddGroupToUserCallback {
        void onAddGroupSuccess(User user);

        void onAddGroupFailure(String error);
    }

    interface CreateGroupCallback {
        void onCreateGroupSuccess(String groupId);

        void onCreateGroupFailure(String error);
    }

    interface GetUserByIdCallback {
        void onGetUserSuccess(UserWithGroups user);

        void onGetUserFailure(String error);
    }

    interface GetGroupsCallback {
        void onGetGroupsSuccess(List<GroupWithMembers> groupList);

        void onGetGroupsFailure(String error);
    }

    interface GetGroupCommentsCallback {
        void onGetGroupCommentsSuccess(GroupComments groupComments);

        void onGetGroupCommentsFailure(String error);
    }

    interface AddCommentCallback {
        void onAddCommentSuccess(List<Comment> commentList);

        void onAddCommentFailure(String error);
    }

    interface DeleteGroupCallback {
        void onDeleteGroupSuccess(String msg);

        void onDeleteGroupFailure(String error);
    }

    interface CreateJoinRequestCallback {
        void onCreateJoinRequestSuccess(JoinRequest req);

        void onCreateJoinRequestFailure(String error);
    }

    interface GetUserRequestsCallback {
        void onGetUserRequestsSuccess(UserRequests reqList);

        void onGetUserRequestsFailure(String error);
    }

    interface UpdateRequestStatusCallback {
        void onUpdateRequestStatusSuccess(JoinRequest request);

        void onUpdateRequestStatusFailure(String error);
    }

    interface UpdateGroupMembersCallback {
        void onUpdateGroupMembersSuccess(Group group);

        void onUpdateGroupMembersFailure(String error);
    }
}
