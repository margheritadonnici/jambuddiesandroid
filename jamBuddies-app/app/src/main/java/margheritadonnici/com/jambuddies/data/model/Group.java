package margheritadonnici.com.jambuddies.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Margherita Donnici on 11/23/17.
 */

public class Group {

    @SerializedName("groupName")
    @Expose
    private String groupName;
    @SerializedName("creator")
    @Expose
    private String creator;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("coverBand")
    @Expose
    private Boolean coverBand;
    @SerializedName("coverArtists")
    @Expose
    private String coverArtists;
    @SerializedName("location")
    @Expose
    private List<Double> location = null;
    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("lookingFor")
    @Expose
    private List<String> lookingFor = null;
    @SerializedName("existingMembers")
    @Expose
    private ArrayList<ExistingMember> existingMembers = null;
    @SerializedName("genres")
    @Expose
    private List<String> genres = null;

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getCoverBand() {
        return coverBand;
    }

    public void setCoverBand(Boolean coverBand) {
        this.coverBand = coverBand;
    }

    public String getCoverArtists() {
        return coverArtists;
    }

    public void setCoverArtists(String coverArtists) {
        this.coverArtists = coverArtists;
    }

    public List<Double> getLocation() {
        return location;
    }

    public void setLocation(List<Double> location) {
        this.location = location;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<String> getLookingFor() {
        return lookingFor;
    }

    public void setLookingFor(List<String> lookingFor) {
        this.lookingFor = lookingFor;
    }

    public ArrayList<ExistingMember> getExistingMembers() {
        return existingMembers;
    }

    public void setExistingMembers(ArrayList<ExistingMember> existingMembers) {
        this.existingMembers = existingMembers;
    }

    public List<String> getGenres() {
        return genres;
    }

    public void setGenres(List<String> genres) {
        this.genres = genres;
    }


    @Override
    public String toString() {
        return "Group{" +
                "groupName='" + groupName + '\'' +
                ", creator='" + creator + '\'' +
                ", description='" + description + '\'' +
                ", coverBand=" + coverBand +
                ", coverArtists='" + coverArtists + '\'' +
                ", location=" + location +
                ", id='" + id + '\'' +
                ", lookingFor=" + lookingFor +
                ", existingMembers=" + existingMembers.toString() +
                ", genres=" + genres +
                '}';
    }
}
