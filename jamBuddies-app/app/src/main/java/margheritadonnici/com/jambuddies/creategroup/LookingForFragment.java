package margheritadonnici.com.jambuddies.creategroup;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import margheritadonnici.com.jambuddies.R;
import margheritadonnici.com.jambuddies.data.model.ExistingMember;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link LookingForFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class LookingForFragment extends Fragment implements LookingForContract.View {

    private static final String TAG = "LookingForFragment";
    private LookingForContract.Presenter mPresenter;

    private RecyclerView rvNewMembers;
    private TextView emptyListView;

    private ArrayList<ExistingMember> members = new ArrayList<ExistingMember>();
    private static NewMembersListAdapter adapter;
    private ProgressDialog mProgressDialog;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment LookingForFragment.
     */

    public static LookingForFragment newInstance() {
        LookingForFragment fragment = new LookingForFragment();
        return fragment;
    }

    @Override
    public void setPresenter(LookingForContract.Presenter presenter) {
        this.mPresenter = presenter;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPresenter.start();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View rootView = inflater.inflate(R.layout.fragment_looking_for, container, false);

        // Enable action bar help button
        // https://www.grokkingandroid.com/adding-action-items-from-within-fragments/
        setHasOptionsMenu(true);

        /********** RECYCLER VIEW **********/
        // Lookup the recyclerview in fragment layout
        rvNewMembers = (RecyclerView) rootView.findViewById(R.id.looking_for_list);
        // Get the TextView to display if our dataset is empty (Should never happen!)
        emptyListView = (TextView) rootView.findViewById(R.id.empty_view);
        if (members.isEmpty()) {
            emptyListView.setVisibility(View.VISIBLE);
        }
        // Create adapter passing in the members list
        adapter = new NewMembersListAdapter(getActivity(), this, members);
        // Set layout manager to position the items
        rvNewMembers.setLayoutManager(new LinearLayoutManager(getActivity()));
        // Add first element (user)
        ExistingMember userMember = new ExistingMember("Choose instrument", true);
        adapter.add(userMember);
        // Attach the adapter to the recyclerview to populate items
        rvNewMembers.setAdapter(adapter);

        /********** BUTTONS **********/
        // Add members button
        final Button addMember = (Button) rootView.findViewById(R.id.add_new_member);
        addMember.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                showNewMembersDialog(-1); // -1 because we are adding a member
            }
        });

        // SUBMIT BUTTON
        final Button submitButton = (Button) rootView.findViewById(R.id.submit_button);
        submitButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (mPresenter.validateData(members)) {
                    mPresenter.submitData(members);
                }
            }
        });
        return rootView;
    }

    /********** ACTION BAR HELP BUTTON **********/
    // Create the action bar help button
    @Override
    public void onCreateOptionsMenu(
            Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.action_bar_button, menu);
    }

    // Handle action bar help button activities
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.help_button) {
            // HELP DIALOG
            AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
            alertDialog.setTitle("Help");
            alertDialog.setMessage("Choose new members");
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            alertDialog.show();
        }
        return super.onOptionsItemSelected(item);
    }

    // Getter used by Adapter to display/not display empty list message on dataset update
    public TextView getEmptyListView() {
        return emptyListView;
    }

    static void addNewMember(String instrument) {
        ExistingMember newMember = new ExistingMember(instrument, false);
        adapter.add(newMember);
    }

    static void editNewMember(String instrument, int position) {
        ExistingMember newMember;
        if (position == 0) {
            // We are editing the header
            newMember = new ExistingMember(instrument, true);
        } else {
            newMember = new ExistingMember(instrument, false);
        }
        adapter.edit(newMember, position);
    }

    // Dialog with mini-form to create new members. If position is set to -1, we are adding a new member; otherwise we are editing the member in that position
    public void showNewMembersDialog(final int position) {

        final AlertDialog createMemberDialog;
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View dialogView = inflater.inflate(R.layout.add_new_member_dialog, null);

        // Instruments spinner
        final Spinner spinner = (Spinner) dialogView.findViewById(R.id.new_members_instruments_spinner);
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<String> adapter = null;
        if (position == 0) {
            // The user can only choose from the instruments in his profile
            String[] instrumentsArray = mPresenter.getUserInstruments();
            if (instrumentsArray != null) {
                adapter = new ArrayAdapter<String>(getContext(),
                        android.R.layout.simple_spinner_item,
                        instrumentsArray);
            }
        } else {
            adapter = new ArrayAdapter<String>(getContext(),
                    android.R.layout.simple_spinner_item,
                    getResources().getStringArray(R.array.music_instruments));
        }
        if (adapter != null) {
            // Specify the layout to use when the list of choices appears
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            // Apply the adapter to the spinner
            spinner.setAdapter(adapter);
        }

        alertDialogBuilder.setView(dialogView);
        alertDialogBuilder.setPositiveButton("OK", null); // OnClickHandler is null because we will override in onShowListener
        alertDialogBuilder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        alertDialogBuilder.setTitle("Looking For");
        createMemberDialog = alertDialogBuilder.create();

        // OnClickListener for OK button, have to implement it by adding an onShowListener and overriding the onClickListener of the button to prevent the dialog from closing even if there are errors
        // https://stackoverflow.com/questions/2620444/how-to-prevent-a-dialog-from-closing-when-a-button-is-clicked/7636468#7636468
        createMemberDialog.setOnShowListener(new DialogInterface.OnShowListener() {

            @Override
            public void onShow(DialogInterface dialog) {

                Button button = ((AlertDialog) dialog).getButton(AlertDialog.BUTTON_POSITIVE);
                button.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {
                        // Get user input data
                        String instrument = spinner.getSelectedItem().toString();
                        // Add member to list
                        if (position < 0) {
                            addNewMember(instrument);
                            // Close keyboard
                            InputMethodManager in = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                            in.hideSoftInputFromWindow(view.getApplicationWindowToken(), 0);
                            // Scroll to bottom
                            rvNewMembers.scrollToPosition(members.size() - 1);
                        } else {
                            editNewMember(instrument, position);
                        }
                        createMemberDialog.dismiss();
                    }
                });
            }
        });
        createMemberDialog.show();
    }


    @Override
    public void showCreateGroupSuccess(final List<String> lookingFor, final String groupName) {
        if (mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
        AlertDialog.Builder shareDialogBuilder = new AlertDialog.Builder(getContext());
        shareDialogBuilder.setTitle("Group created!");
        shareDialogBuilder.setMessage("Your new group has been created. Share it to increase your chances of finding new members!");
        shareDialogBuilder.setIcon(R.drawable.share_blue);
        shareDialogBuilder.setPositiveButton("SHARE", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/html");
                String shareBody = "My group " + groupName + " is looking for ";
                for (String instrument : lookingFor) {
                    shareBody += instrument;
                    if (lookingFor.indexOf(instrument) != lookingFor.size() - 1) {
                        shareBody += ", ";
                    }
                }
                shareBody += ". Check it out on the JamBuddies app! Download on Google Play Store: https://play.google.com/store";
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Join my new music group!");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, Html.fromHtml(shareBody));
                startActivity(Intent.createChooser(sharingIntent, "Share via"));
                dialogInterface.dismiss();
                getActivity().setResult(Activity.RESULT_OK);
                getActivity().finish();
            }
        });
        shareDialogBuilder.setNegativeButton("MAYBE LATER", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                getActivity().setResult(Activity.RESULT_OK);
                getActivity().finish();
            }
        });
        AlertDialog shareDialog = shareDialogBuilder.create();
        shareDialog.show();
    }

    @Override
    public void showMessage(String msg) {
        Snackbar.make(getActivity().findViewById(R.id.create_group_frame_layout), msg, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showWarningDialog(String msg) {
        AlertDialog alertDialog = new AlertDialog.Builder(getContext()).create();
        alertDialog.setTitle("Warning");
        alertDialog.setMessage(msg);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

    @Override
    public void showLoadingDialog(String msg) {
        mProgressDialog = new ProgressDialog(getContext(), R.style.Dark_Dialog);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage(msg);
        mProgressDialog.show();
    }

    @Override
    public void hideLoadingDialog() {
        if (mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }
}
