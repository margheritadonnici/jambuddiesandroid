package margheritadonnici.com.jambuddies.createprofile;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.location.Address;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.widget.ImageView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import margheritadonnici.com.jambuddies.data.model.User;
import margheritadonnici.com.jambuddies.utils.ImageUtils;
import margheritadonnici.com.jambuddies.utils.LocationServicesHelper;

import static android.app.Activity.RESULT_OK;

/**
 * Created by Margherita Donnici on 11/19/17.
 */

public class CreateProfileDetailsPresenter implements CreateProfileDetailsContract.Presenter,
        ActivityCompat.OnRequestPermissionsResultCallback,
        GoogleApiClient.OnConnectionFailedListener,
        GoogleApiClient.ConnectionCallbacks {

    private static final String TAG = "ProfileDetailsPresenter";
    public static final int REQUEST_IMAGE_FROM_GALLERY = 1;
    public static final int REQUEST_IMAGE_CAPTURE = 2;

    private final CreateProfileDetailsContract.View mProfileDetailsView;

    private LocationServicesHelper mLocationHelper;

    private Context mCallerContext;

    private User mUserData;

    public CreateProfileDetailsPresenter(CreateProfileDetailsContract.View profileDetailsView, Context callerContext) {
        this.mProfileDetailsView = profileDetailsView;
        profileDetailsView.setPresenter(this);
        this.mCallerContext = callerContext;
        mLocationHelper = new LocationServicesHelper(callerContext, this);
    }

    @Override
    public void start() {
        // Check availability of play services
        if (mLocationHelper.checkPlayServices()) {
            // Building the GoogleApi client
            mLocationHelper.buildGoogleApiClient();
        }
        mUserData = ((CreateProfileActivity) mCallerContext).getUserData();
    }

    @Override
    public void saveData(ImageView profilePic, String bio, String dateOfBirth, LatLng location, String youtubeChannel, String fbPage) {
        // Convert profile picture to base64
        Bitmap profilePicBitmap = ImageUtils.getBitmapFromImageView(profilePic);
        String profilePicBase64 = ImageUtils.getBase64FromBitmap(profilePicBitmap);
        mUserData.setProfilePicture(profilePicBase64);
        mUserData.setBio(bio);
        mUserData.setDateOfBirth(dateOfBirth);
        //Convert location to a list of doubles [longitude, latitude]
        List<Double> locList = new ArrayList<Double>();
        if (location != null) {
            locList.add(location.longitude);
            locList.add(location.latitude);
            mUserData.setLocation(locList);
        }
        mUserData.setYoutubeChannel(youtubeChannel);
        mUserData.setFbPage(fbPage);
    }

    @Override
    public LatLng getUserLocation() {
        checkUserPermissions();
        if (!mLocationHelper.isGPSEnabled()) {
            mProfileDetailsView.showEnableGPSDialog();
        }
        Location userLocation = mLocationHelper.getLocation();
        LatLng returnLocation = null;

        if (userLocation != null) {
            returnLocation = new LatLng(userLocation.getLatitude(), userLocation.getLongitude());
        }
        return returnLocation;
    }

    @Override
    public String getLocationAddressString(LatLng location) {
        Address locationAddress = mLocationHelper.getAddress(location.latitude, location.longitude);
        return mLocationHelper.getAddressString(locationAddress);
    }

    @Override
    public GoogleApiClient getGoogleApiClient() {
        return mLocationHelper.getGoogleApiClient();
    }

    @Override
    public void checkUserPermissions() {
        mLocationHelper.checkpermission();
    }

    @Override
    public Bitmap result(int requestCode, int resultCode, Intent data, String currentPhotoPath, int width, int height) {
        mLocationHelper.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_IMAGE_FROM_GALLERY && resultCode == RESULT_OK && null != data) {
            // http://viralpatel.net/blogs/pick-image-from-galary-android-app/
            Uri selectedImage = data.getData();
            try {
                Bitmap rotatedBitmap = ImageUtils.getCorrectlyOrientedImage(mCallerContext, selectedImage);
                return rotatedBitmap;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bitmap rotatedBitmap = null;
            try {
                rotatedBitmap = ImageUtils.getRotatedBitmap(currentPhotoPath, ImageUtils.getBitmapFromPath(width, height, currentPhotoPath));
            } catch (IOException e) {
                e.printStackTrace();
            }
            return rotatedBitmap;
        }

        return null;
    }

    // This method is called when the user responds to the permission request
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        mLocationHelper.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    /**
     * Google connection callback methods
     */
    @Override
    public void onConnectionFailed(ConnectionResult result) {
        Log.e(TAG, "Google Places API connection failed with error code" + result.getErrorCode());
        mProfileDetailsView.showMessage("Google Places API connection failed");

    }

    @Override
    public void onConnected(Bundle arg0) {
        Log.i(TAG, "Google Places API connected.");
        // Once connected with google api, set the location AutoComplete adapter
        mProfileDetailsView.getPlaceArrayAdapter().setGoogleApiClient(mLocationHelper.getGoogleApiClient());

    }

    @Override
    public void onConnectionSuspended(int arg0) {
        Log.e(TAG, "Google Places API connection suspended.");
        mProfileDetailsView.getPlaceArrayAdapter().setGoogleApiClient(null);
        // Reconnect
        mLocationHelper.connectApiClient();
    }
}
