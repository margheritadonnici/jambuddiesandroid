package margheritadonnici.com.jambuddies.home;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import margheritadonnici.com.jambuddies.R;
import margheritadonnici.com.jambuddies.data.model.JoinRequestWithDetails;
import margheritadonnici.com.jambuddies.utils.ImageUtils;

/**
 * Created by Margherita Donnici on 1/11/18.
 */

public class ReceivedRequestListAdapter extends RecyclerView.Adapter<ReceivedRequestListAdapter.ViewHolder> {

    private static final String TAG = "ReceivedReqListAdapter";
    private List<JoinRequestWithDetails> mRequests;
    private Context mContext;
    private HomeFragment mFragment;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView groupName;
        public TextView fromUser;
        public TextView instrument;
        public Button statusButton;
        public ImageButton acceptButton;
        public ImageButton rejectButton;
        public ImageView icon;
        public View layout;

        public ViewHolder(View itemView) {
            super(itemView);
            layout = itemView;
            groupName = (TextView) itemView.findViewById(R.id.group_name);
            fromUser = (TextView) itemView.findViewById(R.id.from_user);
            instrument = (TextView) itemView.findViewById(R.id.instrument);
            statusButton = (Button) itemView.findViewById(R.id.status_badge);
            acceptButton = (ImageButton) itemView.findViewById(R.id.accept_btn);
            rejectButton = (ImageButton) itemView.findViewById(R.id.reject_btn);
            icon = (ImageView) itemView.findViewById(R.id.icon);
        }
    }

    // Pass in the group array into the constructor
    public ReceivedRequestListAdapter(Context context, HomeFragment myFragment, List<JoinRequestWithDetails> reqList) {
        mRequests = reqList;
        mContext = context;
        mFragment = myFragment;
    }

    // Easy access to the context object in the recyclerview
    private Context getContext() {
        return mContext;
    }

    public void add(JoinRequestWithDetails item) {
        mRequests.add(item);
        int position = mRequests.size() - 1;
        if (position < 0) {
            notifyItemInserted(0);
        } else {
            notifyItemInserted(mRequests.size() - 1);
        }
    }

    public void updateGroupList(List<JoinRequestWithDetails> newlist) {
        mRequests.clear();
        mRequests.addAll(newlist);
        this.notifyDataSetChanged();
    }

    public void remove(int position) {
        mRequests.remove(position);
        notifyItemRemoved(position);
        if (mRequests.isEmpty()) {
            //  mFragment.getEmptyListView().setVisibility(View.VISIBLE);
        }
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ReceivedRequestListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View groupView;
        groupView = inflater.inflate(R.layout.received_request_layout, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ReceivedRequestListAdapter.ViewHolder viewHolder = new ReceivedRequestListAdapter.ViewHolder(groupView);
        return viewHolder;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ReceivedRequestListAdapter.ViewHolder holder, final int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        final JoinRequestWithDetails req = mRequests.get(position);
        holder.groupName.setText(req.getGroupId().getGroupName());
        holder.fromUser.setText(req.getFrom().getFirstName() + " " + req.getFrom().getLastName());
        holder.instrument.setText(req.getInstrument());
        holder.icon.setImageResource(ImageUtils.getInstrumentImage(req.getInstrument()));
        holder.statusButton.setText(req.getStatus());
        switch (req.getStatus()) {
            case "Rejected":
                holder.statusButton.setBackgroundColor(Color.RED);
                holder.rejectButton.setVisibility(View.GONE);
                holder.acceptButton.setVisibility(View.GONE);
                break;
            case "Accepted":
                holder.statusButton.setBackgroundColor(Color.rgb(0, 128, 0)); //dark green
                holder.rejectButton.setVisibility(View.GONE);
                holder.acceptButton.setVisibility(View.GONE);
                break;
            default:
                break; //remain blue
        }

        holder.acceptButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mFragment.mPresenter.acceptRequest(mRequests.get(position));
            }
        });

        holder.rejectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mFragment.mPresenter.rejectRequest(mRequests.get(position));
            }
        });
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mRequests.size();
    }
}