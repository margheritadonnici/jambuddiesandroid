package margheritadonnici.com.jambuddies.creategroup;


import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;

import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.plumillonforge.android.chipview.Chip;
import com.plumillonforge.android.chipview.ChipView;
import com.plumillonforge.android.chipview.ChipViewAdapter;
import com.plumillonforge.android.chipview.OnChipClickListener;

import java.util.ArrayList;
import java.util.List;

import margheritadonnici.com.jambuddies.R;
import margheritadonnici.com.jambuddies.utils.MainChipViewAdapter;
import margheritadonnici.com.jambuddies.utils.PlaceArrayAdapter;
import margheritadonnici.com.jambuddies.utils.Tag;

import static margheritadonnici.com.jambuddies.R.id.group_description;
import static margheritadonnici.com.jambuddies.R.id.group_location;
import static margheritadonnici.com.jambuddies.R.id.group_name;
import static margheritadonnici.com.jambuddies.R.id.my_location_button;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link CreateGroupDetailsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */

public class CreateGroupDetailsFragment extends Fragment implements
        CreateGroupDetailsContract.View {

    private static final String TAG = "CreateGroupFragment";
    private static final LatLngBounds BOUNDS = new LatLngBounds(new LatLng(37.398160, -122.180831),
            new LatLng(37.430610, -121.972090));

    private CreateGroupDetailsContract.Presenter mPresenter;
    private PlaceArrayAdapter mPlaceArrayAdapter;
    private LatLng mLocation;

    private TextInputLayout nameTIL;
    private EditText nameEditText;
    private TextInputLayout locationTIL;
    private AutoCompleteTextView locationAutoCompleteTextView;
    private EditText descriptionEditText;
    private TextInputLayout coverArtist;
    private EditText coverArtistEditText;
    private CheckBox coverArtistCheckBox;
    private ImageButton myLocationButton;

    @Override
    public void setPresenter(@NonNull CreateGroupDetailsContract.Presenter presenter) {
        mPresenter = presenter;
    }

    public PlaceArrayAdapter getPlaceArrayAdapter() {
        return mPlaceArrayAdapter;
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment CreateGroupDetailsFragment.
     */
    public static CreateGroupDetailsFragment newInstance() {
        CreateGroupDetailsFragment fragment = new CreateGroupDetailsFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPresenter.start();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View rootView = inflater.inflate(R.layout.fragment_create_group_details, container, false);

        // Bind Views
        nameTIL = (TextInputLayout) rootView.findViewById(R.id.group_name_til);
        nameEditText = (EditText) rootView.findViewById(group_name);
        locationTIL = (TextInputLayout) rootView.findViewById(R.id.group_location_til);
        descriptionEditText = (EditText) rootView.findViewById(group_description);
        coverArtist = (TextInputLayout) rootView.findViewById(R.id.cover_artist_til);
        coverArtistEditText = (EditText) rootView.findViewById(R.id.cover_artist);
        coverArtistCheckBox = (CheckBox) rootView.findViewById(R.id.cover_band_checkbox);
        locationAutoCompleteTextView = (AutoCompleteTextView) rootView.findViewById(group_location);

        // Location AutoCompleteTextView
        mPlaceArrayAdapter = new PlaceArrayAdapter(getContext(), android.R.layout.simple_list_item_1, BOUNDS, null);
        locationAutoCompleteTextView.setAdapter(mPlaceArrayAdapter);
        locationAutoCompleteTextView.setThreshold(2);
        locationAutoCompleteTextView.setOnItemClickListener(
                new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                        Log.e(TAG, "onclick");
                        final PlaceArrayAdapter.PlaceAutocomplete item = mPlaceArrayAdapter.getItem(position);
                        final String placeId = String.valueOf(item.placeId);
                        PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi.getPlaceById(mPresenter.getGoogleApiClient(), placeId);
                        placeResult.setResultCallback(updatePlaceDetailsCallback);
                    }
                }
        );

        // My Location Button
        myLocationButton = (ImageButton) rootView.findViewById(my_location_button);
        myLocationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Get the user location
                mPresenter.checkUserPermissions();
                mLocation = mPresenter.getUserLocation();
                if (mLocation != null) {
                    mPresenter.getLocationAddressString(mLocation);
                    locationAutoCompleteTextView.setText(mPresenter.getLocationAddressString(mLocation));
                }

            }
        });

        // Cover Band checkbox
        coverArtistCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    coverArtist.setVisibility(View.VISIBLE);
                } else {
                    coverArtist.setVisibility(View.INVISIBLE);
                }
            }
        });

        // Name EditText listener for removing error when user types something
        nameEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                nameTIL.setError("");
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        // Genres chip list
        // Chip List management
        final List<Chip> chipList = new ArrayList<>();
        // Chip list adapter
        ChipViewAdapter adapterOverride = new MainChipViewAdapter(getContext());
        final ChipView genresChipView = (ChipView) rootView.findViewById(R.id.chip_view);
        genresChipView.setAdapter(adapterOverride);
        genresChipView.setOnChipClickListener(new OnChipClickListener() {
            @Override
            public void onChipClick(Chip chip) {
                genresChipView.remove(chip);
            }
        });

        // AutoComplete
        //Creating the instance of ArrayAdapter containing list of music genres
        final String[] musicGenresList = getResources().getStringArray(R.array.music_genres);
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>
                (this.getContext(), android.R.layout.select_dialog_item, musicGenresList);
        //Getting the instance of AutoCompleteTextView
        final AutoCompleteTextView genreAutoCompleteTextView = (AutoCompleteTextView) rootView.findViewById(R.id.genre_autocomplete_text_view);
        genreAutoCompleteTextView.setThreshold(1); // will start working from first character
        genreAutoCompleteTextView.setAdapter(adapter); //setting the adapter data into the AutoCompleteTextView
        genreAutoCompleteTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // Close keyboard
                InputMethodManager in = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                in.hideSoftInputFromWindow(view.getApplicationWindowToken(), 0);
                // Clear text view
                genreAutoCompleteTextView.setText("");
                // Add chip
                chipList.add(new Tag(adapter.getItem(position), 1));
                // Remove selected item from list (to avoid duplicates)
                adapter.remove(adapter.getItem(position));
                adapter.notifyDataSetChanged();
                genresChipView.setChipList(chipList);
            }
        });

        genresChipView.setChipList(chipList);

        // Next button
        final Button nextButton = (Button) rootView.findViewById(R.id.next_button);
        nextButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Close keyboard
                InputMethodManager in = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                in.hideSoftInputFromWindow(rootView.getApplicationWindowToken(), 0);
                // Fetch form data
                String groupName = nameEditText.getText().toString();
                String groupDescription = descriptionEditText.getText().toString();
                Boolean coverBand = coverArtistCheckBox.isChecked();
                String coverArtists;
                if (coverBand) {
                    coverArtists = coverArtistEditText.getText().toString();
                } else {
                    coverArtists = null;
                }
                ArrayList<String> genreList = new ArrayList<String>();
                for (Chip chip : chipList) {
                    genreList.add(chip.getText());
                }
                if (mPresenter.validateData(groupName, groupDescription, coverBand, coverArtists, genreList, mLocation)) {
                    // Go to next fragment
                    ((CreateGroupActivity) getActivity()).goToPickInstrumentsFragment();
                }

            }
        });

        return rootView;
    }

    private ResultCallback<PlaceBuffer> updatePlaceDetailsCallback
            = new ResultCallback<PlaceBuffer>() {
        @Override
        public void onResult(PlaceBuffer places) {
            if (!places.getStatus().isSuccess()) {
                Log.e(TAG, "Place query did not complete. Error: " +
                        places.getStatus().toString());
                return;
            }
            // Selecting the first object buffer.
            final Place place = places.get(0);
            mLocation = place.getLatLng();
        }
    };

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        mPresenter.result(requestCode, resultCode, data);
    }

    @Override
    public void showError(String error) {
        switch (error) {
            case "name":
                nameTIL.setError("Please insert a name");
                break;
            case "location":
                locationTIL.setError("Please insert a valid location");
                break;
        }

    }

    public void showMessage(String message) {
        Snackbar.make(getView(), message, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showEnableGPSDialog() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

}
