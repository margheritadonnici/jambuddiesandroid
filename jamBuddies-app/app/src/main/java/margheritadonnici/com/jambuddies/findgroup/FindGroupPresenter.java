package margheritadonnici.com.jambuddies.findgroup;

import android.content.Context;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

import java.util.List;

import margheritadonnici.com.jambuddies.MainActivity;
import margheritadonnici.com.jambuddies.data.AppDataManager;
import margheritadonnici.com.jambuddies.data.DataManager;
import margheritadonnici.com.jambuddies.data.model.GroupWithMembers;
import margheritadonnici.com.jambuddies.data.model.JoinRequest;

/**
 * Created by Margherita Donnici on 12/3/17.
 */

public class FindGroupPresenter implements FindGroupContract.Presenter {

    private static final String TAG = "FindGroupPresenter";
    private final FindGroupContract.View mFindGroupView;
    private final AppDataManager mDataManager;
    private final Context mContext;
    private SearchFilter mSearchFilter = null;

    public FindGroupPresenter(FindGroupContract.View mFindGroupView, AppDataManager mDataManager, Context callerContext) {
        this.mFindGroupView = mFindGroupView;
        mFindGroupView.setPresenter(this);
        this.mDataManager = mDataManager;
        this.mContext = callerContext;
    }


    @Override
    public void start() {
        // Base search
        mSearchFilter = ((MainActivity) mContext).getSearchFilter();
        mSearchFilter.setInstrumentsFilter(mDataManager.getCurrentUserInstruments());
        mSearchFilter.setLocation(mDataManager.getUserStoredLocation());
    }

    public void searchForGroups(SearchFilter filter) {
        mFindGroupView.showLoadingDialog("Searching for groups...");
        mDataManager.getGroupsApiCall(filter, new DataManager.GetGroupsCallback() {
            @Override
            public void onGetGroupsSuccess(List<GroupWithMembers> groupList) {
                mFindGroupView.hideLoadingDialog();
                mFindGroupView.displayData(groupList);
            }

            @Override
            public void onGetGroupsFailure(String error) {
                mFindGroupView.hideLoadingDialog();
                mFindGroupView.showMessage("Unable to fetch groups");
            }
        });
    }

    @Override
    public LatLng getCurrentUserLocation() {
        return mDataManager.getUserStoredLocation();
    }

    @Override
    public String getCurrentUserId() {
        return mDataManager.getCurrentUserId();
    }

    @Override
    public List<String> getCurrentUserInstruments() {
        return mDataManager.getCurrentUserInstruments();
    }

    @Override
    public void createJoinRequest(JoinRequest req) {
        mFindGroupView.showLoadingDialog("Sending request...");
        mDataManager.createJoinRequestApiCall(req, new DataManager.CreateJoinRequestCallback() {
            @Override
            public void onCreateJoinRequestSuccess(JoinRequest req) {
                mFindGroupView.hideLoadingDialog();
                mFindGroupView.showMessage("Request sent. It will appear in your home screen.");
            }

            @Override
            public void onCreateJoinRequestFailure(String error) {
                mFindGroupView.hideLoadingDialog();
                mFindGroupView.showMessage(error);
            }
        });
    }
}
