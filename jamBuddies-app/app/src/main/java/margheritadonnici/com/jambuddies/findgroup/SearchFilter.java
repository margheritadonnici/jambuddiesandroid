package margheritadonnici.com.jambuddies.findgroup;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Margherita Donnici on 12/3/17.
 */

public class SearchFilter {

    private String nameFilter = null;
    private int rangeFilter = 0;
    private LatLng location = null;
    private List<String> instrumentsFilter = null;
    private List<String> genresFilter = null;


    public String getNameFilter() {
        return nameFilter;
    }

    public void setNameFilter(String nameFilter) {
        this.nameFilter = nameFilter;
    }

    public int getRangeFilter() {
        return rangeFilter;
    }

    public void setRangeFilter(int rangeFilter) {
        this.rangeFilter = rangeFilter;
    }

    public List<String> getInstrumentsFilter() {
        return instrumentsFilter;
    }

    public void setInstrumentsFilter(List<String> instrumentsFilter) {
        this.instrumentsFilter = instrumentsFilter;
    }

    public List<String> getGenresFilter() {
        return genresFilter;
    }

    public void setGenresFilter(List<String> genresFilter) {
        this.genresFilter = genresFilter;
    }

    public LatLng getLocation() {
        return location;
    }

    public void setLocation(LatLng location) {
        this.location = location;
    }
}
