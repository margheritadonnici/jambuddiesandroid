

package margheritadonnici.com.jambuddies.data.model;

import java.io.Serializable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Margherita Donnici on 1/11/18.
 */


public class JoinRequest implements Serializable {

    @SerializedName("to")
    @Expose
    private String to;
    @SerializedName("from")
    @Expose
    private String from;
    @SerializedName("groupId")
    @Expose
    private String groupId;
    @SerializedName("instrument")
    @Expose
    private String instrument;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("created")
    @Expose
    private String created;
    private final static long serialVersionUID = 3368885298312679478L;

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getInstrument() {
        return instrument;
    }

    public void setInstrument(String instrument) {
        this.instrument = instrument;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    @Override
    public String toString() {
        return "JoinRequest{" +
                ", to='" + to + '\'' +
                ", from='" + from + '\'' +
                ", groupId='" + groupId + '\'' +
                ", instrument='" + instrument + '\'' +
                ", status='" + status + '\'' +
                ", id='" + id + '\'' +
                ", created='" + created + '\'' +
                '}';
    }
}
