package margheritadonnici.com.jambuddies.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Margherita Donnici on 1/11/18.
 */

public class Comment implements Serializable {

    @SerializedName("userId")
    @Expose
    private String userId;
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("created")
    @Expose
    private String created;

    // Used for recycleView
    private String authorName;
    private String instrument;

    private final static long serialVersionUID = 3243762600408799659L;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public String getInstrument() {
        return instrument;
    }

    public void setInstrument(String instrument) {
        this.instrument = instrument;
    }

    @Override
    public String toString() {
        return "Comment{" +
                "userId='" + userId + '\'' +
                ", msg='" + msg + '\'' +
                ", created='" + created + '\'' +
                ", authorName='" + authorName + '\'' +
                ", instrument='" + instrument + '\'' +
                '}';
    }
}
