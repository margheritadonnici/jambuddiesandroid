package margheritadonnici.com.jambuddies.findgroup;

import android.content.Context;
import android.location.Address;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;

import margheritadonnici.com.jambuddies.MainActivity;
import margheritadonnici.com.jambuddies.data.AppDataManager;
import margheritadonnici.com.jambuddies.utils.LocationServicesHelper;

/**
 * Created by Margherita Donnici on 12/6/17.
 */

public class AddFilterPresenter implements AddFilterContract.Presenter,
        ActivityCompat.OnRequestPermissionsResultCallback,
        GoogleApiClient.OnConnectionFailedListener,
        GoogleApiClient.ConnectionCallbacks {

    private static final String TAG = "AddFilterPresenter";
    private AddFilterContract.View mAddFilterView;
    private Context mContext;
    private LocationServicesHelper mLocationHelper;
    private AppDataManager mDataManager;

    SearchFilter mSearchFilter;

    public AddFilterPresenter(AddFilterContract.View addFilterView, Context callerContext) {
        this.mAddFilterView = addFilterView;
        mAddFilterView.setPresenter(this);
        this.mContext = callerContext;
        mDataManager = AppDataManager.getInstance(mContext);
        mLocationHelper = new LocationServicesHelper(callerContext, this);
    }

    @Override
    public void start() {
        // Check availability of play services
        if (mLocationHelper.checkPlayServices()) {
            // Building the GoogleApi client
            mLocationHelper.buildGoogleApiClient();
        }
    }


    @Override
    public LatLng getUserLocation() {
        checkUserPermissions();
        if (!mLocationHelper.isGPSEnabled()) {
            mAddFilterView.showEnableGPSDialog();
        }
        Location userLocation = mLocationHelper.getLocation();
        LatLng returnLocation = null;

        if (userLocation != null) {
            returnLocation = new LatLng(userLocation.getLatitude(), userLocation.getLongitude());
        }
        return returnLocation;
    }

    @Override
    public LatLng getUserStoredLocation() {
        return mDataManager.getUserStoredLocation();
    }

    @Override
    public List<String> getUserInstruments() {
        return mDataManager.getCurrentUserInstruments();
    }

    @Override
    public String getLocationAddressString(LatLng location) {
        Address locationAddress = mLocationHelper.getAddress(location.latitude, location.longitude);
        return mLocationHelper.getAddressString(locationAddress);
    }

    @Override
    public GoogleApiClient getGoogleApiClient() {
        return mLocationHelper.getGoogleApiClient();
    }

    @Override
    public void checkUserPermissions() {
        mLocationHelper.checkpermission();
    }


    @Override
    public void createSearchFilter(String name, List<String> genres, List<String> instruments, LatLng location, int range) {
        mSearchFilter = ((MainActivity) mContext).getSearchFilter();
        mSearchFilter.setNameFilter(name);
        mSearchFilter.setGenresFilter(genres);
        mSearchFilter.setLocation(location);
        mSearchFilter.setRangeFilter(range);
        mSearchFilter.setInstrumentsFilter(instruments);
        mAddFilterView.onCreateFilterSuccess();
    }

    // This method is called when the user responds to the permission request
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        mLocationHelper.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    /**
     * Google connection callback methods
     */
    @Override
    public void onConnectionFailed(ConnectionResult result) {
        Log.e(TAG, "Google Places API connection failed with error code" + result.getErrorCode());
        mAddFilterView.showMessage("Google Places API connection failed");

    }

    @Override
    public void onConnected(Bundle arg0) {
        Log.i(TAG, "Google Places API connected.");
        // Once connected with google api, set the location AutoComplete adapter
        mAddFilterView.getPlaceArrayAdapter().setGoogleApiClient(mLocationHelper.getGoogleApiClient());

    }

    @Override
    public void onConnectionSuspended(int arg0) {
        Log.e(TAG, "Google Places API connection suspended.");
        mAddFilterView.getPlaceArrayAdapter().setGoogleApiClient(null);
        // Reconnect
        mLocationHelper.connectApiClient();
    }
}
