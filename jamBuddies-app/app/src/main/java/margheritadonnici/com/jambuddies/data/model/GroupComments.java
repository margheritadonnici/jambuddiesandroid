package margheritadonnici.com.jambuddies.data.model;

import java.io.Serializable;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Margherita Donnici on 1/11/18.
 */

public class GroupComments implements Serializable {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("comments")
    @Expose
    private List<Comment> comments = null;
    @SerializedName("existingMembers")
    @Expose
    private List<ExistingMemberWithUser> existingMembers = null;
    private final static long serialVersionUID = 6537796222152661636L;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public List<ExistingMemberWithUser> getExistingMembers() {
        return existingMembers;
    }

    public void setExistingMembers(List<ExistingMemberWithUser> existingMembers) {
        this.existingMembers = existingMembers;
    }

}