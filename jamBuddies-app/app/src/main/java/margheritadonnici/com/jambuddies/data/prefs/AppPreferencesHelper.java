package margheritadonnici.com.jambuddies.data.prefs;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Margherita Donnici on 11/15/17.
 */

// This class is responsible for reading and writing the data from android shared preferences.

public class AppPreferencesHelper implements PreferencesHelper {
    private static final String TAG = "AppPreferencesHelper";
    private static AppPreferencesHelper INSTANCE = null;
    private static final String PREF_NAME = "margheritadonnici.com.jambuddies"; // Sharedpref file name
    private static SharedPreferences.Editor mEditor;
    private final SharedPreferences mPrefs;
    private Context context;

    // All Shared Preferences Keys
    private static final String IS_LOGIN = "isLoggedIn";
    public static final String KEY_FIRSTNAME = "firstName";
    public static final String KEY_LASTNAME = "lastName";
    public static final String KEY_USER_ID = "userId";
    public static final String KEY_TOKEN = "token";
    public static final String KEY_FIRST_LOGIN = "firstLogin";
    public static final String KEY_LATITUDE = "location";
    public static final String KEY_LONGITUDE = "longitude";
    public static final String KEY_INSTRUMENTS = "instruments";

    public AppPreferencesHelper(Context context) {
        this.context = context;
        mPrefs = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        mEditor = mPrefs.edit();
    }

    // Returns the single instance of this class, creating it if necessary.
    public static AppPreferencesHelper getInstance(Context context) {
        if (INSTANCE == null) {
            INSTANCE = new AppPreferencesHelper(context);
        }
        return INSTANCE;
    }

    /**
     * Create login session
     */
    public void createLoginSession(String firstName, String lastName, String id, String token, boolean firstLogin,
                                   List<Double> location, List<String> instruments) {
        // Storing login value as TRUE
        mEditor.putBoolean(IS_LOGIN, true);

        // Storing values in mPrefs
        mEditor.putString(KEY_FIRSTNAME, firstName);
        mEditor.putString(KEY_LASTNAME, lastName);
        mEditor.putString(KEY_USER_ID, id);
        mEditor.putString(KEY_TOKEN, token);
        // Store if it's the user's first login or not
        mEditor.putBoolean(KEY_FIRST_LOGIN, firstLogin);
        // Instruments and location are NULL if we are creating the session after sign up (will be updated later)
        if (instruments != null) {
            // Convert instrument array to set
            Set<String> instrumentSet = new HashSet<String>(instruments);
            mEditor.putStringSet(KEY_INSTRUMENTS, instrumentSet);
        }
        // Store location as two longs
        if (location != null) {
            mEditor.putLong(KEY_LATITUDE, Double.doubleToRawLongBits(location.get(0)));
            mEditor.putLong(KEY_LONGITUDE, Double.doubleToRawLongBits(location.get(1)));
        }
        // Commit changes
        mEditor.commit();
        Log.i(TAG, "Created login session for user " + id);
    }

    /**
     * IsLoggedIn method will check user login status, returns TRUE if logged in FALSE if not
     */
    public boolean isLoggedIn() {
        return mPrefs.getBoolean(IS_LOGIN, false);
    }

    @Override
    public List<String> getCurrentUserInstruments() {
        Set<String> instrumentsSet = mPrefs.getStringSet(KEY_INSTRUMENTS, null);
        if (instrumentsSet != null) {
            return new ArrayList<String>(instrumentsSet);
        } else {
            return null;
        }
    }

    @Override
    public LatLng getUserStoredLocation() {
        Long latitude = mPrefs.getLong(KEY_LATITUDE, 0);
        Long longitude = mPrefs.getLong(KEY_LONGITUDE, 0);
        LatLng location = new LatLng(Double.longBitsToDouble(latitude),
                Double.longBitsToDouble(longitude));
        return location;
    }

    @Override
    public String getCurrentUserId() {
        return mPrefs.getString(KEY_USER_ID, null);
    }

    @Override
    public String getToken() {
        return mPrefs.getString(KEY_TOKEN, null);
    }

    @Override
    public String getCurrentUserFirstName() {
        return mPrefs.getString(KEY_FIRSTNAME, null);
    }

    @Override
    public boolean isFirstLogin() {
        return mPrefs.getBoolean(KEY_FIRST_LOGIN, false);
    }

    @Override
    public void setFirstLogin(boolean isFirstLogin) {
        mEditor.putBoolean(KEY_FIRST_LOGIN, isFirstLogin);
        mEditor.commit();
    }

    /**
     * Update login session (called after profile creation)
     */

    @Override
    public void updateUserSession(List<Double> location, List<String> instruments) {
        // Convert instrument array to set
        Set<String> instrumentSet = new HashSet<String>(instruments);
        mEditor.putStringSet(KEY_INSTRUMENTS, instrumentSet);
        // Store location as two longs
        if (location != null) {
            mEditor.putLong(KEY_LATITUDE, Double.doubleToRawLongBits(location.get(0)));
            mEditor.putLong(KEY_LONGITUDE, Double.doubleToRawLongBits(location.get(1)));
        }
        mEditor.commit();
        Log.i(TAG, "Updated login session for user ");
    }

    /**
     * Get all stored session data
     */
    public HashMap<String, String> getUserDetails() {
        HashMap<String, String> user = new HashMap<String, String>();

        user.put(KEY_FIRSTNAME, mPrefs.getString(KEY_FIRSTNAME, null));
        user.put(KEY_LASTNAME, mPrefs.getString(KEY_LASTNAME, null));
        user.put(KEY_USER_ID, mPrefs.getString(KEY_USER_ID, null));
        // Return filled HashMap
        return user;
    }

    /**
     * Clear session details
     */
    public void endLoginSession() {
        // Clearing all data from Shared Preferences
        mEditor.clear();
        mEditor.commit();
        Log.i(TAG, "Cleared shared preferences");
    }

}
