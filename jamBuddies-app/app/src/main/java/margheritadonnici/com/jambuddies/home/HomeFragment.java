package margheritadonnici.com.jambuddies.home;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import margheritadonnici.com.jambuddies.R;
import margheritadonnici.com.jambuddies.data.AppDataManager;
import margheritadonnici.com.jambuddies.data.model.JoinRequestWithDetails;
import margheritadonnici.com.jambuddies.data.model.UserRequests;
import margheritadonnici.com.jambuddies.data.prefs.PreferencesHelper;


/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment implements HomeContract.View {

    private static final String TAG = "HomeFragment";
    HomeContract.Presenter mPresenter;
    private static final String PREF_NAME = "margheritadonnici.com.jambuddies"; // Sharedpref file name
    private TextView greeting;
    private TextView receivedHeader;
    private TextView sentHeader;
    private ProgressDialog mProgressDialog;
    private RecyclerView mReceivedReqsRv;
    private RecyclerView mSentReqsRv;
    private ReceivedRequestListAdapter mReceivedReqsAdapter;
    private SentRequestListAdapter mSentReqsAdapter;
    private List<JoinRequestWithDetails> mReceivedReqsList = new ArrayList<>();
    private List<JoinRequestWithDetails> mSentReqsList = new ArrayList<>();

    public static HomeFragment newInstance() {
        HomeFragment fragment = new HomeFragment();
        return fragment;
    }

    @Override
    public void setPresenter(HomeContract.Presenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Set ActionBar Title
        getActivity().setTitle("Home");

        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_home, container, false);
        // Bind views
        greeting = (TextView) rootView.findViewById(R.id.greeting);
        mReceivedReqsRv = (RecyclerView) rootView.findViewById(R.id.received_req_rv);
        mSentReqsRv = (RecyclerView) rootView.findViewById(R.id.sent_req_rv);
        receivedHeader = (TextView) rootView.findViewById(R.id.received_header);
        sentHeader = (TextView) rootView.findViewById(R.id.sent_header);

        if (mReceivedReqsList.isEmpty()) {
            receivedHeader.setText("You have received no join requests.");
        } else {
            receivedHeader.setText("Received Requests");
        }
        if (mSentReqsList.isEmpty()) {
            sentHeader.setText("You have sent no join requests.");
        } else {
            sentHeader.setText("Sent Requests");
        }

        // RECEIVED REQUESTS RV
        // Create adapter passing in the req list
        mReceivedReqsAdapter = new ReceivedRequestListAdapter(getContext(), this, mReceivedReqsList);
        // Set layout manager to position the items
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        mReceivedReqsRv.setLayoutManager(layoutManager);
        // Attach the adapter to the recyclerview to populate items
        mReceivedReqsRv.setAdapter(mReceivedReqsAdapter);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(mReceivedReqsRv.getContext(), layoutManager.getOrientation());
        mReceivedReqsRv.addItemDecoration(dividerItemDecoration);

        // SENT REQUESTS RV
        // Create adapter passing in the req list
        mSentReqsAdapter = new SentRequestListAdapter(getContext(), this, mSentReqsList);
        // Set layout manager to position the items
        LinearLayoutManager layoutManagerSent = new LinearLayoutManager(getActivity());
        mSentReqsRv.setLayoutManager(layoutManagerSent);
        // Attach the adapter to the recyclerview to populate items
        mSentReqsRv.setAdapter(mSentReqsAdapter);
        DividerItemDecoration dividerItemDecorationSent = new DividerItemDecoration(mSentReqsRv.getContext(), layoutManager.getOrientation());
        mReceivedReqsRv.addItemDecoration(dividerItemDecoration);

        greeting.setText("Hi " + mPresenter.getCurrentUserFirstName() + "!");
        return rootView;
    }

    @Override
    public void displayData(UserRequests reqList) {
        mReceivedReqsList = reqList.getReceivedRequests();
        mReceivedReqsAdapter.updateGroupList(mReceivedReqsList);
        mReceivedReqsAdapter.notifyDataSetChanged();

        mSentReqsList = reqList.getSentRequests();
        mSentReqsAdapter.updateGroupList(mSentReqsList);
        mSentReqsAdapter.notifyDataSetChanged();

        if (mReceivedReqsList.isEmpty()) {
            receivedHeader.setText("You have received no join requests.");
        } else {
            receivedHeader.setText("Received Requests");
        }
        if (mSentReqsList.isEmpty()) {
            sentHeader.setText("You have sent no join requests.");
        } else {
            sentHeader.setText("Sent Requests");
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        // Refresh view
        greeting.setText("Hi " + mPresenter.getCurrentUserFirstName() + "!");
        mPresenter.start();
    }

    @Override
    public void showLoadingDialog(String msg) {
        mProgressDialog = new ProgressDialog(getContext(), R.style.Dark_Dialog);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage(msg);
        mProgressDialog.show();
    }

    @Override
    public void hideLoadingDialog() {
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
        }
    }

    public void showMessage(String message) {
        Snackbar.make(getView(), message, Snackbar.LENGTH_LONG).show();
    }
}
