package margheritadonnici.com.jambuddies.data.remote;

import android.graphics.Paint;

import java.util.ArrayList;
import java.util.List;

import margheritadonnici.com.jambuddies.data.model.Comment;
import margheritadonnici.com.jambuddies.data.model.ExistingMember;
import margheritadonnici.com.jambuddies.data.model.Group;
import margheritadonnici.com.jambuddies.data.model.GroupComments;
import margheritadonnici.com.jambuddies.data.model.GroupWithMembers;
import margheritadonnici.com.jambuddies.data.model.JoinRequest;
import margheritadonnici.com.jambuddies.data.model.JoinRequestWithDetails;
import margheritadonnici.com.jambuddies.data.model.User;
import margheritadonnici.com.jambuddies.data.model.UserRequests;
import margheritadonnici.com.jambuddies.data.model.UserWithGroups;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.Url;

/**
 * Created by Margherita Donnici on 11/15/17.
 */

// https://code.tutsplus.com/tutorials/sending-data-with-retrofit-2-http-client-for-android--cms-27845

public interface ApiService {

    // Create users
//    @POST("/users/")
//    @FormUrlEncoded
//    Call<User> createUser(@Field("firstName") String firstName,
//                          @Field("lastName") String lastName,
//                          @Field("email") String email,
//                          @Field("password") String password);

    @POST("/users")
    Call<User> createUser(@Body User user);

    // Authorization for login
    @POST("/users/auth/")
    Call<User> checkPassword(@Header("Authorization") String basicAuth);

    // Update user
    @PATCH
    @FormUrlEncoded
    Call<User> updateUser(@Url String url,
                          @Field("bio") String bio,
                          @Field("location") Double[] location,
                          @Field("dateOfBirth") String dateOfBirth,
                          @Field("youtubeChannel") String youtubeChannel,
                          @Field("fbPage") String fbPage,
                          @Field("profilePicture") String profilePicture,
                          @Field("instruments") String[] instruments);

    // Add group to user groups
    @PATCH
    @FormUrlEncoded
    Call<User> updateUserGroups(@Url String url,
                                @Field("groups") String groupId);

    // Get User by Id
    @GET
    Call<UserWithGroups> getUserById(@Url String url);

    // Create Group
    @POST("/groups")
    Call<Group> createGroup(@Body Group group);

    // Get groups
    @GET
    Call<List<GroupWithMembers>> getGroups(@Url String url);

    // Get group comment
    @GET
    Call<GroupComments> getGroupComments(@Url String url);

    // Add comment to group
    @PATCH
    @FormUrlEncoded
    Call<List<Comment>> addComment(@Url String url,
                                   @Field("msg") String message,
                                   @Field("userId") String userId);

    // Delete group
    @DELETE
    Call<String> deleteGroup(@Url String url);

    //Create Join Request
    @POST("/requests")
    @FormUrlEncoded
    Call<JoinRequest> createJoinRequest(@Field("to") String to,
                                        @Field("from") String from,
                                        @Field("status") String status,
                                        @Field("groupId") String groupId,
                                        @Field("instrument") String instrument);

    // Get Requests
    @GET
    Call<UserRequests> getUserRequests(@Url String url);

    // Update Request Status
    @PATCH
    @FormUrlEncoded
    Call<JoinRequest> updateRequest(@Url String url,
                                    @Field("status") String status);

    // Add new member to group
    @PATCH
    @FormUrlEncoded
    Call<Group> updateGroupMembers(@Url String url,
                                   @Field("userId") String userId,
                                   @Field("instrument") String instrument);
}
