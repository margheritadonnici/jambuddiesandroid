package margheritadonnici.com.jambuddies.data;

import android.content.Context;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.List;

import margheritadonnici.com.jambuddies.data.model.Comment;
import margheritadonnici.com.jambuddies.data.model.ExistingMember;
import margheritadonnici.com.jambuddies.data.model.Group;
import margheritadonnici.com.jambuddies.data.model.GroupComments;
import margheritadonnici.com.jambuddies.data.model.GroupWithMembers;
import margheritadonnici.com.jambuddies.data.model.JoinRequest;
import margheritadonnici.com.jambuddies.data.model.User;
import margheritadonnici.com.jambuddies.data.model.UserRequests;
import margheritadonnici.com.jambuddies.data.model.UserWithGroups;
import margheritadonnici.com.jambuddies.data.prefs.AppPreferencesHelper;
import margheritadonnici.com.jambuddies.data.remote.ApiService;
import margheritadonnici.com.jambuddies.data.prefs.PreferencesHelper;
import margheritadonnici.com.jambuddies.data.remote.ApiUtils;
import margheritadonnici.com.jambuddies.findgroup.SearchFilter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Margherita Donnici on 11/15/17.
 */

// This is the one point of contact for any data related operation in the application.
// It delegates all the operations specific to any Helper.
// https://blog.mindorks.com/essential-guide-for-designing-your-android-app-architecture-mvp-part-1-74efaf1cda40

public class AppDataManager implements DataManager {

    private static final String TAG = "AppDataManager";
    private static AppDataManager INSTANCE = null;
    private final PreferencesHelper mPreferencesHelper;
    private ApiService mApiService;

    public AppDataManager(Context context) {
        this.mPreferencesHelper = AppPreferencesHelper.getInstance(context);
        this.mApiService = ApiUtils.getAPIService(getToken());
    }


    // Returns the single instance of this class, creating it if necessary.
    public static AppDataManager getInstance(Context context) {
        if (INSTANCE == null) {
            Log.e(TAG, "Creating new appDataManager instance");
            INSTANCE = new AppDataManager(context);
        }
        return INSTANCE;
    }

    public void updateApiClient() {
        // Create new API Client instance with logged in user's token
        Log.i(TAG, "Initialized new API client with token: " + getToken());
        this.mApiService = ApiUtils.getAPIService(getToken());
    }

    /*
    *  API METHODS
    * */

    // Inserts new user in db
    public void createUserApiCall(String firstName, String lastName, String email, String password, final SignUpCallback callback) {
        User tmpUser = new User();
        tmpUser.setFirstName(firstName);
        tmpUser.setLastName(lastName);
        tmpUser.setEmail(email);
        tmpUser.setPassword(password);

        mApiService.createUser(tmpUser/*firstName, lastName, email, password*/).enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if (response.code() == 400) {
                    Log.i(TAG, "createUserApiCall: error 400");
                    callback.onSignUpFailure("An account with this email already exists.");
                }
                if (response.code() == 500) {
                    Log.e(TAG, "createUserApiCall: error 500");
                    callback.onSignUpFailure("There was a problem signing up.");
                }
                if (response.isSuccessful()) {
                    Log.i(TAG, "user submitted to API." + response.body().toString());
                    callback.onSignUpSuccess(response.body().getId(),
                            response.body().getToken(),
                            response.body().getLocation(),
                            response.body().getInstruments());
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                callback.onSignUpFailure(t.getMessage());
                Log.e(TAG, "Unable to submit user to API: " + t.getMessage());
            }
        });
    }

    // Login authentication (checks if user with that password and email exists)
    public void authenticateUserApiCall(String email, String password, final LoginCallback callback) {
        mApiService.checkPassword(ApiUtils.getAuthToken(email, password)).enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if (response.code() == 401) {
                    Log.i(TAG, "authenticateUserApiCall: error 404");
                    callback.onLoginFailure("Username or password incorrect");
                }
                if (response.code() == 500) {
                    Log.e(TAG, "authenticateUserApiCall: error 500");
                    callback.onLoginFailure("There was a problem signing in.");
                }
                if (response.isSuccessful()) {
                    Log.i(TAG, "user authenticated.");
                    callback.onLoginSuccess(response.body().getFirstName(),
                            response.body().getLastName(),
                            response.body().getId(),
                            response.body().getToken(),
                            response.body().getLocation(),
                            response.body().getInstruments());
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                callback.onLoginFailure(t.getMessage());
                Log.e(TAG, "Unable to login: " + t.getMessage());
            }
        });
    }

    public void updateUserApiCall(String bio,
                                  Double[] location,
                                  String dateOfBirth,
                                  String youtubeChannel,
                                  String fbPage,
                                  String profilePicture,
                                  String[] instruments,
                                  final UserUpdateCallback callback) {
        String url = "/users/" + getCurrentUserId();
        Log.i(TAG, "Updating user on url: " + url);
        mApiService.updateUser(url, bio, location, dateOfBirth, youtubeChannel, fbPage, profilePicture, instruments)
                .enqueue(new Callback<User>() {
                    @Override
                    public void onResponse(Call<User> call, Response<User> response) {
                        if (response.code() == 401) {
                            Log.e(TAG, "updateUserApiCall: Unauthorized");
                            callback.onUserUpdateFailure("User not authenticated");
                        }
                        if (response.code() == 404) {
                            Log.e(TAG, "updateUserApiCall: user not found");
                            callback.onUserUpdateFailure("User not found");
                        }
                        if (response.code() == 500) {
                            Log.e(TAG, "updateUserApiCall: error 500");
                            callback.onUserUpdateFailure("There was a problem updating the user.");
                        }
                        if (response.isSuccessful()) {
                            Log.i(TAG, "user updated.");
                            callback.onUserUpdateSuccess(response.body().getInstruments(), response.body().getLocation());
                        }
                    }

                    @Override
                    public void onFailure(Call<User> call, Throwable t) {
                        callback.onUserUpdateFailure(t.getMessage());
                        Log.e(TAG, "Unable to update user: " + t.getMessage());
                    }
                });
    }

    public void getUserByIdApiCall(String id, final GetUserByIdCallback callback) {
        String url = "/users/" + id;
        Log.i(TAG, "Getting user on url: " + url);
        mApiService.getUserById(url).enqueue(new Callback<UserWithGroups>() {
            @Override
            public void onResponse(Call<UserWithGroups> call, Response<UserWithGroups> response) {
                if (response.code() == 401) {
                    Log.e(TAG, "getUserByIdApiCall: Unauthorized");
                    callback.onGetUserFailure("User not authenticated");
                }
                if (response.code() == 404) {
                    Log.e(TAG, "getUserByIdApiCall: user not found");
                    callback.onGetUserFailure("User not found");
                }
                if (response.isSuccessful()) {
                    Log.i(TAG, "User fetched.");
                    callback.onGetUserSuccess(response.body());
                }

            }

            @Override
            public void onFailure(Call<UserWithGroups> call, Throwable t) {
                Log.e(TAG, "Unable to fetch user");
                callback.onGetUserFailure(t.getMessage());
            }
        });
    }

    public void getGroupsApiCall(SearchFilter filter, final GetGroupsCallback callback) {
        // /groups?near=-44.332113,-44.312311&max_distance=200000000000&instrument=Drums&genre=Pop&name=furthest
        // Build querystring
        String url = "/groups?"; // base url
        // Add location
        url += "near=" + String.valueOf(filter.getLocation().latitude) + "," + String.valueOf(filter.getLocation().longitude);
        // Add user instruments
        url += "&instrument=";
        for (String instrument : filter.getInstrumentsFilter()) {
            url += instrument;
            if (filter.getInstrumentsFilter().indexOf(instrument) != filter.getInstrumentsFilter().size() - 1) {
                url += ",";
            }
        }
        if (filter.getNameFilter() != null) {
            if (!filter.getNameFilter().isEmpty()) {
                // A name filter was specified
                url += "&name=" + filter.getNameFilter();
            }
        }
        if (filter.getRangeFilter() != 0) {
            // A range filter was specified
            int rangeInMeters = filter.getRangeFilter() * 1000;
            url += "&max_distance=" + String.valueOf(rangeInMeters);
        }
        if (filter.getGenresFilter() != null) {
            if (!filter.getGenresFilter().isEmpty()) {
                // A genre filter was specified
                url += "&genre=";
                for (String genre : filter.getGenresFilter()) {
                    url += genre;
                    if (filter.getGenresFilter().indexOf(genre) != filter.getGenresFilter().size() - 1) {
                        url += ",";
                    }
                }
            }
        }
        Log.i(TAG, "Getting group on url: " + url);
        mApiService.getGroups(url).enqueue(new Callback<List<GroupWithMembers>>() {
            @Override
            public void onResponse(Call<List<GroupWithMembers>> call, Response<List<GroupWithMembers>> response) {
                if (response.code() == 401) {
                    Log.e(TAG, "getGroupsApiCall: Unauthorized");
                    callback.onGetGroupsFailure("User not authenticated");
                }
                if (response.code() == 404) {
                    Log.e(TAG, "getGroupsApiCall: group not found");
                    callback.onGetGroupsFailure("User not found");
                }
                if (response.isSuccessful()) {
                    Log.i(TAG, "Groups fetched.");
                    callback.onGetGroupsSuccess(response.body());
                }
            }

            @Override
            public void onFailure(Call<List<GroupWithMembers>> call, Throwable t) {
                Log.e(TAG, "Unable to fetch groups");
                callback.onGetGroupsFailure(t.getMessage());
            }
        });
    }

    public void addGroupToUserApiCall(String id, String groupId, final AddGroupToUserCallback callback) {
        String url = "/users/" + id + "/groups";
        mApiService.updateUserGroups(url, groupId).enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {

                if (response.code() == 401) {
                    Log.e(TAG, "updateUserGroupsApiCall: Unauthorized");
                    callback.onAddGroupFailure("User not authenticated");
                }
                if (response.code() == 404) {
                    Log.e(TAG, "updateUserGroupsApiCall: user not found");
                    callback.onAddGroupFailure("User not found");
                }
                if (response.code() == 500) {
                    Log.e(TAG, "updateUserGroupsApiCall: error 500");
                    callback.onAddGroupFailure("There was a problem updating the user.");
                }
                if (response.isSuccessful()) {
                    Log.i(TAG, "user groups updated.");
                    callback.onAddGroupSuccess(response.body());
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Log.e(TAG, "Unable to update user groups" + t.getMessage());
                callback.onAddGroupFailure(t.getMessage());
            }
        });
    }

    public void createGroupApiCall(Group newGroup,
                                   final CreateGroupCallback callback) {
        mApiService.createGroup(newGroup).enqueue(new Callback<Group>() {
            @Override
            public void onResponse(Call<Group> call, Response<Group> response) {
                if (response.code() == 401) {
                    Log.e(TAG, "createGroupApiCall: Unauthorized");
                    callback.onCreateGroupFailure("User not authenticated");
                }
                if (response.code() == 500) {
                    Log.e(TAG, "createGroupApiCall: error 500");
                    callback.onCreateGroupFailure("There was a problem creating the group.");
                }
                if (response.isSuccessful()) {
                    Log.i(TAG, "Group created: " + response.body().toString());
                    callback.onCreateGroupSuccess(response.body().getId());
                }
                Log.e(TAG, "In OnResponse: " + response.body().toString());
            }

            @Override
            public void onFailure(Call<Group> call, Throwable t) {
                Log.e(TAG, "Unable to create group: " + t.getMessage());
                callback.onCreateGroupFailure(t.getMessage());
            }
        });
    }

    public void getGroupCommentsApiCall(String groupId, final GetGroupCommentsCallback callback) {
        String url = "/groups/" + groupId + "/comments";
        mApiService.getGroupComments(url).enqueue(new Callback<GroupComments>() {
            @Override
            public void onResponse(Call<GroupComments> call, Response<GroupComments> response) {
                if (response.code() == 401) {
                    Log.e(TAG, "getGroupCommentdApiCall: Unauthorized");
                    callback.onGetGroupCommentsFailure("User not authenticated");
                }
                if (response.code() == 404) {
                    Log.e(TAG, "getGroupCommentsApiCall: Group not found");
                    callback.onGetGroupCommentsFailure("Group not found");
                }
                if (response.isSuccessful()) {
                    Log.i(TAG, "Comments fetched.");
                    callback.onGetGroupCommentsSuccess(response.body());
                }
            }

            @Override
            public void onFailure(Call<GroupComments> call, Throwable t) {
                Log.e(TAG, "Unable to fetch comments");
                callback.onGetGroupCommentsFailure(t.getMessage());
            }
        });
    }

    public void addCommentApiCall(String groupId, String msg, String userId, final AddCommentCallback callback) {
        String url = "/groups/" + groupId + "/comments";
        mApiService.addComment(url, msg, userId).enqueue(new Callback<List<Comment>>() {
            @Override
            public void onResponse(Call<List<Comment>> call, Response<List<Comment>> response) {
                if (response.code() == 401) {
                    Log.e(TAG, "addCommentApiCall: Unauthorized");
                    callback.onAddCommentFailure("User not authenticated");
                }
                if (response.code() == 404) {
                    Log.e(TAG, "addCommentApiCall: Group not found");
                    callback.onAddCommentFailure("Group not found");
                }
                if (response.isSuccessful()) {
                    Log.i(TAG, "Comments fetched.");
                    callback.onAddCommentSuccess(response.body());
                }
            }

            @Override
            public void onFailure(Call<List<Comment>> call, Throwable t) {
                Log.e(TAG, "Unable to submit comment.");
                callback.onAddCommentFailure(t.getMessage());
            }
        });
    }

    public void deleteGroupApiCall(String groupId, final DeleteGroupCallback callback) {
        String url = "/groups/" + groupId;
        mApiService.deleteGroup(url).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.code() == 401) {
                    Log.e(TAG, "deleteGroupApiCall: Unauthorized");
                    callback.onDeleteGroupFailure("User not authenticated");
                }
                if (response.code() == 404) {
                    Log.e(TAG, "deleteGroupApiCall: Group not found");
                    callback.onDeleteGroupFailure("Group not found");
                }
                if (response.isSuccessful()) {
                    Log.i(TAG, "Group deleted");
                    callback.onDeleteGroupSuccess(response.body());
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.e(TAG, "Unable to delete group: " + t.getMessage());
                callback.onDeleteGroupFailure(t.getMessage());
            }
        });
    }

    public void createJoinRequestApiCall(JoinRequest req, final CreateJoinRequestCallback callback) {
        mApiService.createJoinRequest(req.getTo(), req.getFrom(), req.getStatus(), req.getGroupId(), req.getInstrument()).enqueue(new Callback<JoinRequest>() {
            @Override
            public void onResponse(Call<JoinRequest> call, Response<JoinRequest> response) {
                if (response.code() == 401) {
                    Log.e(TAG, "createJoinRequestApiCall: Unauthorized");
                    callback.onCreateJoinRequestFailure("User not authenticated");
                }
                if (response.code() == 400) {
                    Log.i(TAG, "createJoinRequestApiCall: error 400");
                    callback.onCreateJoinRequestFailure("You already have an existing join request for this instrument in this group.");
                }
                if (response.isSuccessful()) {
                    Log.i(TAG, "Request created");
                    callback.onCreateJoinRequestSuccess(response.body());
                }
            }

            @Override
            public void onFailure(Call<JoinRequest> call, Throwable t) {
                Log.e(TAG, "Unable to create join request: " + t.getMessage());
                callback.onCreateJoinRequestFailure(t.getMessage());
            }
        });
    }

    public void getUserRequestsApiCall(String userId, final GetUserRequestsCallback callback) {
        String url = "/users/" + userId + "/requests";
        mApiService.getUserRequests(url).enqueue(new Callback<UserRequests>() {
            @Override
            public void onResponse(Call<UserRequests> call, Response<UserRequests> response) {
                if (response.code() == 401) {
                    Log.e(TAG, "getUserRequestsApiCall: Unauthorized");
                    callback.onGetUserRequestsFailure("User not authenticated");
                }
                if (response.code() == 404) {
                    Log.e(TAG, "getUserRequestsApiCall: User not found");
                    callback.onGetUserRequestsFailure("User not found");
                }
                if (response.isSuccessful()) {
                    Log.i(TAG, "Requests fetched.");
                    callback.onGetUserRequestsSuccess(response.body());
                }
            }

            @Override
            public void onFailure(Call<UserRequests> call, Throwable t) {
                Log.e(TAG, "Unable to fetch requests " + t.getMessage());
                callback.onGetUserRequestsFailure(t.getMessage());
            }
        });
    }

    public void updateRequestStatusApiCall(String reqId, String status, final UpdateRequestStatusCallback callback) {
        String url = "/requests/" + reqId;
        mApiService.updateRequest(url, status).enqueue(new Callback<JoinRequest>() {
            @Override
            public void onResponse(Call<JoinRequest> call, Response<JoinRequest> response) {
                if (response.code() == 401) {
                    Log.e(TAG, "updateRequestStatusApiCall: Unauthorized");
                    callback.onUpdateRequestStatusFailure("User not authenticated");
                }
                if (response.code() == 404) {
                    Log.e(TAG, "updateRequestStatusApiCall: Request not found");
                    callback.onUpdateRequestStatusFailure("Request not found");
                }
                if (response.isSuccessful()) {
                    Log.i(TAG, "Request updated.");
                    callback.onUpdateRequestStatusSuccess(response.body());
                }
            }

            @Override
            public void onFailure(Call<JoinRequest> call, Throwable t) {
                Log.e(TAG, "Unable to update request status. " + t.getMessage());
                callback.onUpdateRequestStatusFailure(t.getMessage());
            }
        });
    }

    public void addMemberToGroupApiCall(String groupId, ExistingMember newMember, final UpdateGroupMembersCallback callback) {
        String url = "groups/" + groupId;
        mApiService.updateGroupMembers(url, newMember.getUserId(), newMember.getInstrument()).enqueue(new Callback<Group>() {
            @Override
            public void onResponse(Call<Group> call, Response<Group> response) {
                if (response.code() == 401) {
                    Log.e(TAG, "addMemberToGroupApiCall: Unauthorized");
                    callback.onUpdateGroupMembersFailure("User not authenticated");
                }
                if (response.code() == 404) {
                    Log.e(TAG, "addMemberToGroupApiCall: Group not found");
                    callback.onUpdateGroupMembersFailure("Group not found");
                }
                if (response.isSuccessful()) {
                    Log.i(TAG, "Group updated.");
                    callback.onUpdateGroupMembersSuccess(response.body());
                }
            }

            @Override
            public void onFailure(Call<Group> call, Throwable t) {
                Log.e(TAG, "Unable to update group. " + t.getMessage());
                callback.onUpdateGroupMembersFailure(t.getMessage());
            }
        });
    }
    /*
        *  SHARED PREFS METHODS
        * */
    @Override
    public void createLoginSession(String firstName, String lastName, String id, String token, boolean isFirstLogin, List<Double> location, List<String> instruments) {
        mPreferencesHelper.createLoginSession(firstName, lastName, id, token, isFirstLogin, location, instruments);
    }

    @Override
    public void endLoginSession() {
        mPreferencesHelper.endLoginSession();
    }

    @Override
    public boolean isLoggedIn() {
        return mPreferencesHelper.isLoggedIn();
    }

    @Override
    public List<String> getCurrentUserInstruments() {
        return mPreferencesHelper.getCurrentUserInstruments();
    }

    @Override
    public LatLng getUserStoredLocation() {
        return mPreferencesHelper.getUserStoredLocation();
    }

    @Override
    public String getCurrentUserId() {
        return mPreferencesHelper.getCurrentUserId();
    }

    @Override
    public String getToken() {
        return mPreferencesHelper.getToken();
    }

    @Override
    public String getCurrentUserFirstName() {
        return mPreferencesHelper.getCurrentUserFirstName();
    }

    @Override
    public boolean isFirstLogin() {
        return mPreferencesHelper.isFirstLogin();
    }

    @Override
    public void setFirstLogin(boolean isFirstLogin) {
        mPreferencesHelper.setFirstLogin(isFirstLogin);
    }

    @Override
    public void updateUserSession(List<Double> location, List<String> instruments) {
        mPreferencesHelper.updateUserSession(location, instruments);
    }
}
