package margheritadonnici.com.jambuddies.home;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import margheritadonnici.com.jambuddies.R;
import margheritadonnici.com.jambuddies.data.model.JoinRequestWithDetails;
import margheritadonnici.com.jambuddies.utils.ImageUtils;

/**
 * Created by Margherita Donnici on 1/12/18.
 */

public class SentRequestListAdapter extends RecyclerView.Adapter<SentRequestListAdapter.ViewHolder> {

    private static final String TAG = "SentRequestListAdapter";
    private List<JoinRequestWithDetails> mRequests;
    private Context mContext;
    private HomeFragment mFragment;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView groupName;
        public TextView instrument;
        public Button statusButton;
        public ImageView icon;
        public View layout;

        public ViewHolder(View itemView) {
            super(itemView);
            layout = itemView;
            groupName = (TextView) itemView.findViewById(R.id.group_name);
            instrument = (TextView) itemView.findViewById(R.id.instrument);
            statusButton = (Button) itemView.findViewById(R.id.status_badge);
            icon = (ImageView) itemView.findViewById(R.id.icon);
        }
    }

    // Pass in the group array into the constructor
    public SentRequestListAdapter(Context context, HomeFragment myFragment, List<JoinRequestWithDetails> reqList) {
        mRequests = reqList;
        mContext = context;
        mFragment = myFragment;
    }

    // Easy access to the context object in the recyclerview
    private Context getContext() {
        return mContext;
    }

    public void add(JoinRequestWithDetails item) {
        mRequests.add(item);
        int position = mRequests.size() - 1;
        if (position < 0) {
            notifyItemInserted(0);
        } else {
            notifyItemInserted(mRequests.size() - 1);
        }
        // TODO
        //   mFragment.getEmptyListView().setVisibility(View.GONE);
    }

    public void updateGroupList(List<JoinRequestWithDetails> newlist) {
        mRequests.clear();
        mRequests.addAll(newlist);
        this.notifyDataSetChanged();
    }

    public void remove(int position) {
        mRequests.remove(position);
        notifyItemRemoved(position);
        if (mRequests.isEmpty()) {
            //  mFragment.getEmptyListView().setVisibility(View.VISIBLE);
        }
    }

    // Create new views (invoked by the layout manager)
    @Override
    public SentRequestListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View groupView;
        groupView = inflater.inflate(R.layout.sent_request_layout, parent, false);
        // set the view's size, margins, paddings and layout parameters
        SentRequestListAdapter.ViewHolder viewHolder = new SentRequestListAdapter.ViewHolder(groupView);
        return viewHolder;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final SentRequestListAdapter.ViewHolder holder, final int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        final JoinRequestWithDetails req = mRequests.get(position);
        holder.groupName.setText(req.getGroupId().getGroupName());
        holder.instrument.setText(req.getInstrument());
        holder.icon.setImageResource(ImageUtils.getInstrumentImage(req.getInstrument()));
        holder.statusButton.setText(req.getStatus());
        switch (req.getStatus()) {
            case "Rejected":
                holder.statusButton.setBackgroundColor(Color.RED);
                break;
            case "Accepted":
                holder.statusButton.setBackgroundColor(Color.rgb(0, 128, 0));
                break;
            default:
                break; //remain blue
        }
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mRequests.size();
    }
}
