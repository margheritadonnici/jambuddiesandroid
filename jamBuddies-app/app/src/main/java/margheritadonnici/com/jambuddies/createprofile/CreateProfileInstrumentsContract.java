package margheritadonnici.com.jambuddies.createprofile;

import java.util.ArrayList;

import margheritadonnici.com.jambuddies.BasePresenter;
import margheritadonnici.com.jambuddies.BaseView;

/**
 * Created by Margherita Donnici on 11/22/17.
 */

public interface CreateProfileInstrumentsContract {
    interface View extends BaseView<Presenter> {
        void showMessage(String msg);

        void showUpdateSuccess();
    }

    interface Presenter extends BasePresenter {
        void saveData(ArrayList<String> instrumentsList);
    }
}
