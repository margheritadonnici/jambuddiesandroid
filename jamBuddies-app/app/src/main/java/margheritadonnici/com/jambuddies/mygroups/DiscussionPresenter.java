package margheritadonnici.com.jambuddies.mygroups;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import margheritadonnici.com.jambuddies.data.AppDataManager;
import margheritadonnici.com.jambuddies.data.DataManager;
import margheritadonnici.com.jambuddies.data.model.Comment;
import margheritadonnici.com.jambuddies.data.model.ExistingMemberWithUser;
import margheritadonnici.com.jambuddies.data.model.GroupComments;

/**
 * Created by Margherita Donnici on 1/11/18.
 */

public class DiscussionPresenter implements DiscussionContract.Presenter {

    public static final String TAG = "DiscussionPresenter";
    private final DiscussionContract.View mDiscussionView;
    private final AppDataManager mDataManager;
    private final String mGroupId;

    public DiscussionPresenter(DiscussionContract.View mDiscussionView, String id, AppDataManager mDataManager) {
        this.mDiscussionView = mDiscussionView;
        mDiscussionView.setPresenter(this);
        mGroupId = id;
        this.mDataManager = mDataManager;
    }


    @Override
    public void start() {
        //TODO
    }


    @Override
    public List<Comment> getGroupComments() {
        mDiscussionView.showLoadingDialog("Fetching comments...");
        // Get comment list
        mDataManager.getGroupCommentsApiCall(mGroupId, new DataManager.GetGroupCommentsCallback() {
            @Override
            public void onGetGroupCommentsSuccess(GroupComments groupComments) {
                List<Comment> commentList = groupComments.getComments();
                for (Comment comment : commentList) {
                    // For each comment, find corresponding author name and instrument
                    for (ExistingMemberWithUser member : groupComments.getExistingMembers()) {
                        if (member.getUserId().getId().equals(comment.getUserId())) {
                            comment.setAuthorName(member.getUserId().getFirstName() + " " + member.getUserId().getLastName());
                            comment.setInstrument(member.getInstrument());
                        }
                    }
                }
                mDiscussionView.hideLoadingDialog();
                mDiscussionView.displayData(commentList);

            }

            @Override
            public void onGetGroupCommentsFailure(String error) {
                mDiscussionView.hideLoadingDialog();
                mDiscussionView.showMessage(error);
            }
        });
        return null;
    }

    @Override
    public void submitComment(String msg) {
        mDiscussionView.showLoadingDialog("Submitting comment...");
        mDataManager.addCommentApiCall(mGroupId, msg, mDataManager.getCurrentUserId(), new DataManager.AddCommentCallback() {
            @Override
            public void onAddCommentSuccess(List<Comment> commentList) {
                mDiscussionView.hideLoadingDialog();
                // Refresh comment list
                getGroupComments();
            }

            @Override
            public void onAddCommentFailure(String error) {
                mDiscussionView.hideLoadingDialog();
                mDiscussionView.showMessage(error);
            }
        });
    }


}
