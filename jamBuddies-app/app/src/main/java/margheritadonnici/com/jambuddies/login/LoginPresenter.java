package margheritadonnici.com.jambuddies.login;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import margheritadonnici.com.jambuddies.data.AppDataManager;
import margheritadonnici.com.jambuddies.data.DataManager;

/**
 * Created by Margherita Donnici on 10/28/17.
 */

public class LoginPresenter implements LoginContract.Presenter {

    private static final String TAG = "LoginPresenter";

    private final LoginContract.View mLoginView;
    private final AppDataManager mDataManager;

    public LoginPresenter(LoginContract.View loginView, AppDataManager appDataManager) {
        this.mLoginView = loginView;
        this.mLoginView.setPresenter(this);
        this.mDataManager = appDataManager;
    }

    public void login(String email, String password) {
        // Form validation
        boolean valid = true;
        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            mLoginView.showError("email");
            valid = false;
        } else {
            mLoginView.removeError("email");
        }

        if (password.isEmpty()) {
            mLoginView.showError("password");
            valid = false;
        } else {
            mLoginView.removeError("password");
        }

        if (!valid) {
            // If form is not valid, do not continue
            return;
        }

        // Form is valid: proceed with login
        mLoginView.showLoadingDialog();
        mDataManager.authenticateUserApiCall(email, password, new DataManager.LoginCallback() {
            @Override
            public void onLoginSuccess(String firstName, String lastName, String id, String token, List<Double> location, List<String> instruments) {
                mDataManager.createLoginSession(firstName, lastName, id, token, false, location, instruments);
                mDataManager.updateApiClient();
                mLoginView.showLoginSuccess();
            }

            @Override
            public void onLoginFailure(String error) {
                mLoginView.showLoginError(error);
            }
        });
    }

    @Override
    public void onSignUpResult(int requestCode, int resultCode, Intent data) {
        // If sign up was successful
        if (LoginActivity.REQUEST_SIGNUP == requestCode && Activity.RESULT_OK == resultCode) {
            // Create login session
            Log.i(TAG, "userId: " + data.getStringExtra("userId"));
            Log.e(TAG, "token: " + data.getStringExtra("token"));
            mDataManager.createLoginSession(data.getStringExtra("firstName"),
                    data.getStringExtra("lastName"),
                    data.getStringExtra("userId"),
                    data.getStringExtra("token"),
                    true,
                    null,
                    null);
            mDataManager.updateApiClient();
            mLoginView.showSuccessfulSignUp();
        }

    }

    @Override
    public void start() {
        //TODO
    }
}
