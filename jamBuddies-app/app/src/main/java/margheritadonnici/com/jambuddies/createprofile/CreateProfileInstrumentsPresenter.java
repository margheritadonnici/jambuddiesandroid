package margheritadonnici.com.jambuddies.createprofile;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

import margheritadonnici.com.jambuddies.data.AppDataManager;
import margheritadonnici.com.jambuddies.data.DataManager;
import margheritadonnici.com.jambuddies.data.model.User;

/**
 * Created by Margherita Donnici on 11/22/17.
 */

public class CreateProfileInstrumentsPresenter implements CreateProfileInstrumentsContract.Presenter {

    private static final String TAG = "ProfileInstrumentsPresenter";
    private final CreateProfileInstrumentsContract.View mProfileInstrumentsView;
    private final AppDataManager mDataManager;
    private Context mCallerContext;

    User mUserdata;

    public CreateProfileInstrumentsPresenter(CreateProfileInstrumentsContract.View mProfileInstrumentsView, Context callerContext) {
        this.mProfileInstrumentsView = mProfileInstrumentsView;
        mProfileInstrumentsView.setPresenter(this);
        this.mDataManager = AppDataManager.getInstance(callerContext);
        this.mCallerContext = callerContext;
    }


    @Override
    public void start() {
        mUserdata = ((CreateProfileActivity) mCallerContext).getUserData();
    }

    @Override
    public void saveData(ArrayList<String> instrumentsList) {
        if (instrumentsList.isEmpty()) {
            mProfileInstrumentsView.showMessage("Please select at least one instrument.");
        } else {
            mUserdata.setInstruments(instrumentsList);
            // Convert ArrayLists to Arrays
            String[] userInstrumentsArray = mUserdata.getInstruments().toArray(new String[instrumentsList.size()]);
            Double[] userLocationArray;
            if (mUserdata.getLocation() != null) {
                userLocationArray = mUserdata.getLocation().toArray(new Double[2]);
            } else {
                userLocationArray = null;
            }
            // API call to save data on db
            mDataManager.updateUserApiCall(mUserdata.getBio(),
                    userLocationArray,
                    mUserdata.getDateOfBirth(),
                    mUserdata.getYoutubeChannel(),
                    mUserdata.getFbPage(),
                    mUserdata.getProfilePicture(),
                    userInstrumentsArray,
                    new DataManager.UserUpdateCallback() {
                        @Override
                        public void onUserUpdateSuccess(List<String> instruments, List<Double> location) {
                            // Update session
                            mDataManager.setFirstLogin(false);
                            mDataManager.updateUserSession(location, instruments);
                            mProfileInstrumentsView.showUpdateSuccess();
                        }

                        @Override
                        public void onUserUpdateFailure(String error) {
                            mProfileInstrumentsView.showMessage(error);
                        }
                    });
        }
    }


}
