package margheritadonnici.com.jambuddies.utils;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Base64;
import android.widget.ImageView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import margheritadonnici.com.jambuddies.R;

/**
 * Created by Margherita Donnici on 11/19/17.
 */

public class ImageUtils {

    private static final int MAX_IMAGE_DIMENSION = 512;
    private static Context mContext;

    // Creates image file to save picture taken from camera
    public static File createImageFile(Context mContext) throws IOException {
        // Create an image file name, with date to avoid collisions
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = mContext.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        return image;
    }

    // Reduce the amount of dynamic heap used by expanding the JPEG into a memory array
    // that's already scaled to match the size of the destination view.
    public static Bitmap getBitmapFromPath(int targetWidth, int targetHeight, String currentPhotoPath) {
        // Get the dimensions of the View
        int targetW = targetWidth;
        int targetH = targetHeight;

        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(currentPhotoPath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        // Determine how much to scale down the image
        int scaleFactor = Math.min(photoW / targetW, photoH / targetH);

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;

        return BitmapFactory.decodeFile(currentPhotoPath, bmOptions);
    }

    // Capture image orientation
    public static int getOrientation(Context context, Uri photoUri) {
    /* it's on the external media. */
        Cursor cursor = context.getContentResolver().query(photoUri,
                new String[]{MediaStore.Images.ImageColumns.ORIENTATION}, null, null, null);

        if (cursor.getCount() != 1) {
            return -1;
        }

        cursor.moveToFirst();
        return cursor.getInt(0);
    }

    public static Bitmap getCorrectlyOrientedImage(Context context, Uri photoUri) throws IOException {
        InputStream is = context.getContentResolver().openInputStream(photoUri);
        BitmapFactory.Options dbo = new BitmapFactory.Options();
        dbo.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(is, null, dbo);
        is.close();

        int rotatedWidth, rotatedHeight;
        int orientation = getOrientation(context, photoUri);

        if (orientation == 90 || orientation == 270) {
            rotatedWidth = dbo.outHeight;
            rotatedHeight = dbo.outWidth;
        } else {
            rotatedWidth = dbo.outWidth;
            rotatedHeight = dbo.outHeight;
        }

        Bitmap srcBitmap;
        is = context.getContentResolver().openInputStream(photoUri);
        if (rotatedWidth > MAX_IMAGE_DIMENSION || rotatedHeight > MAX_IMAGE_DIMENSION) {
            float widthRatio = ((float) rotatedWidth) / ((float) MAX_IMAGE_DIMENSION);
            float heightRatio = ((float) rotatedHeight) / ((float) MAX_IMAGE_DIMENSION);
            float maxRatio = Math.max(widthRatio, heightRatio);

            // Create the bitmap from file
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = (int) maxRatio;
            srcBitmap = BitmapFactory.decodeStream(is, null, options);
        } else {
            srcBitmap = BitmapFactory.decodeStream(is);
        }
        is.close();

    /*
     * if the orientation is not 0 (or -1, which means we don't know), we
     * have to do a rotation.
     */
        if (orientation > 0) {
            Matrix matrix = new Matrix();
            matrix.postRotate(orientation);

            srcBitmap = Bitmap.createBitmap(srcBitmap, 0, 0, srcBitmap.getWidth(),
                    srcBitmap.getHeight(), matrix, true);
        }

        return srcBitmap;
    }

    public static Bitmap rotateImage(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(),
                matrix, true);
    }

    public static Bitmap getRotatedBitmap(String photoPath, Bitmap photoBitmap) throws IOException {
        ExifInterface ei = new ExifInterface(photoPath);
        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);

        Bitmap rotatedBitmap = null;
        switch (orientation) {

            case ExifInterface.ORIENTATION_ROTATE_90:
                rotatedBitmap = rotateImage(photoBitmap, 90);
                break;

            case ExifInterface.ORIENTATION_ROTATE_180:
                rotatedBitmap = rotateImage(photoBitmap, 180);
                break;

            case ExifInterface.ORIENTATION_ROTATE_270:
                rotatedBitmap = rotateImage(photoBitmap, 270);
                break;

            case ExifInterface.ORIENTATION_NORMAL:
            default:
                rotatedBitmap = photoBitmap;
        }
        return rotatedBitmap;
    }

    public static Bitmap getBitmapFromImageView(ImageView img) {
        BitmapDrawable drawable = (BitmapDrawable) img.getDrawable();
        Bitmap bitmap = drawable.getBitmap();
        return bitmap;
    }

    public static String getBase64FromBitmap(Bitmap bitmap) {
        // Convert image to byte array
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] byteArray = baos.toByteArray();
        return Base64.encodeToString(byteArray, Base64.DEFAULT);
    }

    public static Bitmap getBitmapFromBase64(String encoding) {
        //decode base64 string to image
        byte[] imageBytes = Base64.decode(encoding, Base64.DEFAULT);
        Bitmap decodedImage = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length);
        return decodedImage;
    }

    public static int getInstrumentImage(String instrument) {
        int image;
        switch (instrument) {
            case "Accordion":
                image = R.drawable.accordion;
                break;
            case "Acoustic Guitar":
                image = R.drawable.acoustic_guitar;
                break;
            case "Balalaika":
                image = R.drawable.balalaika;
                break;
            case "Banjo":
                image = R.drawable.banjo;
                break;
            case "Bass Guitar":
                image = R.drawable.bass_guitar;
                break;
            case "Bongo":
                image = R.drawable.bongo;
                break;
            case "Clarinet":
                image = R.drawable.clarinet;
                break;
            case "Cymbals":
                image = R.drawable.cymbals;
                break;
            case "Conga":
                image = R.drawable.conga;
                break;
            case "Djembe":
                image = R.drawable.djembe;
                break;
            case "Drums":
                image = R.drawable.drums;
                break;
            case "Electric Guitar":
                image = R.drawable.electric_guitar;
                break;
            case "Mixer":
                image = R.drawable.mixer;
                break;
            case "Flute":
                image = R.drawable.flute;
                break;
            case "French Horn":
                image = R.drawable.french_horn;
                break;
            case "Gong":
                image = R.drawable.gong;
                break;
            case "Harmonica":
                image = R.drawable.harmonica;
                break;
            case "Harp":
                image = R.drawable.harp;
                break;
            case "Maracas":
                image = R.drawable.maracas;
                break;
            case "Singer":
                image = R.drawable.microphone;
                break;
            case "Oboe":
                image = R.drawable.oboe;
                break;
            case "Classical Piano":
                image = R.drawable.piano;
                break;
            case "Keyboard":
                image = R.drawable.keyboard;
                break;
            case "Piccolo":
                image = R.drawable.piccolo;
                break;
            case "Saxophone":
                image = R.drawable.saxophone;
                break;
            case "Tambourine":
                image = R.drawable.tambourine;
                break;
            case "Timpani":
                image = R.drawable.timpani;
                break;
            case "Triangle":
                image = R.drawable.triangle;
                break;
            case "Trombone":
                image = R.drawable.trombone;
                break;
            case "Trumpet":
                image = R.drawable.trumpet;
                break;
            case "Tuba":
                image = R.drawable.tuba;
                break;
            case "Ukulele":
                image = R.drawable.ukelele;
                break;
            case "Violin":
                image = R.drawable.violin;
                break;
            case "Xylophone":
                image = R.drawable.xylophone;
            default:
                image = R.drawable.quaver;
                break;
        }
        return image;
    }
}
