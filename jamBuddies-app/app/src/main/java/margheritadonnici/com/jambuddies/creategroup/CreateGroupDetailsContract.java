package margheritadonnici.com.jambuddies.creategroup;

import android.content.Intent;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;

import margheritadonnici.com.jambuddies.BasePresenter;
import margheritadonnici.com.jambuddies.BaseView;
import margheritadonnici.com.jambuddies.utils.PlaceArrayAdapter;

/**
 * Created by Margherita Donnici on 10/29/17.
 */

public interface CreateGroupDetailsContract {

    interface View extends BaseView<Presenter> {

        void showMessage(String msg);

        void showError(String error);

        PlaceArrayAdapter getPlaceArrayAdapter();

        void showEnableGPSDialog();
    }

    interface Presenter extends BasePresenter {

        LatLng getUserLocation();

        String getLocationAddressString(LatLng location);

        GoogleApiClient getGoogleApiClient();

        void checkUserPermissions();

        void result(int requestCode, int resultCode, Intent data);

        boolean validateData(String groupName, String groupDescription, Boolean coverBand, String coverArtists, ArrayList<String> genreList, LatLng mLocation);
    }
}
