package margheritadonnici.com.jambuddies.data.model;

import java.io.Serializable;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Margherita Donnici on 1/11/18.
 */

public class UserRequests implements Serializable {

    @SerializedName("receivedRequests")
    @Expose
    private List<JoinRequestWithDetails> receivedRequests = null;
    @SerializedName("sentRequests")
    @Expose
    private List<JoinRequestWithDetails> sentRequests = null;
    private final static long serialVersionUID = -1619795874092568613L;

    public List<JoinRequestWithDetails> getReceivedRequests() {
        return receivedRequests;
    }

    public void setReceivedRequests(List<JoinRequestWithDetails> receivedRequests) {
        this.receivedRequests = receivedRequests;
    }

    public List<JoinRequestWithDetails> getSentRequests() {
        return sentRequests;
    }

    public void setSentRequests(List<JoinRequestWithDetails> sentRequests) {
        this.sentRequests = sentRequests;
    }

}
