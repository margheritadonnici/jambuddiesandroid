package margheritadonnici.com.jambuddies.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Margherita Donnici on 1/9/18.
 */

public class ExistingMemberWithUser {

    @SerializedName("userId")
    @Expose
    private User userId;
    @SerializedName("instrument")
    @Expose
    private String instrument;

    private boolean isHeader;

    public ExistingMemberWithUser(String instrument, boolean isHeader) {
        this.instrument = instrument;
        this.isHeader = isHeader;
    }

    public User getUserId() {
        return userId;
    }

    public void setUserId(User userId) {
        this.userId = userId;
    }

    public String getInstrument() {
        return instrument;
    }

    public void setInstrument(String instrument) {
        this.instrument = instrument;
    }

    public void setIsHeader(boolean bool) {
        this.isHeader = bool;
    }

    public boolean isHeader() {
        return isHeader;
    }

    @Override
    public String toString() {
        return "ExistingMemberWithUser{" +
                "userId=" + userId +
                ", instrument='" + instrument + '\'' +
                ", isHeader=" + isHeader +
                '}';
    }
}
