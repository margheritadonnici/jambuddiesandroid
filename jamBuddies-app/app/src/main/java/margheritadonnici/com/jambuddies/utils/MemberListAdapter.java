package margheritadonnici.com.jambuddies.utils;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import margheritadonnici.com.jambuddies.R;
import margheritadonnici.com.jambuddies.data.model.ExistingMemberWithUser;

/**
 * Created by Margherita Donnici on 1/11/18.
 */

// Adapter for the member list
public class MemberListAdapter extends ArrayAdapter<ExistingMemberWithUser> {

    private final Context mContext;
    private final List<ExistingMemberWithUser> mMemberList;

    public MemberListAdapter(Context context, List<ExistingMemberWithUser> memberList) {
        super(context, R.layout.view_members_list_layout, memberList);
        this.mContext = context;
        this.mMemberList = memberList;
    }

    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View rowView = inflater.inflate(R.layout.view_members_list_layout, null, true);
        TextView memberName = (TextView) rowView.findViewById(R.id.member_name);
        ImageView imageView = (ImageView) rowView.findViewById(R.id.icon);
        TextView memberInstrument = (TextView) rowView.findViewById(R.id.member_instrument);
        memberName.setText(mMemberList.get(position).getUserId().getFirstName() + " " + mMemberList.get(position).getUserId().getLastName());
        if (mMemberList.get(position).getUserId().getProfilePicture() == null) {
            // If the user hasn't selected any profile picture, show the instrument image
            imageView.setImageResource(ImageUtils.getInstrumentImage(mMemberList.get(position).getInstrument()));
        } else {
            // If the user has selected a profile picture, show it
            imageView.setImageBitmap(ImageUtils.getBitmapFromBase64(mMemberList.get(position).getUserId().getProfilePicture()));
        }
        memberInstrument.setText(mMemberList.get(position).getInstrument());
        return rowView;
    }
}
