package margheritadonnici.com.jambuddies.creategroup;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import margheritadonnici.com.jambuddies.R;
import margheritadonnici.com.jambuddies.data.model.ExistingMember;
import margheritadonnici.com.jambuddies.utils.ImageUtils;

/**
 * Created by Margherita Donnici on 10/17/17.
 */

// http://www.vogella.com/tutorials/AndroidRecyclerView/article.html0
// https://guides.codepath.com/android/using-the-recyclerview
// https://code.tutsplus.com/tutorials/getting-started-with-recyclerview-and-cardview-on-android--cms-23465

public class NewMembersListAdapter extends RecyclerView.Adapter<NewMembersListAdapter.ViewHolder> {

    private List<ExistingMember> mMembers;
    private Context mContext;
    private LookingForFragment mFragment;

    private class VIEW_TYPES {
        public static final int Header = 1;
        public static final int Normal = 2;
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder


    public class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public CardView card;
        public ImageView icon;
        public TextView txtHeader;
        public TextView txtFooter;
        public TextView txtNotes;
        public ImageButton deleteButton;
        public ImageButton editButton;
        public View layout;

        public ViewHolder(View itemView) {
            super(itemView);
            layout = itemView;
            icon = (ImageView) itemView.findViewById(R.id.icon);
            card = (CardView) itemView.findViewById(R.id.list_member_card_view);
            txtHeader = (TextView) itemView.findViewById(R.id.firstLine);
            txtFooter = (TextView) itemView.findViewById(R.id.secondLine);
            txtNotes = (TextView) itemView.findViewById(R.id.thirdLine);
            deleteButton = (ImageButton) itemView.findViewById(R.id.delete_btn);
            editButton = (ImageButton) itemView.findViewById(R.id.edit_btn);
        }
    }

    // Pass in the contact array into the constructor
    public NewMembersListAdapter(Context context, LookingForFragment myFragment, List<ExistingMember> membersList) {
        mMembers = membersList;
        mContext = context;
        mFragment = myFragment;
    }

    // Easy access to the context object in the recyclerview
    private Context getContext() {
        return mContext;
    }

    public void add(ExistingMember item) {
        mMembers.add(item);
        int position = mMembers.size() - 1;
        if (position < 0) {
            notifyItemInserted(0);
        } else {
            notifyItemInserted(mMembers.size() - 1);
        }
        mFragment.getEmptyListView().setVisibility(View.GONE);
    }

    public void remove(int position) {
        mMembers.remove(position);
        notifyItemRemoved(position);
        if (mMembers.isEmpty()) {
            mFragment.getEmptyListView().setVisibility(View.VISIBLE);
        }
    }

    public void edit(ExistingMember editedMember, int position) {
        ExistingMember originalMember = mMembers.get(position);
        originalMember.setInstrument(editedMember.getInstrument());
        notifyItemChanged(position);
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View memberView;
        switch (viewType) {
            case NewMembersListAdapter.VIEW_TYPES.Normal:
                memberView = inflater.inflate(R.layout.members_list_layout, parent, false);
                break;
            case NewMembersListAdapter.VIEW_TYPES.Header:
                memberView = inflater.inflate(R.layout.existing_members_list_header, parent, false);
                break;
            default:
                memberView = inflater.inflate(R.layout.members_list_layout, parent, false);
                break;
        }
        // set the view's size, margins, paddings and layout parameters
        ViewHolder viewHolder = new ViewHolder(memberView);
        return viewHolder;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        final ExistingMember member = mMembers.get(position);
        holder.txtHeader.setText(member.getInstrument());
        if (position == 0) {
            holder.txtFooter.setText("You");
        }
        holder.icon.setImageResource(ImageUtils.getInstrumentImage(member.getInstrument()));
        // Edit button listener
        holder.editButton.setOnClickListener(new View.OnClickListener() {
                                                 @Override
                                                 public void onClick(View view) {
                                                     mFragment.showNewMembersDialog(holder.getAdapterPosition());
                                                 }
                                             }

        );
        // Only add listener for delete button if the item is not the header
        if (position != 0) {
            holder.deleteButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    remove(holder.getAdapterPosition());
                }
            });
        }
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mMembers.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (mMembers.get(position).isHeader())
            return NewMembersListAdapter.VIEW_TYPES.Header;
        else
            return NewMembersListAdapter.VIEW_TYPES.Normal;
    }
}

