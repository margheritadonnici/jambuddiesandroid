package margheritadonnici.com.jambuddies.utils;
import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.plumillonforge.android.chipview.ChipViewAdapter;

import margheritadonnici.com.jambuddies.R;
import margheritadonnici.com.jambuddies.utils.Tag;

public class MainChipViewAdapter extends ChipViewAdapter {
    public MainChipViewAdapter(Context context) {
        super(context);
    }

    @Override
    public int getLayoutRes(int position) {
        Tag tag = (Tag) getChip(position);

        switch (tag.getType()) {
            case 2:
            case 3:
            default:
                return 0;
            case 1:
                return R.layout.chip_close;
        }
    }

    @Override
    public int getBackgroundColor(int position) {
        Tag tag = (Tag) getChip(position);

        switch (tag.getType()) {
            default:
                return 0;
            case 1:
                return getColor(R.color.colorAccent);
            case 2:
                return getColor(R.color.red);
            case 3:
                return getColor(R.color.green);
            case 4:
                return getColor(R.color.colorAccent);
        }
    }

    @Override
    public int getBackgroundColorSelected(int position) {
        return 0;
    }

    @Override
    public int getBackgroundRes(int position) {
        return 0;
    }

    @Override
    public void onLayout(View view, int position) {
        Tag tag = (Tag) getChip(position);

        if (tag.getType() == 2 || tag.getType() == 3 || tag.getType() == 4)
            ((TextView) view.findViewById(android.R.id.text1)).setTextColor(getColor(R.color.white));
    }
}
