package margheritadonnici.com.jambuddies.createprofile;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import margheritadonnici.com.jambuddies.R;
import margheritadonnici.com.jambuddies.data.model.User;
import margheritadonnici.com.jambuddies.utils.ActivityUtils;

public class CreateProfileActivity extends AppCompatActivity {

    private static final String TAG = "CreateProfileActivity";
    private CreateProfileDetailsPresenter mProfileDetailsPresenter;
    private CreateProfileDetailsFragment mProfileDetailsFragment;
    private CreateProfileInstrumentsPresenter mCreateProfileInstrumentsPresenter;
    public User userData;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_profile);

        // Set Action Bar title
        getSupportActionBar().setTitle("Create Profile");

        // Initialize a user instance
        userData = new User();

        // Load first form fragment
        mProfileDetailsFragment = (CreateProfileDetailsFragment) getSupportFragmentManager().findFragmentById(R.id.create_profile_frame_layout);
        if (mProfileDetailsFragment == null) {
            mProfileDetailsFragment = CreateProfileDetailsFragment.newInstance();
            ActivityUtils.addFragmentToActivity(getSupportFragmentManager(), mProfileDetailsFragment, R.id.create_profile_frame_layout);
        }
        // Create the presenter
        mProfileDetailsPresenter = new CreateProfileDetailsPresenter(mProfileDetailsFragment, this);
    }

    public void goToPickInstrumentsFragment() {
        CreateProfileInstrumentsFragment pickInstrumentsFragment = CreateProfileInstrumentsFragment.newInstance();
        ActivityUtils.replaceFragment(getSupportFragmentManager(), pickInstrumentsFragment, R.id.create_profile_frame_layout);
        // Create the presenter
        mCreateProfileInstrumentsPresenter = new CreateProfileInstrumentsPresenter(pickInstrumentsFragment, this);
        mCreateProfileInstrumentsPresenter.start();
    }

    @Override
    public void onBackPressed() {
        if (mProfileDetailsFragment.isVisible()) {
            android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(this).create();
            alertDialog.setTitle("Warning");
            alertDialog.setIcon(R.drawable.warning_icon);
            alertDialog.setMessage("Please create your profile before continuing.");
            alertDialog.setButton(android.app.AlertDialog.BUTTON_NEUTRAL, "OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            alertDialog.show();
        } else {
            super.onBackPressed();
        }
        int count = getFragmentManager().getBackStackEntryCount();
    }

    public User getUserData() {
        return userData;
    }
}